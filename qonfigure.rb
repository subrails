# License, not of this script, but of the application it contains:
#
# 
# Copyright (C) 2007 David Cuadrado                                     
# krawek@gmail.com                                                      
#                                                                       
# This library is free software; you can redistribute it and/or         
# modify it under the terms of the GNU Lesser General Public            
# License as published by the Free Software Foundation; either          
# version 2.1 of the License, or (at your option) any later version.    
#                                                                       
# This library is distributed in the hope that it will be useful,       
# but WITHOUT ANY WARRANTY; without even the implied warranty of        
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     
# Lesser General Public License for more details.                       
#                                                                       
# You should have received a copy of the GNU Lesser General Public      
# License along with this library; if not, write to the Free Software   
# Foundation, Inc.,                                                     
# 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              
# 

# License of this script, not of the application it contains:
#
# Copyright Erik Veenstra <tar2rubyscript@erikveen.dds.nl>
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 59 Temple Place, Suite 330,
# Boston, MA 02111-1307 USA.

# Parts of this code are based on code from Thomas Hurst
# <tom@hur.st>.

# Tar2RubyScript constants

unless defined?(BLOCKSIZE)
  ShowContent	= ARGV.include?("--tar2rubyscript-list")
  JustExtract	= ARGV.include?("--tar2rubyscript-justextract")
  ToTar		= ARGV.include?("--tar2rubyscript-totar")
  Preserve	= ARGV.include?("--tar2rubyscript-preserve")
end

ARGV.concat	[]

ARGV.delete_if{|arg| arg =~ /^--tar2rubyscript-/}

ARGV << "--tar2rubyscript-preserve"	if Preserve

# Tar constants

unless defined?(BLOCKSIZE)
  BLOCKSIZE		= 512

  NAMELEN		= 100
  MODELEN		= 8
  UIDLEN		= 8
  GIDLEN		= 8
  CHKSUMLEN		= 8
  SIZELEN		= 12
  MAGICLEN		= 8
  MODTIMELEN		= 12
  UNAMELEN		= 32
  GNAMELEN		= 32
  DEVLEN		= 8

  TMAGIC		= "ustar"
  GNU_TMAGIC		= "ustar  "
  SOLARIS_TMAGIC	= "ustar\00000"

  MAGICS		= [TMAGIC, GNU_TMAGIC, SOLARIS_TMAGIC]

  LF_OLDFILE		= '\0'
  LF_FILE		= '0'
  LF_LINK		= '1'
  LF_SYMLINK		= '2'
  LF_CHAR		= '3'
  LF_BLOCK		= '4'
  LF_DIR		= '5'
  LF_FIFO		= '6'
  LF_CONTIG		= '7'

  GNUTYPE_DUMPDIR	= 'D'
  GNUTYPE_LONGLINK	= 'K'	# Identifies the *next* file on the tape as having a long linkname.
  GNUTYPE_LONGNAME	= 'L'	# Identifies the *next* file on the tape as having a long name.
  GNUTYPE_MULTIVOL	= 'M'	# This is the continuation of a file that began on another volume.
  GNUTYPE_NAMES		= 'N'	# For storing filenames that do not fit into the main header.
  GNUTYPE_SPARSE	= 'S'	# This is for sparse files.
  GNUTYPE_VOLHDR	= 'V'	# This file is a tape/volume header.  Ignore it on extraction.
end

class Dir
  def self.rm_rf(entry)
    begin
      File.chmod(0755, entry)
    rescue
    end

    if File.ftype(entry) == "directory"
      pdir	= Dir.pwd

      Dir.chdir(entry)
        Dir.open(".") do |d|
          d.each do |e|
            Dir.rm_rf(e)	if not [".", ".."].include?(e)
          end
        end
      Dir.chdir(pdir)

      begin
        Dir.delete(entry)
      rescue => e
        $stderr.puts e.message
      end
    else
      begin
        File.delete(entry)
      rescue => e
        $stderr.puts e.message
      end
    end
  end
end

class Reader
  def initialize(filehandle)
    @fp	= filehandle
  end

  def extract
    each do |entry|
      entry.extract
    end
  end

  def list
    each do |entry|
      entry.list
    end
  end

  def each
    @fp.rewind

    while entry	= next_entry
      yield(entry)
    end
  end

  def next_entry
    buf	= @fp.read(BLOCKSIZE)

    if buf.length < BLOCKSIZE or buf == "\000" * BLOCKSIZE
      entry	= nil
    else
      entry	= Entry.new(buf, @fp)
    end

    entry
  end
end

class Entry
  attr_reader(:header, :data)

  def initialize(header, fp)
    @header	= Header.new(header)

    readdata =
    lambda do |header|
      padding	= (BLOCKSIZE - (header.size % BLOCKSIZE)) % BLOCKSIZE
      @data	= fp.read(header.size)	if header.size > 0
      dummy	= fp.read(padding)	if padding > 0
    end

    readdata.call(@header)

    if @header.longname?
      gnuname		= @data[0..-2]

      header		= fp.read(BLOCKSIZE)
      @header		= Header.new(header)
      @header.name	= gnuname

      readdata.call(@header)
    end
  end

  def extract
    if not @header.name.empty?
      if @header.symlink?
        begin
          File.symlink(@header.linkname, @header.name)
        rescue SystemCallError => e
          $stderr.puts "Couldn't create symlink #{@header.name}: " + e.message
        end
      elsif @header.link?
        begin
          File.link(@header.linkname, @header.name)
        rescue SystemCallError => e
          $stderr.puts "Couldn't create link #{@header.name}: " + e.message
        end
      elsif @header.dir?
        begin
          Dir.mkdir(@header.name, @header.mode)
        rescue SystemCallError => e
          $stderr.puts "Couldn't create dir #{@header.name}: " + e.message
        end
      elsif @header.file?
        begin
          File.open(@header.name, "wb") do |fp|
            fp.write(@data)
            fp.chmod(@header.mode)
          end
        rescue => e
          $stderr.puts "Couldn't create file #{@header.name}: " + e.message
        end
      else
        $stderr.puts "Couldn't handle entry #{@header.name} (flag=#{@header.linkflag.inspect})."
      end

      #File.chown(@header.uid, @header.gid, @header.name)
      #File.utime(Time.now, @header.mtime, @header.name)
    end
  end

  def list
    if not @header.name.empty?
      if @header.symlink?
        $stderr.puts "s %s -> %s" % [@header.name, @header.linkname]
      elsif @header.link?
        $stderr.puts "l %s -> %s" % [@header.name, @header.linkname]
      elsif @header.dir?
        $stderr.puts "d %s" % [@header.name]
      elsif @header.file?
        $stderr.puts "f %s (%s)" % [@header.name, @header.size]
      else
        $stderr.puts "Couldn't handle entry #{@header.name} (flag=#{@header.linkflag.inspect})."
      end
    end
  end
end

class Header
  attr_reader(:name, :uid, :gid, :size, :mtime, :uname, :gname, :mode, :linkflag, :linkname)
  attr_writer(:name)

  def initialize(header)
    fields	= header.unpack('A100 A8 A8 A8 A12 A12 A8 A1 A100 A8 A32 A32 A8 A8')
    types	= ['str', 'oct', 'oct', 'oct', 'oct', 'time', 'oct', 'str', 'str', 'str', 'str', 'str', 'oct', 'oct']

    begin
      converted	= []
      while field = fields.shift
        type	= types.shift

        case type
        when 'str'	then converted.push(field)
        when 'oct'	then converted.push(field.oct)
        when 'time'	then converted.push(Time::at(field.oct))
        end
      end

      @name, @mode, @uid, @gid, @size, @mtime, @chksum, @linkflag, @linkname, @magic, @uname, @gname, @devmajor, @devminor	= converted

      @name.gsub!(/^\.\//, "")
      @linkname.gsub!(/^\.\//, "")

      @raw	= header
    rescue ArgumentError => e
      raise "Couldn't determine a real value for a field (#{field})"
    end

    raise "Magic header value #{@magic.inspect} is invalid."	if not MAGICS.include?(@magic)

    @linkflag	= LF_FILE			if @linkflag == LF_OLDFILE or @linkflag == LF_CONTIG
    @linkflag	= LF_DIR			if @linkflag == LF_FILE and @name[-1] == '/'
    @size	= 0				if @size < 0
  end

  def file?
    @linkflag == LF_FILE
  end

  def dir?
    @linkflag == LF_DIR
  end

  def symlink?
    @linkflag == LF_SYMLINK
  end

  def link?
    @linkflag == LF_LINK
  end

  def longname?
    @linkflag == GNUTYPE_LONGNAME
  end
end

class Content
  @@count	= 0	unless defined?(@@count)

  def initialize
    @@count += 1

    @archive	= File.open(File.expand_path(__FILE__), "rb"){|f| f.read}.gsub(/\r/, "").split(/\n\n/)[-1].split("\n").collect{|s| s[2..-1]}.join("\n").unpack("m").shift
    temp	= ENV["TEMP"]
    temp	= "/tmp"	if temp.nil?
    temp	= File.expand_path(temp)
    @tempfile	= "#{temp}/tar2rubyscript.f.#{Process.pid}.#{@@count}"
  end

  def list
    begin
      File.open(@tempfile, "wb")	{|f| f.write @archive}
      File.open(@tempfile, "rb")	{|f| Reader.new(f).list}
    ensure
      File.delete(@tempfile)
    end

    self
  end

  def cleanup
    @archive	= nil

    self
  end
end

class TempSpace
  @@count	= 0	unless defined?(@@count)

  def initialize
    @@count += 1

    @archive	= File.open(File.expand_path(__FILE__), "rb"){|f| f.read}.gsub(/\r/, "").split(/\n\n/)[-1].split("\n").collect{|s| s[2..-1]}.join("\n").unpack("m").shift
    @olddir	= Dir.pwd
    temp	= ENV["TEMP"]
    temp	= "/tmp"	if temp.nil?
    temp	= File.expand_path(temp)
    @tempfile	= "#{temp}/tar2rubyscript.f.#{Process.pid}.#{@@count}"
    @tempdir	= "#{temp}/tar2rubyscript.d.#{Process.pid}.#{@@count}"

    @@tempspace	= self

    @newdir	= @tempdir

    @touchthread =
    Thread.new do
      loop do
        sleep 60*60

        touch(@tempdir)
        touch(@tempfile)
      end
    end
  end

  def extract
    Dir.rm_rf(@tempdir)	if File.exists?(@tempdir)
    Dir.mkdir(@tempdir)

    newlocation do

		# Create the temp environment.

      File.open(@tempfile, "wb")	{|f| f.write @archive}
      File.open(@tempfile, "rb")	{|f| Reader.new(f).extract}

		# Eventually look for a subdirectory.

      entries	= Dir.entries(".")
      entries.delete(".")
      entries.delete("..")

      if entries.length == 1
        entry	= entries.shift.dup
        if File.directory?(entry)
          @newdir	= "#{@tempdir}/#{entry}"
        end
      end
    end

		# Remember all File objects.

    @ioobjects	= []
    ObjectSpace::each_object(File) do |obj|
      @ioobjects << obj
    end

    at_exit do
      @touchthread.kill

		# Close all File objects, opened in init.rb .

      ObjectSpace::each_object(File) do |obj|
        obj.close	if (not obj.closed? and not @ioobjects.include?(obj))
      end

		# Remove the temp environment.

      Dir.chdir(@olddir)

      Dir.rm_rf(@tempfile)
      Dir.rm_rf(@tempdir)
    end

    self
  end

  def cleanup
    @archive	= nil

    self
  end

  def touch(entry)
    entry	= entry.gsub!(/[\/\\]*$/, "")	unless entry.nil?

    return	unless File.exists?(entry)

    if File.directory?(entry)
      pdir	= Dir.pwd

      begin
        Dir.chdir(entry)

        begin
          Dir.open(".") do |d|
            d.each do |e|
              touch(e)	unless [".", ".."].include?(e)
            end
          end
        ensure
          Dir.chdir(pdir)
        end
      rescue Errno::EACCES => error
        $stderr.puts error
      end
    else
      File.utime(Time.now, File.mtime(entry), entry)
    end
  end

  def oldlocation(file="")
    if block_given?
      pdir	= Dir.pwd

      Dir.chdir(@olddir)
        res	= yield
      Dir.chdir(pdir)
    else
      res	= File.expand_path(file, @olddir)	if not file.nil?
    end

    res
  end

  def newlocation(file="")
    if block_given?
      pdir	= Dir.pwd

      Dir.chdir(@newdir)
        res	= yield
      Dir.chdir(pdir)
    else
      res	= File.expand_path(file, @newdir)	if not file.nil?
    end

    res
  end

  def templocation(file="")
    if block_given?
      pdir	= Dir.pwd

      Dir.chdir(@tempdir)
        res	= yield
      Dir.chdir(pdir)
    else
      res	= File.expand_path(file, @tempdir)	if not file.nil?
    end

    res
  end

  def self.oldlocation(file="")
    if block_given?
      @@tempspace.oldlocation { yield }
    else
      @@tempspace.oldlocation(file)
    end
  end

  def self.newlocation(file="")
    if block_given?
      @@tempspace.newlocation { yield }
    else
      @@tempspace.newlocation(file)
    end
  end

  def self.templocation(file="")
    if block_given?
      @@tempspace.templocation { yield }
    else
      @@tempspace.templocation(file)
    end
  end
end

class Extract
  @@count	= 0	unless defined?(@@count)

  def initialize
    @archive	= File.open(File.expand_path(__FILE__), "rb"){|f| f.read}.gsub(/\r/, "").split(/\n\n/)[-1].split("\n").collect{|s| s[2..-1]}.join("\n").unpack("m").shift
    temp	= ENV["TEMP"]
    temp	= "/tmp"	if temp.nil?
    @tempfile	= "#{temp}/tar2rubyscript.f.#{Process.pid}.#{@@count += 1}"
  end

  def extract
    begin
      File.open(@tempfile, "wb")	{|f| f.write @archive}
      File.open(@tempfile, "rb")	{|f| Reader.new(f).extract}
    ensure
      File.delete(@tempfile)
    end

    self
  end

  def cleanup
    @archive	= nil

    self
  end
end

class MakeTar
  def initialize
    @archive	= File.open(File.expand_path(__FILE__), "rb"){|f| f.read}.gsub(/\r/, "").split(/\n\n/)[-1].split("\n").collect{|s| s[2..-1]}.join("\n").unpack("m").shift
    @tarfile	= File.expand_path(__FILE__).gsub(/\.rbw?$/, "") + ".tar"
  end

  def extract
    File.open(@tarfile, "wb")	{|f| f.write @archive}

    self
  end

  def cleanup
    @archive	= nil

    self
  end
end

def oldlocation(file="")
  if block_given?
    TempSpace.oldlocation { yield }
  else
    TempSpace.oldlocation(file)
  end
end

def newlocation(file="")
  if block_given?
    TempSpace.newlocation { yield }
  else
    TempSpace.newlocation(file)
  end
end

def templocation(file="")
  if block_given?
    TempSpace.templocation { yield }
  else
    TempSpace.templocation(file)
  end
end

if ShowContent
  Content.new.list.cleanup
elsif JustExtract
  Extract.new.extract.cleanup
elsif ToTar
  MakeTar.new.extract.cleanup
else
  TempSpace.new.extract.cleanup

  $:.unshift(templocation)
  $:.unshift(newlocation)
  $:.push(oldlocation)

  verbose	= $VERBOSE
  $VERBOSE	= nil
  s	= ENV["PATH"].dup
  if Dir.pwd[1..2] == ":/"	# Hack ???
    s << ";#{templocation.gsub(/\//, "\\")}"
    s << ";#{newlocation.gsub(/\//, "\\")}"
    s << ";#{oldlocation.gsub(/\//, "\\")}"
  else
    s << ":#{templocation}"
    s << ":#{newlocation}"
    s << ":#{oldlocation}"
  end
  ENV["PATH"]	= s
  $VERBOSE	= verbose

  TAR2RUBYSCRIPT	= true	unless defined?(TAR2RUBYSCRIPT)

  newlocation do
    if __FILE__ == $0
      $0.replace(File.expand_path("./init.rb"))

      if File.file?("./init.rb")
        load File.expand_path("./init.rb")
      else
        $stderr.puts "%s doesn't contain an init.rb ." % __FILE__
      end
    else
      if File.file?("./init.rb")
        load File.expand_path("./init.rb")
      end
    end
  end
end


# cW9uZmlndXJlLwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAADAwMDA3NTUAMDAwMTc1MQAwMDAxNzUxADAwMDAwMDAwMDAw
# ADEwNzIzMzQzMjI0ADAxMTIwNAAgNQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB1c3RhciAgAGtyYXdl
# awAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAABxb25maWd1cmUvcW9uZi5yYgAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMDAwMDY0NAAwMDAxNzUxADAw
# MDE3NTEAMDAwMDAwMzIzNTQAMTA3MTYyMDY3MDcAMDEyNTExACAwAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAHVzdGFyICAAa3Jhd2VrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAApyZXF1aXJlICd0
# ZXN0JwpyZXF1aXJlICdwa2djb25maWcnCnJlcXVpcmUgJ2Zyb250ZW5kZmFj
# dG9yeScKcmVxdWlyZSAnY29uZmlnJwpyZXF1aXJlICdxbWFrZScKcmVxdWly
# ZSAnbWFrZWZpbGUnCgptb2R1bGUgUW9uZgogIFZFUlNJT04gPSAiMC41Ii5m
# cmVlemUKICBWRVJTSU9OX0RBVEUgPSAiMjAwNy0xMC0xNCIKICAKICBjbGFz
# cyBRdFZlcnNpb24KICAgIGF0dHJfcmVhZGVyIDp2ZXJzaW9uLCA6b3AgIyAw
# OiBlcXVhbCAxOiBncmVhdGVyIDI6IGdyZWF0ZXIgZXF1YWwKICAgIAogICAg
# ZGVmIGluaXRpYWxpemUodmVyc2lvbikKICAgICAgQG9wID0gMgogICAgICBw
# YXJzZSh2ZXJzaW9uKQogICAgZW5kCiAgICAKICAgIGRlZiA9PSh2ZXIpCiAg
# ICAgIEBvcCA9IDAKICAgICAgcGFyc2UodmVyKQogICAgZW5kCiAgICAKICAg
# IGRlZiA+KHZlcikKICAgICAgQG9wID0gMQogICAgICBwYXJzZSh2ZXIpCiAg
# ICBlbmQKICAgIAogICAgZGVmID49KHZlcikKICAgICAgQG9wID0gMgogICAg
# ICBwYXJzZSh2ZXIpCiAgICBlbmQKICAgIAogICAgZGVmIHRvX3MKICAgICAg
# cyA9ICJRdCAiCiAgICAgIGNhc2UgQG9wCiAgICAgICAgd2hlbiAwCiAgICAg
# ICAgICBzKz0iPT0iCiAgICAgICAgd2hlbiAxCiAgICAgICAgICBzKz0iPiIK
# ICAgICAgICBlbHNlCiAgICAgICAgICBzKz0iPj0iCiAgICAgIGVuZAogICAg
# ICBzKz0gIiAiK0B2ZXJzaW9uCiAgICAgIAogICAgICBzCiAgICBlbmQKICAg
# IAogICAgcHJpdmF0ZQogICAgZGVmIHBhcnNlKHZlcnNpb24pCiAgICAgIEB2
# ZXJzaW9uID0gdmVyc2lvbi50b19zCiAgICAgIHZlciA9IEB2ZXJzaW9uLnRv
# X3Muc3BsaXQoJy4nKQogICAgICBzaXplID0gdmVyLnNpemUKICAgICAgICAK
# ICAgICAgZmluYWx2ZXIgPSBbXQogICAgICAzLnRpbWVzIHsgfGl8CiAgICAg
# ICAgdHAgPSB2ZXJbaV0KICAgICAgICAKICAgICAgICBpZiB0cC5uaWw/CiAg
# ICAgICAgICB0cCA9ICIwIgogICAgICAgIGVuZAogICAgICAgIAogICAgICAg
# IGZpbmFsdmVyIDw8IHRwCiAgICAgIH0KICAgICAgCiAgICAgIEB2ZXJzaW9u
# ID0gZmluYWx2ZXIuam9pbigiLiIpCiAgICBlbmQKICBlbmQKICAKICBjbGFz
# cyBTZXR1cAogICAgYXR0cl9yZWFkZXIgOm9wdGlvbnMsIDpwYWNrYWdlcywg
# OnRlc3RzLCA6cGtnX2NvbmZpZ3MKICAgIGRlZiBpbml0aWFsaXplCiAgICAg
# IEBvcHRpb25zID0gW10KICAgICAgQHBhY2thZ2VzID0gW10KICAgICAgQHRl
# c3RzID0gW10KICAgICAgQHBrZ19jb25maWdzID0gW10KICAgICAgQHF0X3Zl
# cnNpb24gPSBRdFZlcnNpb24ubmV3KCI0LjAuMCIpCiAgICAgIAogICAgICBh
# ZGRfb3B0aW9uKDpuYW1lID0+ICJwcmVmaXgiLCA6dHlwZSA9PiAicGF0aCIs
# IDpkZWZhdWx0ID0+ICIvdXNyL2xvY2FsLyIsIDpvcHRpb25hbCA9PiB0cnVl
# LCA6ZGVzY3JpcHRpb24gPT4gIlNldHMgdGhlIGluc3RhbGwgcHJlZml4IikK
# ICAgIGVuZAogICAgCiAgICBkZWYgYWRkX29wdGlvbihvcHRpb24pCiAgICAg
# IGlmIG5vdCBvcHRpb25bOnR5cGVdLm5pbD8KICAgICAgICBvcHRpb25bOnR5
# cGVdLmRvd25jYXNlIQogICAgICBlbmQKICAgICAgCiAgICAgIEBvcHRpb25z
# IDw8IG9wdGlvbgogICAgZW5kCiAgICAKICAgIGRlZiBmaW5kX3BhY2thZ2Uo
# b3B0aW9ucykKICAgICAgQHBhY2thZ2VzIDw8IG9wdGlvbnMKICAgIGVuZAog
# ICAgCiAgICBkZWYgYWRkX3Rlc3Qob3B0aW9uKQogICAgICByYWlzZSAiUGxl
# YXNlIHNldCBhIHZhbGlkIG5hbWUgdG8gdGhlIHRlc3QiIGlmIG9wdGlvbls6
# bmFtZV0udG9fcy5lbXB0eT8KICAgICAgcmFpc2UgIlBsZWFzZSBzZXQgYSB2
# YWxpZCBpZCB0byB0aGUgdGVzdCIgaWYgb3B0aW9uWzppZF0udG9fcy5lbXB0
# eT8KICAgICAgCiAgICAgIEB0ZXN0cyA8PCBvcHRpb24KICAgIGVuZAogICAg
# CiAgICBkZWYgZ2VuZXJhdGVfcGtnY29uZmlnKG9wdGlvbnMpCiAgICAgIHJh
# aXNlICJQbGVhc2Ugc2V0IGEgdmFsaWQgbmFtZSIgaWYgb3B0aW9uc1s6cGFj
# a2FnZV9uYW1lXS50b19zLmVtcHR5PwogICAgICBAcGtnX2NvbmZpZ3MgPDwg
# b3B0aW9ucwogICAgZW5kCiAgICAKICAgIGRlZiBxdAogICAgICBAcXRfdmVy
# c2lvbgogICAgZW5kCiAgICAKICBlbmQKICAKICBjbGFzcyBDb25maWd1cmUK
# ICAgIGRlZiBpbml0aWFsaXplCiAgICAgIEBhcmd1bWVudHMgPSB7fQogICAg
# ICBAY29uZmlnID0gQ29uZmlnLm5ldwogICAgICAKICAgICAgQGxvZ19maWxl
# ID0gb2xkbG9jYXRpb24oImNvbmZpZy5sb2ciKQogICAgICAKICAgICAgRmls
# ZS5vcGVuKEBsb2dfZmlsZSwgInciKSBkbyB8ZnwKICAgICAgICBmIDw8ICI9
# PT09PT09PiBHZW5lcmF0ZWQgYXQgI3tUaW1lLm5vd30gPD09PT09PT1cbiIK
# ICAgICAgZW5kCiAgICAgIAogICAgICBxbWFrZV9wYXRocyA9IFtdCiAgICAg
# IGlmKCBEZXRlY3RPUy5vcz8gIT0gMSApICMgVW5peAogICAgICAgIElPLnBv
# cGVuKCJsb2NhdGUgLXIgL3FtYWtlJCAyPiAje25ld2xvY2F0aW9uKCdsb2Nh
# dGVfbG9nJyl9IikgeyB8cHJjfAogICAgICAgICAgcHJlX3BhdGhzID0gcHJj
# LnJlYWRsaW5lcwogICAgICAgICAgcHJlX3BhdGhzLmVhY2ggeyB8cW18CiAg
# ICAgICAgICAgIHFtLnN0cmlwIQogICAgICAgICAgICBpZiBGaWxlLmV4aXN0
# cz8ocW0pCiAgICAgICAgICAgICAgc3RhdCA9IEZpbGUuc3RhdChxbSkKICAg
# ICAgICAgICAgICAKICAgICAgICAgICAgICBpZiAobm90IChzdGF0LnN5bWxp
# bms/IG9yIHN0YXQuZGlyZWN0b3J5PykpIGFuZCBzdGF0LmV4ZWN1dGFibGU/
# CiAgICAgICAgICAgICAgICBxbWFrZV9wYXRocyA8PCBxbQogICAgICAgICAg
# ICAgIGVuZAogICAgICAgICAgICBlbmQKICAgICAgICAgIH0KICAgICAgICB9
# CiAgICAgIGVuZAogICAgICAKICAgICAgQHFtYWtlID0gUU1ha2UubmV3KHFt
# YWtlX3BhdGhzLCBAbG9nX2ZpbGUpCiAgICAgIAogICAgICBjZmcgPSBTZXR1
# cC5uZXcKICAgICAgY29uZmlndXJlKGNmZykKICAgICAgCiAgICAgIHByaW50
# ICI9PiBTZWFyY2hpbmcgZm9yICN7Y2ZnLnF0fS4uLiAiCiAgICAgIGlmIEBx
# bWFrZS5maW5kX3FtYWtlKGNmZy5xdC52ZXJzaW9uLCBjZmcucXQub3ApCiAg
# ICAgICAgcHV0cyAiKCN7QHFtYWtlLnBhdGh9KSBbWUVTXSIKICAgICAgZWxz
# ZQogICAgICAgIHB1dHMgIltOT10iCiAgICAgICAgZXhpdCgtMSkKICAgICAg
# ZW5kCiAgICAgIAogICAgICBAb3B0aW9ucyA9IGNmZy5vcHRpb25zCiAgICAg
# IEBwYWNrYWdlcyA9IGNmZy5wYWNrYWdlcwogICAgICBAdGVzdHMgPSBjZmcu
# dGVzdHMKICAgICAgQHBrZ19jb25maWdzID0gY2ZnLnBrZ19jb25maWdzCiAg
# ICAgIEBzdGF0dXNGaWxlID0gRGlyLmdldHdkKyIvdXBkYXRlLW1ha2VmaWxl
# IgogICAgZW5kCiAgICAKICAgIGRlZiBmaW5kX3BhY2thZ2VzICMgVE9ETzog
# c29wb3J0YXIgaW5mb3JtYWNpb24gcGFyYSBlbCBzaXN0ZW1hIG9wZXJhdGl2
# byBzb2JyZSBjb21vIGluc3RhbGFyIGVsIHBhcXVldGUKICAgICAgQHBhY2th
# Z2VzLmVhY2ggeyB8cGFja2FnZXwKICAgICAgICBwcmludCAiPT4gU2VhcmNo
# aW5nIGZvciBwYWNrYWdlOiAje3BhY2thZ2VbOm5hbWVdfS4uLiAgIgogICAg
# ICAgIHBrZ2NvbmZpZyA9IFBrZ0NvbmZpZy5uZXcocGFja2FnZVs6bmFtZV0p
# CiAgICAgICAgcGtnY29uZmlnLmFkZF9zZWFyY2hfcGF0aCgiI3tAYXJndW1l
# bnRzWyJwcmVmaXgiXX0vbGliL3BrZ2NvbmZpZyIpCiAgICAgICAgYmVnaW4K
# ICAgICAgICAgIHNldHVwX3BrZ2NvbmZpZyhwa2djb25maWcsIEBhcmd1bWVu
# dHMpCiAgICAgICAgcmVzY3VlIE5vTWV0aG9kRXJyb3IgPT4gZQogICAgICAg
# ICAgaWYgbm90IGUubWVzc2FnZSA9fiAvc2V0dXBfcGtnY29uZmlnLwogICAg
# ICAgICAgICBwdXRzICJFcnJvcjogI3tlLm1lc3NhZ2V9IgogICAgICAgICAg
# ICBwdXRzIGUuYmFja3RyYWNlCiAgICAgICAgICAgIGV4aXQoLTEpCiAgICAg
# ICAgICBlbmQKICAgICAgICBlbmQKICAgICAgICAKICAgICAgICBpZiBub3Qg
# cGtnY29uZmlnLmZpbmQocGFja2FnZVs6bmFtZV0pCiAgICAgICAgICBwdXRz
# ICJbTk9dIgogICAgICAgICAgCiAgICAgICAgICBvcHRpb25hbCA9IHBhY2th
# Z2VbOm9wdGlvbmFsXS5uaWw/ID8gZmFsc2UgOiBwYWNrYWdlWzpvcHRpb25h
# bF0KICAgICAgICAgIAogICAgICAgICAgaWYgbm90IG9wdGlvbmFsCiAgICAg
# ICAgICAgIHB1dHMgIj0+IFRoZSBwYWNrYWdlICcje3BhY2thZ2VbOm5hbWVd
# fScgaXMgcmVxdWlyZWQuIgogICAgICAgICAgICBleGl0KC0xKQogICAgICAg
# ICAgZW5kCiAgICAgICAgZWxzZQogICAgICAgICAgcHV0cyAiW1lFU10iCiAg
# ICAgICAgICBnbG9iYWwgPSBwYWNrYWdlWzpnbG9iYWxdLm5pbD8gPyBmYWxz
# ZSA6IHBhY2thZ2VbOmdsb2JhbF0KICAgICAgICAgIAogICAgICAgICAgcW9u
# ZnBrZyA9IEBjb25maWcKICAgICAgICAgIAogICAgICAgICAgaWYgbm90IGds
# b2JhbAogICAgICAgICAgICBxb25mcGtnID0gUGFja2FnZS5uZXcocGFja2Fn
# ZVs6bmFtZV0pCiAgICAgICAgICBlbmQKICAgICAgICAgIAogICAgICAgICAg
# CiMgICAgICAgICAgIHBrZ2NvbmZpZy5yZXF1aXJlcy5lYWNoIHsgfHJlcXwK
# IyAgICAgICAgICAgICBAY29uZmlnLmFkZF9xb25mX3BhY2thZ2UocmVxKQoj
# ICAgICAgICAgICB9CiAgICAgICAgICAKICAgICAgICAgIHFvbmZwa2cuYWRk
# X2xpYihwa2djb25maWcubGlicykKICAgICAgICAgIAogICAgICAgICAgcGtn
# Y29uZmlnLmluY2x1ZGVfcGF0aC5lYWNoIHsgfGluY3BhdGh8CiAgICAgICAg
# ICAgIHFvbmZwa2cuYWRkX2luY2x1ZGVfcGF0aChpbmNwYXRoKQogICAgICAg
# ICAgfQogICAgICAgICAgCiAgICAgICAgICBwa2djb25maWcuZGVmaW5lcy5l
# YWNoIHsgfGRlZmluZXwKICAgICAgICAgICAgcW9uZnBrZy5hZGRfZGVmaW5l
# KGRlZmluZSkKICAgICAgICAgIH0KICAgICAgICAgIAogICAgICAgICAgaWYg
# bm90IGdsb2JhbAogICAgICAgICAgICBAY29uZmlnLmFkZF9xb25mX3BhY2th
# Z2UocW9uZnBrZykKICAgICAgICAgIGVsc2UKICAgICAgICAgICAgQGNvbmZp
# Zy5hZGRfcW9uZl9mZWF0dXJlKHBhY2thZ2VbOm5hbWVdKQogICAgICAgICAg
# ZW5kCiAgICAgICAgZW5kCiAgICAgIH0KICAgIGVuZAogICAgCiAgICBkZWYg
# cnVuX3Rlc3RzCiAgICAgIEB0ZXN0cy5lYWNoIHsgfHRlc3RvcHR8CiAgICAg
# ICAgcHJpbnQgIj0+IFRlc3RpbmcgI3t0ZXN0b3B0WzpuYW1lXX0uLi4gICIK
# ICAgICAgICAKICAgICAgICB0c3QgPSBUZXN0Lm5ldyh0ZXN0b3B0LCBAY29u
# ZmlnLCBAbG9nX2ZpbGUpCiAgICAgICAgCiAgICAgICAgYmVnaW4KICAgICAg
# ICAgIHNldHVwX3Rlc3QodGVzdG9wdFs6aWRdLCB0c3QsIEBhcmd1bWVudHMp
# CiAgICAgICAgcmVzY3VlIE5vTWV0aG9kRXJyb3IgPT4gZQogICAgICAgICAg
# aWYgbm90IGUubWVzc2FnZSA9fiAvc2V0dXBfdGVzdC8KICAgICAgICAgICAg
# cHV0cyAiRXJyb3I6ICN7ZX0iCiAgICAgICAgICAgIHB1dHMgZS5iYWNrdHJh
# Y2UKICAgICAgICAgICAgZXhpdCgtMSkKICAgICAgICAgIGVuZAogICAgICAg
# IGVuZAogICAgICAgIAogICAgICAgIG9wdGlvbmFsID0gdGVzdG9wdFs6b3B0
# aW9uYWxdLm5pbD8gPyBmYWxzZSA6IHRlc3RvcHRbOm9wdGlvbmFsXQogICAg
# ICAgIGlmIHRzdC5ydW4oQHFtYWtlKQogICAgICAgICAgZ2xvYmFsID0gdGVz
# dG9wdFs6Z2xvYmFsXS5uaWw/ID8gZmFsc2UgOiB0ZXN0b3B0WzpnbG9iYWxd
# CiAgICAgICAgICAKICAgICAgICAgIHFvbmZwa2cgPSBAY29uZmlnCiAgICAg
# ICAgICAKICAgICAgICAgIGlmIG5vdCBnbG9iYWwKICAgICAgICAgICAgcW9u
# ZnBrZyA9IFBhY2thZ2UubmV3KHRzdC5pZCkKICAgICAgICAgIGVuZAogICAg
# ICAgICAgCiAgICAgICAgICB0c3QuaW5jbHVkZV9wYXRoLmVhY2ggeyB8aW5j
# cGF0aHwKICAgICAgICAgICAgcW9uZnBrZy5hZGRfaW5jbHVkZV9wYXRoKGlu
# Y3BhdGgpCiAgICAgICAgICB9CiAgICAgICAgICAKICAgICAgICAgIHRzdC5k
# ZWZpbmVzLmVhY2ggeyB8ZGVmaW5lfAogICAgICAgICAgICBxb25mcGtnLmFk
# ZF9kZWZpbmUoZGVmaW5lKQogICAgICAgICAgfQogICAgICAgICAgCiAgICAg
# ICAgICBxb25mcGtnLmFkZF9saWIodHN0LmxpYnMudG9fcykKICAgICAgICAg
# IAogICAgICAgICAgaWYgbm90IGdsb2JhbAogICAgICAgICAgICBAY29uZmln
# LmFkZF9xb25mX3BhY2thZ2UocW9uZnBrZykKICAgICAgICAgIGVsc2UKICAg
# ICAgICAgICAgQGNvbmZpZy5hZGRfcW9uZl9mZWF0dXJlKHRzdC5pZCkKICAg
# ICAgICAgIGVuZAogICAgICAgICAgcHV0cyAiW09LXSIKICAgICAgICBlbHNl
# CiAgICAgICAgICBwdXRzICJbRkFJTEVEXSIKICAgICAgICAgIAogICAgICAg
# ICAgaWYgbm90IG9wdGlvbmFsCiAgICAgICAgICAgIHB1dHMgIj0+ICN7dGVz
# dG9wdFs6bmFtZV19IGlzIHJlcXVpcmVkIgogICAgICAgICAgICBleGl0KC0x
# KQogICAgICAgICAgZW5kCiAgICAgICAgZW5kCiAgICAgIH0KICAgIGVuZAog
# ICAgCiAgICBkZWYgZ2VuZXJhdGVfbWFrZWZpbGVzCiAgICAgIHB1dHMgIj0+
# IENyZWF0aW5nIG1ha2VmaWxlcy4uLiIKICAgICAgQHFtYWtlLnJ1bigiIiwg
# dHJ1ZSkKICAgICAgCiAgICAgIHB1dHMgIj0+IFVwZGF0aW5nIG1ha2VmaWxl
# cy4uLiIKICAgICAgCiAgICAgIEBtYWtlZmlsZXMgPSBNYWtlZmlsZTo6Zmlu
# ZF9tYWtlZmlsZXMoRGlyLmdldHdkKQogICAgICBAbWFrZWZpbGVzLmVhY2gg
# eyB8bWFrZWZpbGV8CiAgICAgICAgICBNYWtlZmlsZTo6b3ZlcnJpZGUobWFr
# ZWZpbGUsIEZpbGUuZXhwYW5kX3BhdGgoQGFyZ3VtZW50c1sicHJlZml4Il0u
# dG9fcyksIEBzdGF0dXNGaWxlKQogICAgICB9CiAgICAgIAogICAgICBGaWxl
# Lm9wZW4oQHN0YXR1c0ZpbGUsICJ3IiApIHsgfGZpbGV8CiAgICAgICAgZmls
# ZSA8PCAlQAoje0ZpbGUucmVhZChuZXdsb2NhdGlvbigibWFrZWZpbGUucmIi
# KSl9ClFvbmY6Ok1ha2VmaWxlOjpvdmVycmlkZSggQVJHVlswXS50b19zLCAi
# I3tAYXJndW1lbnRzWyJwcmVmaXgiXX0iLCAiI3tAc3RhdHVzRmlsZX0iICkK
# QAoKaWYgRGV0ZWN0T1Mub3M/ID09IDEgI3dpbjMyCiAgICAgICAgZmlsZSA8
# PCAlQApRb25mOjpNYWtlZmlsZTo6b3ZlcnJpZGUoIEFSR1ZbMF0udG9fcysi
# LnJlbGVhc2UiLCAiI3tAYXJndW1lbnRzWyJwcmVmaXgiXX0iLCAiI3tAc3Rh
# dHVzRmlsZX0iICkKUW9uZjo6TWFrZWZpbGU6Om92ZXJyaWRlKCBBUkdWWzBd
# LnRvX3MrIi5kZWJ1ZyIsICIje0Bhcmd1bWVudHNbInByZWZpeCJdfSIsICIj
# e0BzdGF0dXNGaWxlfSIgKQpACmVuZAogICAgICB9CiAgICBlbmQKICAgIAog
# ICAgZGVmIGdldF9hcmdzKGFyZ3YpCiAgICAgIG9wdGMgPSAwCiAgICAgIGxh
# c3Rfb3B0ID0gIiIKICAgICAgCiAgICAgIEBvcHRpb25zLmVhY2ggeyB8b3B0
# aW9ufAogICAgICAgIGdpdmVuX29wdGlvbiA9IG5pbAogICAgICAgIAogICAg
# ICAgIGFyZ3YuZWFjaCB7IHxhcmd8CiAgICAgICAgICBhcmcuc3RyaXAhCiAg
# ICAgICAgICBnaXZlbiA9IGZhbHNlCiAgICAgICAgICAKICAgICAgICAgIGNh
# c2Ugb3B0aW9uWzp0eXBlXS50b19zCiAgICAgICAgICAgIHdoZW4gInN0cmlu
# ZyIKICAgICAgICAgICAgICBpZiBhcmcgPX4gLy0tI3tvcHRpb25bOm5hbWVd
# fT0oW1xXXHddKikvCiAgICAgICAgICAgICAgICBnaXZlbiA9IHRydWUKICAg
# ICAgICAgICAgICAgIGdpdmVuX29wdGlvbiA9ICQxCiAgICAgICAgICAgICAg
# ZW5kCiAgICAgICAgICAgIHdoZW4gInBhdGgiCiAgICAgICAgICAgICAgaWYg
# YXJnID1+IC8tLSN7b3B0aW9uWzpuYW1lXX09KFtcV1x3XSopLwogICAgICAg
# ICAgICAgICAgZ2l2ZW4gPSB0cnVlCiAgICAgICAgICAgICAgICBnaXZlbl9v
# cHRpb24gPSBGaWxlLmV4cGFuZF9wYXRoKCQxKQogICAgICAgICAgICAgIGVu
# ZAogICAgICAgICAgICB3aGVuICJib29sIgogICAgICAgICAgICAgIGlmIGFy
# ZyA9fiAvLS11c2UtI3tvcHRpb25bOm5hbWVdfT0oXHcrKS8KICAgICAgICAg
# ICAgICAgIGdpdmVuID0gdHJ1ZQogICAgICAgICAgICAgICAgZ2l2ZW5fb3B0
# aW9uID0gJDEKICAgICAgICAgICAgICBlbmQKICAgICAgICAgICAgZWxzZQog
# ICAgICAgICAgICAgIGlmIGFyZyA9fiAvLS0je29wdGlvbls6bmFtZV19Lwog
# ICAgICAgICAgICAgICAgZ2l2ZW5fb3B0aW9uID0gInllcyIKICAgICAgICAg
# ICAgICAgIGdpdmVuID0gdHJ1ZQogICAgICAgICAgICAgIGVuZAogICAgICAg
# ICAgZW5kCiAgICAgICAgICAKICAgICAgICAgIGlmIGdpdmVuCiAgICAgICAg
# ICAgIGFyZ3YuZGVsZXRlKGFyZykKICAgICAgICAgIGVuZAogICAgICAgIH0K
# ICAgICAgICAKICAgICAgICBvcHRpb25bOnZhbHVlXSA9IGdpdmVuX29wdGlv
# bgogICAgICB9CiAgICAgIAogICAgICBmcm9udGVuZF9rZXkgPSAidGsiCiAg
# ICAgIGFyZ3YuZWFjaCB7IHx1bmtub3dufAogICAgICAgIHVua25vd24uc3Ry
# aXAhCiAgICAgICAgaWYgdW5rbm93biA9fiAvLS13aXRoLWNvbmZpZy1mcm9u
# dGVuZD0oXHcrKS8KICAgICAgICAgIGZyb250ZW5kX2tleSA9ICQxCiAgICAg
# ICAgZWxzaWYgdW5rbm93biA9fiAvLS1oZWxwLwogICAgICAgICAgdXNhZ2UK
# ICAgICAgICAgIGV4aXQoMCkKICAgICAgICBlbHNpZiB1bmtub3duID1+IC8t
# LXZlcnNpb24vCiAgICAgICAgICBwdXRzICJRT25mICN7VkVSU0lPTn0gWyN7
# VkVSU0lPTl9EQVRFfV0iCiAgICAgICAgICBleGl0KDApCiAgICAgICAgZW5k
# CiAgICAgIH0KICAgICAgCiAgICAgIGZyb250ZW5kID0gRnJvbnRlbmRGYWN0
# b3J5LmNyZWF0ZShmcm9udGVuZF9rZXkpCiAgICAgIGlmIGZyb250ZW5kLm5p
# bD8KICAgICAgICBwdXRzICI9PiBJbnZhbGlkIGZyb250ZW5kIgogICAgICAg
# IHVzYWdlKCkKICAgICAgICBleGl0KC0xKQogICAgICBlbHNpZiBub3QgZnJv
# bnRlbmQucGFyc2UoQG9wdGlvbnMpCiAgICAgICAgdXNhZ2UoKQogICAgICAg
# IGV4aXQoLTEpCiAgICAgIGVuZAogICAgICAKICAgICAgQG9wdGlvbnMuZWFj
# aCB7IHxvcHRpb258CiAgICAgICAgdmFsdWUgPSBvcHRpb25bOnZhbHVlXS5u
# aWw/ID8gb3B0aW9uWzpkZWZhdWx0XS50b19zIDogb3B0aW9uWzp2YWx1ZV0K
# ICAgICAgICBAYXJndW1lbnRzW29wdGlvbls6bmFtZV1dID0gdmFsdWUKICAg
# ICAgfQogICAgICAKICAgIGVuZAogICAgCiAgICBkZWYgc2F2ZShwYXRoKQog
# ICAgICBiZWdpbgogICAgICAgIHNldHVwX2NvbmZpZyhAY29uZmlnLCBAYXJn
# dW1lbnRzKQogICAgICByZXNjdWUgTm9NZXRob2RFcnJvciA9PiBlCiAgICAg
# ICAgaWYgbm90IGUubWVzc2FnZSA9fiAvc2V0dXBfY29uZmlnLwogICAgICAg
# ICAgcHV0cyAiRXJyb3I6ICN7ZX0iCiAgICAgICAgICBwdXRzIGUuYmFja3Ry
# YWNlCiAgICAgICAgICBleGl0KC0xKQogICAgICAgIGVuZAogICAgICBlbmQK
# ICAgICAgCiAgICAgIEBjb25maWcuYWRkX2RlZmluZSglQF9fUFJFRklYX189
# J1xcXFwiJyN7RmlsZS5leHBhbmRfcGF0aCBAYXJndW1lbnRzWyJwcmVmaXgi
# XX0nXFxcXCInQCkKICAgICAgCiAgICAgIHNhdmVfcGtnY29uZmlnCiAgICAg
# IEBjb25maWcuc2F2ZShwYXRoKQogICAgICAKICAgICAgbGF1bmNoZXIgPSBu
# aWwKICAgICAgaWYgRGV0ZWN0T1Mub3M/ID09IDEgIyBXaW5kb3dzCiAgICAg
# ICAgbGF1bmNoZXIgPSBGaWxlLm5ldygibGF1bmNoZXIuYmF0IiwgInciKQog
# ICAgICAgIAogICAgICAgIGxhdW5jaGVyIDw8ICJzZXQgUEFUSD0je0Bjb25m
# aWcuYnVpbGRfZGlyLmdzdWIoJy8nLCAnXFwnKX1cXGJpbjsje0Bjb25maWcu
# YnVpbGRfZGlyLmdzdWIoJy8nLCAnXFwnKX1cXGxpYjsje0Bjb25maWcubGli
# cmFyeV9wYXRoLmpvaW4oIjsiKS5nc3ViKCcvJywgJ1xcJyl9OyVQQVRIJVxu
# IgogICAgICAgIGxhdW5jaGVyIDw8ICIlMVxuIgogICAgICBlbHNlCiAgICAg
# ICAgbGF1bmNoZXIgPSBGaWxlLm5ldygibGF1bmNoZXIuc2giLCJ3IikKICAg
# ICAgICBsYXVuY2hlciA8PCAiIyEvYmluL2Jhc2hcbiIKICAgICAgICBsYXVu
# Y2hlciA8PCAiZXhwb3J0IFBBVEg9I3tAY29uZmlnLmJ1aWxkX2Rpcn0vYmlu
# OiR7UEFUSH1cbiIKICAgICAgICBsYXVuY2hlciA8PCAiZXhwb3J0IExEX0xJ
# QlJBUllfUEFUSD0je0Bjb25maWcuYnVpbGRfZGlyfS9iaW46I3tAY29uZmln
# LmJ1aWxkX2Rpcn0vbGliOiN7QGNvbmZpZy5saWJyYXJ5X3BhdGguam9pbigi
# OiIpfToke0xEX0xJQlJBUllfUEFUSH06JHtEWV9MRF9MSUJSQVJZX1BBVEh9
# XG4iCiAgICAgICAgbGF1bmNoZXIgPDwgImV4cG9ydCBEWUxEX0xJQlJBUllf
# UEFUSD0ke0xEX0xJQlJBUllfUEFUSH1cbiIKICAgICAgICBsYXVuY2hlciA8
# PCAiQVBQPSQxXG4iCiAgICAgICAgbGF1bmNoZXIgPDwgInNoaWZ0XG4iCiAg
# ICAgICAgbGF1bmNoZXIgPDwgImVjaG8gTGF1bmNoaW5nOiAkQVBQXG4iCiAg
# ICAgICAgbGF1bmNoZXIgPDwgIiRBUFAgJCpcbiIKICAgICAgZW5kCiAgICAg
# IAogICAgICBGaWxlLmNobW9kKDA3NTUsIGxhdW5jaGVyLnBhdGgpCiAgICAg
# IAogICAgZW5kCiAgICAKICAgIGRlZiBzYXZlX3BrZ2NvbmZpZwogICAgICBp
# ZiBub3QgQHBrZ19jb25maWdzLmVtcHR5PwogICAgICAgIEBwa2dfY29uZmln
# cy5lYWNoIHsgfHBrZ19jb25maWd8CiAgICAgICAgICBGaWxlLm9wZW4oIHBr
# Z19jb25maWdbOnBhY2thZ2VfbmFtZV0rIi5wYyIsICJ3IikgZG8gfGZpbGV8
# CiAgICAgICAgICAgIGZpbGUgPDwgInByZWZpeD0iIDw8IEBhcmd1bWVudHNb
# InByZWZpeCJdIDw8ICJcbiIKICAgICAgICAgICAgZmlsZSA8PCAiZXhlY19w
# cmVmaXg9JHtwcmVmaXh9IiA8PCAiXG4iCiAgICAgICAgICAgIGZpbGUgPDwg
# ImxpYmRpcj0je3BrZ19jb25maWdbOmxpYmRpcl0ubmlsPyA/ICIke3ByZWZp
# eH0vbGliIiA6IHBrZ19jb25maWdbOmxpYmRpcl0gfSIgPDwgIlxuIgogICAg
# ICAgICAgICBmaWxlIDw8ICJpbmNsdWRlZGlyPSN7cGtnX2NvbmZpZ1s6aW5j
# bHVkZWRpcl0ubmlsPyA/ICIke3ByZWZpeH0vaW5jbHVkZSIgOiBwa2dfY29u
# ZmlnWzppbmNsdWRlZGlyXSB9IiA8PCAiXG5cblxuIgogICAgICAgICAgICAK
# ICAgICAgICAgICAgZmlsZSA8PCAiTmFtZTogIiA8PCBwa2dfY29uZmlnWzpw
# YWNrYWdlX25hbWVdIDw8ICJcbiIKICAgICAgICAgICAgZmlsZSA8PCAiRGVz
# Y3JpcHRpb246ICIgPDwgcGtnX2NvbmZpZ1s6ZGVzY3JpcHRpb25dIDw8ICJc
# biIKICAgICAgICAgICAgZmlsZSA8PCAiUmVxdWlyZXM6ICIgPDwgcGtnX2Nv
# bmZpZ1s6cmVxdWlyZXNdLmpvaW4oIiAiKSA8PCAiXG4iIGlmIHBrZ19jb25m
# aWdbOnJlcXVpcmVzXS5jbGFzcyA9PSBBcnJheQogICAgICAgICAgICBmaWxl
# IDw8ICJWZXJzaW9uOiAiIDw8IHBrZ19jb25maWdbOnZlcnNpb25dIDw8ICJc
# biIKICAgICAgICAgICAgCiAgICAgICAgICAgIGZpbGUgPDwgIkxpYnM6ICIg
# PDwgcGtnX2NvbmZpZ1s6bGlic10gPDwgIlxuIgogICAgICAgICAgICBmaWxl
# IDw8ICJDZmxhZ3M6ICIgPDwgcGtnX2NvbmZpZ1s6Y2ZsYWdzXSA8PCAiXG4i
# CiAgICAgICAgICBlbmQKICAgICAgICB9CiAgICAgIGVuZAogICAgZW5kCiAg
# ICAKICAgIAogICAgZGVmIGxvZyh0eHQpCiAgICAgIEZpbGUub3BlbihAbG9n
# X2ZpbGUsICJhIikgZG8gfGZ8CiAgICAgICAgZiA8PCB0eHQgPDwgIlxuIgog
# ICAgICBlbmQKICAgIGVuZAogICAgCiAgICBkZWYgdXNhZ2UKICAgICAgcHV0
# cyAiT3B0aW9uczoiCiAgICAgIEBvcHRpb25zLmVhY2ggeyB8b3B0aW9ufAog
# ICAgICAgIGNhc2Ugb3B0aW9uWzp0eXBlXQogICAgICAgICAgd2hlbiAic3Ry
# aW5nIgogICAgICAgICAgICBwcmludCAiXG4gIC0tI3tvcHRpb25bOm5hbWVd
# fT1bdmFsdWVdIgogICAgICAgICAgICBwcmludCAiXHRcdC4uLi4uLiN7b3B0
# aW9uWzpkZXNjcmlwdGlvbl19ICIKICAgICAgICAgICAgCiAgICAgICAgICAg
# IGlmIG5vdCBvcHRpb25bOmRlZmF1bHRdLm5pbD8KICAgICAgICAgICAgICBw
# cmludCAiW2RlZmF1bHQ9I3tvcHRpb25bOmRlZmF1bHRdfV0iCiAgICAgICAg
# ICAgIGVuZAogICAgICAgICAgd2hlbiAicGF0aCIKICAgICAgICAgICAgcHJp
# bnQgIlxuICAtLSN7b3B0aW9uWzpuYW1lXX09Wy9hL3BhdGhdIgogICAgICAg
# ICAgICBwcmludCAiXHRcdC4uLi4uLiN7b3B0aW9uWzpkZXNjcmlwdGlvbl19
# ICIKICAgICAgICAgICAgCiAgICAgICAgICAgIGlmIG5vdCBvcHRpb25bOmRl
# ZmF1bHRdLm5pbD8KICAgICAgICAgICAgICBwcmludCAiW2RlZmF1bHQ9I3tv
# cHRpb25bOmRlZmF1bHRdfV0iCiAgICAgICAgICAgIGVuZAogICAgICAgICAg
# d2hlbiAiYm9vbCIKICAgICAgICAgICAgcHJpbnQgIlxuICAtLXVzZS0je29w
# dGlvbls6bmFtZV19PVt5ZXN8bm9dIgogICAgICAgICAgICBpZiBub3Qgb3B0
# aW9uWzpkZWZhdWx0XS5uaWw/CiAgICAgICAgICAgICAgZGVmYXVsdCA9IG9w
# dGlvbls6ZGVmYXVsdF0KICAgICAgICAgICAgICBpZiBkZWZhdWx0ID09IHRy
# dWUgb3IgZGVmYXVsdCA9PSAieWVzIiBvciBkZWZhdWx0ID09ICJ0cnVlIgog
# ICAgICAgICAgICAgICAgZGVmYXVsdCA9ICJ5ZXMiCiAgICAgICAgICAgICAg
# ZWxzaWYgZGVmYXVsdCA9PSBmYWxzZSBvciBkZWZhdWx0ID09ICJubyIgb3Ig
# ZGVmYXVsdCA9PSAiZmFsc2UiCiAgICAgICAgICAgICAgICBkZWZhdWx0ID0g
# Im5vIgogICAgICAgICAgICAgIGVsc2UKICAgICAgICAgICAgICAgIGRlZmF1
# bHQgPSAibm8iCiAgICAgICAgICAgICAgZW5kCiAgICAgICAgICAgICAgCiAg
# ICAgICAgICAgICAgcHJpbnQgIlx0XHQuLi4uLi4je29wdGlvbls6ZGVzY3Jp
# cHRpb25dfSBbZGVmYXVsdD0je2RlZmF1bHR9XSIKICAgICAgICAgICAgZW5k
# CiAgICAgICAgICBlbHNlCiAgICAgICAgICAgIHByaW50ICJcbiAgLS0je29w
# dGlvbls6bmFtZV19IgogICAgICAgICAgICBwcmludCAiXHRcdC4uLi4uLiN7
# b3B0aW9uWzpkZXNjcmlwdGlvbl19IgogICAgICAgIGVuZAogICAgICB9CiAg
# ICAgIAogICAgICBwcmludCAiXG4gIC0td2l0aC1jb25maWctZnJvbnRlbmQ9
# W3BsYWlufHRrXSIKICAgICAgcHJpbnQgIlx0XHQuLi4uLi5TZXRzIHRoZSBm
# cm9udGVuZCB0byBjb25maWd1cmUgdGhlIGFwcGxpY2F0aW9uIFtkZWZhdWx0
# PXRrXSIKICAgICAgcHJpbnQgIlxuICAtLWhlbHAiCiAgICAgIHByaW50ICJc
# biAgLS12ZXJzaW9uIgogICAgICBwcmludCAiXHRcdC4uLi4uLlNob3dzIHRo
# aXMgbWVzc2FnZSIKICAgICAgCiAgICAgIHB1dHMgIiIKICAgIGVuZAogIGVu
# ZAplbmQKCmJlZ2luCiAgbG9hZCAic2V0dXAucmIiCnJlc2N1ZSBMb2FkRXJy
# b3IKICBGaWxlLm9wZW4ob2xkbG9jYXRpb24oInNldHVwLnJiIiksICJ3Iikg
# ZG8gfGZpbGV8CiAgICBmaWxlIDw8IEZpbGUucmVhZCgic2V0dXBfcmIudHBs
# IikgPDwgIlxuIgogIGVuZAogIHB1dHMgIj0+IEEgdGVtcGxhdGUgZmlsZSB3
# YXMgZ2VuZXJhdGVkIGluIHNldHVwLnJiLCBwbGVhc2UgZWRpdCBpdCBhbmQg
# cmVydW4gdGhlIGNvbmZpZ3VyZSBzY3JpcHQuIgogIGV4aXQoLTEpCmVuZAoK
# aWYgX19GSUxFX18gPT0gJDAKICBxb25mID0gUW9uZjo6Q29uZmlndXJlLm5l
# dwogIHFvbmYuZ2V0X2FyZ3MoQVJHVikKICBxb25mLmZpbmRfcGFja2FnZXMK
# ICBxb25mLnJ1bl90ZXN0cwogIAogIHFvbmYuc2F2ZSgiY29uZmlnLnByaSIp
# CiAgCiAgcW9uZi5nZW5lcmF0ZV9tYWtlZmlsZXMKZW5kCgoKCgAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHFv
# bmZpZ3VyZS9kZXRlY3Rvcy5yYgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAwMDAwNjQ0ADAwMDE3NTEAMDAwMTc1MQAwMDAwMDAwMTcyNAAx
# MDY3Mzc0MDMwMwAwMTMzNTIAIDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdXN0YXIgIABrcmF3ZWsA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAACnJlcXVpcmUgJ3JiY29uZmlnJwoKbW9kdWxlIFFv
# bmYKCmNsYXNzIERldGVjdE9TCglPUyA9IHsKCQkwID0+ICJVbmtub3duIiwK
# CQkxID0+ICJXaW5kb3dzIiwKCQkyID0+ICJVbml4IiwKCQkzID0+ICJMaW51
# eCIsCgkJMzEgPT4gIkRlYmlhbiIsCgkJMzExID0+ICJVYnVudHUiLAoJCTMy
# ID0+ICJHZW50b28iLAoJCTQgPT4gIk1hYyBPUyBYIgoJfQoJCglkZWYgc2Vs
# Zi5vcz8KCQlAb3MgPSAwCgkJCgkJaWYgOjpDb25maWc6OkNPTkZJR1siYnVp
# bGRfb3MiXSA9PSAibXN3aW4zMiIKCQkJcmV0dXJuIDEKCQllbHNlCgkJCUBv
# cyA9IDIKCQkJCgkJCWxpbnV4ID0gRGV0ZWN0T1MuZmluZF9saW51eAoJCQlA
# b3MgPSBsaW51eCBpZiBsaW51eCA+IDAKCQllbmQKCQkKCQkKCQlyZXR1cm4g
# QG9zCgllbmQKCQoJZGVmIHNlbGYuZmluZF9saW51eAoJCWN3ZCA9IERpci5n
# ZXR3ZAoJCURpci5jaGRpcigiL2V0Yy8iKQoJCQoJCURpclsiKnstLF99e3Zl
# cnNpb24scmVsZWFzZX0iXS5lYWNoIHsgfHBhdGh8CgkJCWlmIHBhdGguZG93
# bmNhc2UgPX4gL14oXHcqKSgtfF8pKHZlcnNpb258cmVsZWFzZSkvCgkJCQlE
# ZXRlY3RPUzo6T1MuZWFjaCB7IHxpZCwgb3N8CgkJCQkJaWYgb3MuZG93bmNh
# c2UgPT0gJDEKCQkJCQkJRGlyLmNoZGlyKGN3ZCkKCQkJCQkJCgkJCQkJCWlm
# ICQxID09ICJkZWJpYW4iCgkJCQkJCQlpZiBgYXB0LWNhY2hlIC12YC5pbmNs
# dWRlPygidWJ1bnR1IikKCQkJCQkJCQlyZXR1cm4gMzExCgkJCQkJCQllbmQK
# CQkJCQkJZW5kCgkJCQkJCQoJCQkJCQlyZXR1cm4gaWQKCQkJCQllbmQKCQkJ
# CX0KCQkJZW5kCgkJfQoJCQoJCURpci5jaGRpcihjd2QpCgkJCgkJaWYgYHVu
# YW1lYC5kb3duY2FzZSA9PSAibGludXgiCgkJCXJldHVybiAyCgkJZW5kCgkJ
# CgkJcmV0dXJuIDAKCWVuZAplbmQKCmVuZAoKaWYgX19GSUxFX18gPT0gJDAK
# CXB1dHMgUW9uZjo6RGV0ZWN0T1M6Ok9TW1FvbmY6OkRldGVjdE9TLm9zP10K
# ZW5kCgoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAHFvbmZpZ3VyZS9tYWtlZmlsZS5yYn4AAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAwMDAwNjQ0ADAwMDE3NTEAMDAwMTc1MQAwMDAwMDAw
# MjIwNAAxMDcxMjEyNjAwNwAwMTM0NzYAIDAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdXN0YXIgIABr
# cmF3ZWsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAADQpyZXF1aXJlICdyYmNvbmZpZycNCg0K
# bW9kdWxlIFFvbmYNCiAgY2xhc3MgTWFrZWZpbGUNCiAgICBkZWYgc2VsZi5m
# aW5kX21ha2VmaWxlcyhwYXRoKQ0KICAgICAgbWFrZWZpbGVzID0gW10NCiAg
# ICAgIERpci5mb3JlYWNoKHBhdGgpIHsgfGZ8DQogICAgICAgIGZpbGUgPSAi
# I3twYXRofS8je2Z9Ig0KICAgICAgICBpZiBGaWxlLnN0YXQoZmlsZSkuZGly
# ZWN0b3J5Pw0KICAgICAgICAgIGlmIG5vdCBmID1+IC9eXC4vDQogICAgICAg
# ICAgICBtYWtlZmlsZXMuY29uY2F0IGZpbmRfbWFrZWZpbGVzKGZpbGUpDQog
# ICAgICAgICAgZW5kDQogICAgICAgIGVsc2lmIGYuZG93bmNhc2UgPX4gL15t
# YWtlZmlsZSguZGVidWd8LnJlbGVhc2UpezAsMX0vDQogICAgICAgICAgbWFr
# ZWZpbGVzIDw8IGZpbGUNCiAgICAgICAgZW5kDQogICAgICB9DQogICAgICBt
# YWtlZmlsZXMNCiAgICBlbmQNCiAgICANCiAgICBkZWYgc2VsZi5vdmVycmlk
# ZShtYWtlZmlsZSwgZGVzdGRpciwgc3RhdHVzRmlsZSkNCiAgICAgIG5ld21h
# a2VmaWxlID0gIiINCiAgICAgIA0KICAgICAgcnVieV9iaW4gPSAiI3s6OkNv
# bmZpZzo6Q09ORklHWyJiaW5kaXIiXX0vI3s6OkNvbmZpZzo6Q09ORklHWyJy
# dWJ5X2luc3RhbGxfbmFtZSJdfSINCiAgICAgIA0KICAgICAgaWYgRmlsZTo6
# QUxUX1NFUEFSQVRPUg0KICAgICAgICBydWJ5X2Jpbi5nc3ViISgiLyIsIEZp
# bGU6OkFMVF9TRVBBUkFUT1IpDQogICAgICAgIGRlc3RkaXIuZ3N1YiEoIi8i
# LCBGaWxlOjpBTFRfU0VQQVJBVE9SKQ0KICAgICAgZW5kDQogICAgICANCiAg
# ICAgIEZpbGUub3BlbihtYWtlZmlsZSwgInIiKSB7IHxmfA0KICAgICAgICBs
# aW5lcyA9IGYucmVhZGxpbmVzDQogICAgICAJDQogICAgICAgIGxpbmVzLmVh
# Y2ggeyB8bGluZXwNCiAgICAgICAgICBpZiBsaW5lID1+IC9eSU5TVEFMTF9E
# SVIvDQogICAgICAgICAgCW5ld21ha2VmaWxlICs9ICJJTlNUQUxMX1JPT1Qg
# PSAje2Rlc3RkaXJ9Ig0KICAgICAgICAgIGVuZA0KICAgICAgICAgIG5ld21h
# a2VmaWxlICs9IGxpbmUNCiAgICAgICAgfQ0KICAgICAgfQ0KICAgICAgDQog
# ICAgICBGaWxlLm9wZW4obWFrZWZpbGUsICJ3IikgeyB8ZnwNCiAgICAgICAg
# ZiA8PCBuZXdtYWtlZmlsZQ0KICAgICAgfQ0KICAgIGVuZA0KICBlbmQNCmVu
# ZA0KDQoNCgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcW9uZmlndXJlL3BsYWluX2Zy
# b250ZW5kLnJiAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAwMDA2NDQA
# MDAwMTc1MQAwMDAxNzUxADAwMDAwMDAwNDY2ADEwNjczNzQwMzAzADAxNDU0
# NAAgMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAB1c3RhciAgAGtyYXdlawAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAK
# cmVxdWlyZSAnZnJvbnRlbmQnCgptb2R1bGUgUW9uZgoKY2xhc3MgUGxhaW5G
# cm9udGVuZCA8IEZyb250ZW5kCiAgZGVmIHBhcnNlKG9wdGlvbnMpCiAgICBv
# cHRpb25zLmVhY2ggeyB8b3B0aW9ufAogICAgICBpZiBvcHRpb25bOnZhbHVl
# XS50b19zLmVtcHR5PyBhbmQgb3B0aW9uWzpvcHRpb25hbF0gPT0gZmFsc2UK
# ICAgICAgICBwdXRzICI9PiBUaGUgb3B0aW9uICcje29wdGlvbls6bmFtZV19
# JyBpcyByZXF1aXJlZC4iCiAgICAgICAgcmV0dXJuIGZhbHNlCiAgICAgIGVu
# ZAogICAgfQogICAgCiAgICB0cnVlCiAgZW5kCmVuZAoKZW5kCgoKAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAHFvbmZpZ3VyZS9jb25maWcucmIAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwMDAwNjQ0ADAwMDE3NTEAMDAw
# MTc1MQAwMDAwMDAwNzA2NgAxMDcxMzc0NTAyNAAwMTMwMTIAIDAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAdXN0YXIgIABrcmF3ZWsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACnJlcXVpcmUgJ2Rl
# dGVjdG9zJwoKbW9kdWxlIFFvbmYKCmNsYXNzIFBhY2thZ2UKICBhdHRyX3Jl
# YWRlciA6aW5jbHVkZV9wYXRoLCA6bGlicywgOm1vZHVsZXMsIDpkZWZpbmVz
# LCA6bmFtZQogIGRlZiBpbml0aWFsaXplKG5hbWUpCiAgICBAaW5jbHVkZV9w
# YXRoID0gW10KICAgIEBsaWJzID0gW10KICAgIEBtb2R1bGVzID0gW10KICAg
# IEBkZWZpbmVzID0gW10KICAgIEBuYW1lID0gbmFtZQogIGVuZAogIAogIGRl
# ZiBhZGRfaW5jbHVkZV9wYXRoKHBhdGgpCiAgICBAaW5jbHVkZV9wYXRoIDw8
# IHBhdGgKICBlbmQKICAKICBkZWYgYWRkX2xpYihsaWIpCiAgICBAbGlicyA8
# PCBsaWIudG9fcwogIGVuZAogIAogIGRlZiBhZGRfcXRtb2R1bGUobW9kKQog
# ICAgQG1vZHVsZXMgPDwgbW9kCiAgZW5kCiAgCiAgZGVmIGFkZF9kZWZpbmUo
# ZGVmaW5lKQogICAgQGRlZmluZXMgPDwgZGVmaW5lCiAgZW5kCiAgCiAgZGVm
# IHdyaXRlKGYpCiAgICBpZiBub3QgQGluY2x1ZGVfcGF0aC5lbXB0eT8KICAg
# ICAgICBmIDw8ICJJTkNMVURFUEFUSCArPSAiIDw8IEBpbmNsdWRlX3BhdGgu
# dW5pcS5qb2luKCIgIikgPDwgIlxuIgogICAgZW5kCiAgICAKICAgIGlmIG5v
# dCBAbGlicy5lbXB0eT8KICAgICAgICBmIDw8ICJMSUJTICs9ICIgPDwgQGxp
# YnMudW5pcS5qb2luKCIgIikgPDwgIlxuIgogICAgZW5kCiAgICAKICAgIGlm
# IG5vdCBAbW9kdWxlcy5lbXB0eT8KICAgICAgICBmIDw8ICJRVCArPSAiIDw8
# IEBtb2R1bGVzLnVuaXEuam9pbigiICIpIDw8ICJcbiIKICAgIGVuZAogICAg
# CiAgICBpZiBub3QgQGRlZmluZXMuZW1wdHk/CiAgICAgICAgZiA8PCAiREVG
# SU5FUyArPSAiIDw8IEBkZWZpbmVzLnVuaXEuam9pbigiICIpIDw8ICJcbiIK
# ICAgIGVuZAogIGVuZAplbmQKCmNsYXNzIENvbmZpZyA8IFBhY2thZ2UKICBh
# dHRyX3JlYWRlciA6YnVpbGRfZGlyCiAgZGVmIGluaXRpYWxpemUKICAgIHN1
# cGVyKG5pbCkKICAgIEBxb25mX2ZlYXR1cmVzID0gW10KICAgIEBxb25mX3Bh
# Y2thZ2VzID0gW10KICAgIEBidWlsZF9kaXIgPSBvbGRsb2NhdGlvbigiIikr
# Ii9fYnVpbGRfIgogIGVuZAoKICBkZWYgYWRkX3FvbmZfcGFja2FnZShwa2cp
# CiAgICBAcW9uZl9wYWNrYWdlcyA8PCBwa2cKICBlbmQKICAKICBkZWYgYWRk
# X3FvbmZfZmVhdHVyZShmdHIpCiAgICBAcW9uZl9mZWF0dXJlcyA8PCBmdHIK
# ICBlbmQgCgogIGRlZiBpbmNsdWRlX3BhdGgKICAgIGluY3BhdGggPSBAaW5j
# bHVkZV9wYXRoCiAgICAKICAgIEBxb25mX3BhY2thZ2VzLmVhY2ggeyB8cGtn
# fAogICAgICAgIGluY3BhdGguY29uY2F0IHBrZy5pbmNsdWRlX3BhdGgKICAg
# IH0KICAgIAogICAgaW5jcGF0aAogIGVuZAogIAogIGRlZiBsaWJzCiAgICBs
# aWIgPSBAbGlicwogICAgQHFvbmZfcGFja2FnZXMuZWFjaCB7IHxwa2d8CiAg
# ICAgICAgbGliLmNvbmNhdCBwa2cubGlicwogICAgfQogICAgCiAgICBsaWIK
# ICBlbmQKICAKICBkZWYgbGlicmFyeV9wYXRoCiAgICBsaWJzX2EgPSBbXQoK
# ICAgIHNwbGl0dGVkID0gQGxpYnMuam9pbigiICIpLnNwbGl0KCIgIikKICAg
# IHNwbGl0dGVkLmVhY2ggeyB8c3B8CiAgICAgICAgaWYoIHNwID1+IC8tTCgu
# KylccyovICkKICAgICAgICAgICAgbGlic19hIDw8ICQxCiAgICAgICAgZW5k
# CiAgICB9CiAgICAKICAgIGxpYnNfYQogIGVuZAogIAogIGRlZiBkZWZpbmVz
# CiAgICBkZWZzID0gQGRlZmluZXMKICAgIAogICAgQHFvbmZfcGFja2FnZXMu
# ZWFjaCB7IHxwa2d8CiAgICAgICAgZGVmcy5jb25jYXQgcGtnLmRlZmluZXMK
# ICAgIH0KICAgIAogICAgZGVmcwogIGVuZAogIAogIGRlZiBzYXZlKHBhdGgp
# CiAgICBpZiBwYXRoWzBdLmNociA9PSBGaWxlOjpTRVBBUkFUT1IKICAgICAg
# ICBwYXRoID0gcGF0aAogICAgZWxzZQogICAgICAgIHBhdGggPSAiI3tEaXIu
# Z2V0d2R9LyN7cGF0aH0iCiAgICBlbmQKICAgIAogICAgRmlsZS5vcGVuKHBh
# dGgsICJ3IikgeyB8ZnwKICAgICAgICBmIDw8ICIjIEdlbmVyYXRlZCBhdXRv
# bWF0aWNhbGx5IGF0ICN7VGltZS5ub3d9ISBQTEVBU0UgRE8gTk9UIEVESVQg
# TUFOVUFMTFkhIjw8ICJcbiIKICAgICAgICAKICAgICAgICBmIDw8ICJQUk9K
# RUNUX0RJUiA9ICIgPDwgb2xkbG9jYXRpb24oIiIpIDw8ICJcbiIKICAgICAg
# ICBmIDw8ICJCVUlMRF9ESVIgPSAje0BidWlsZF9kaXJ9XG4iCiAgICAgICAg
# CiAgICAgICAgZiA8PCAnY29udGFpbnMoVEVNUExBVEUsICJhcHAiKSB7JyA8
# PCAiXG4iCiAgICAgICAgZiA8PCAiREVTVERJUiA9ICQkQlVJTERfRElSL2Jp
# blxuIgogICAgICAgIGYgPDwgJ30gZWxzZTpjb250YWlucyhURU1QTEFURSwg
# ImxpYiIpIHsnIDw8ICJcbiIKICAgICAgICBmIDw8ICJERVNURElSID0gJCRC
# VUlMRF9ESVIvbGliXG4iCiAgICAgICAgZiA8PCAnfSBlbHNlIHsnIDw8ICJc
# biIKICAgICAgICBmIDw8ICJERVNURElSID0gJCRCVUlMRF9ESVJcbiIKICAg
# ICAgICBmIDw8ICJ9XG5cblxuIgogICAgICAgIAogICAgICAgIGYgPDwgIkxJ
# QlMgKz0gLUwkJEJVSUxEX0RJUi9saWJcbiIKICAgICAgICBmIDw8ICJSRUxB
# VElWRV9QQVRIID0gJCRyZXBsYWNlKF9QUk9fRklMRV9QV0RfLCAkJFBST0pF
# Q1RfRElSLCAiIilcbiIKICAgICAgICBmIDw8ICJDVVJSRU5UX0JVSUxEX0RJ
# UiA9ICQkQlVJTERfRElSJCRSRUxBVElWRV9QQVRIXG4iCiAgICAgICAgCmlm
# IERldGVjdE9TLm9zPyAhPSAxICMgRGlzYWJsZWQgYmVjYXVzZSBXaW5kb3dz
# IHN1Y2tzLgogICAgICAgIGYgPDwgIk9CSkVDVFNfRElSID0gJCRDVVJSRU5U
# X0JVSUxEX0RJUlxuIgogICAgICAgIGYgPDwgIk1PQ19ESVIgPSAkJENVUlJF
# TlRfQlVJTERfRElSXG4iCiAgICAgICAgZiA8PCAiVUlfRElSID0gJCRDVVJS
# RU5UX0JVSUxEX0RJUlxuXG4iCmVuZAogICAgICAgIAogICAgICAgIHdyaXRl
# KGYpCiAgICAgICAgCiAgICAgICAgCiAgICAgICAgZiA8PCAiUU9ORl9GRUFU
# VVJFUyArPSAiIDw8IEBxb25mX3BhY2thZ2VzLm1hcCB7IHxlfCBlLm5hbWUg
# fS5qb2luKCIgIikgIDw8ICJcbiIKICAgICAgICBmIDw8ICJRT05GX0ZFQVRV
# UkVTICs9ICIgPDwgQHFvbmZfZmVhdHVyZXMuam9pbigiICIpICA8PCAiXG4i
# CiAgICAgICAgCiAgICAgICAgZiA8PCAiXG5kZWZpbmVUZXN0KGxpbmtfd2l0
# aCkgeyIgPDwgIlxuIgogICAgICAgIGYgPDwgIiAgcmV0ID0gZmFsc2VcbiIK
# ICAgICAgICBAcW9uZl9wYWNrYWdlcy5lYWNoIHsgfHFvbmZ8CiAgICAgICAg
# ICBmIDw8ICVAICBjb250YWlucygkJGxpc3QoJCRzcGxpdCgxKSkgLCIje3Fv
# bmYubmFtZX0iKSB7XG5ACiAgICAgICAgICBmIDw8ICIgICAgcmV0ID0gdHJ1
# ZVxuIgogICAgICAgICAgZiA8PCAiICAgIG1lc3NhZ2UoPT4gTGlua2luZyBx
# b25mIG1vZHVsZTogI3txb25mLm5hbWV9KVxuIgogICAgICAgICAgcW9uZi53
# cml0ZShmKQogICAgICAgICAgZiA8PCAiICB9XG4iCiAgICAgICAgfQogICAg
# ICAgIGYgPDwgIiAgZXhwb3J0KElOQ0xVREVQQVRIKVxuIgogICAgICAgIGYg
# PDwgIiAgZXhwb3J0KExJQlMpXG4iCiAgICAgICAgZiA8PCAiICBleHBvcnQo
# UVQpXG4iCiAgICAgICAgZiA8PCAiICBleHBvcnQoREVGSU5FUylcbiIKICAg
# ICAgICBmIDw8ICIgIHJldHVybigkJHJldClcbiIKICAgICAgICBmIDw8ICJ9
# XG4iCiAgICB9CiAgZW5kCmVuZAoKZW5kCgoAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHFvbmZpZ3VyZS9w
# a2djb25maWcucmIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAw
# MDAwNjQ0ADAwMDE3NTEAMDAwMTc1MQAwMDAwMDAwNjcxNAAxMDcyMzM0MzIy
# NAAwMTM1MTAAIDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAdXN0YXIgIABrcmF3ZWsAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAACgpyZXF1aXJlICdkZXRlY3RvcycKCm1vZHVsZSBRb25mCgpjbGFz
# cyBQa2dDb25maWdQYXJzZXIKICBkZWYgaW5pdGlhbGl6ZQogICAgQHZhcmlh
# YmxlcyA9IHt9CiAgICBAc3BlY3MgPSB7fQogIGVuZAogIAogIGRlZiBwYXJz
# ZShwYXRoKQogICAgRmlsZS5vcGVuKHBhdGgpIHsgfGZpbGV8CiAgICAgIGZp
# bGUuZWFjaCB7IHxsaW5lfAogICAgICAgIGlmIGxpbmUgPX4gLyhcdyspPSgu
# KikkLwogICAgICAgICAgQHZhcmlhYmxlc1skMV0gPSAkMgogICAgICAgIGVs
# c2lmIGxpbmUgPX4gLyhcdyspOlxzKC4qKSQvCiAgICAgICAgICBAc3BlY3Nb
# JDFdID0gJDIKICAgICAgICBlbmQKICAgICAgfQogICAgfQogIGVuZAogIAog
# IGRlZiB2YWx1ZSh2YXJpYWJsZSkKICAgIHZhbCA9IEB2YXJpYWJsZXNbdmFy
# aWFibGVdCiAgICB3aGlsZSB2YWwgPX4gLyhcJFx7KFx3KylcfSkvCiAgICAg
# IHZhbC5nc3ViISgkMSwgdmFsdWUoJDIpKQogICAgZW5kCiAgICAKICAgIHZh
# bAogIGVuZAogIAogIGRlZiBzcGVjKHZhcmlhYmxlKQogICAgdmFsID0gQHNw
# ZWNzW3ZhcmlhYmxlXQogICAgd2hpbGUgdmFsID1+IC8oXCRceyhcdyspXH0p
# LwogICAgICB2YWwuZ3N1YiEoJDEsIHZhbHVlKCQyKSkKICAgIGVuZAogICAg
# CiAgICB2YWwKICBlbmQKICAKICBkZWYgbGlicwogICAgc3BlYygiTGlicyIp
# CiAgZW5kCiAgCiAgZGVmIGNmbGFncwogICAgc3BlYygiQ2ZsYWdzIikKICBl
# bmQKICAKICBkZWYgcmVxdWlyZXMKICAgIHNwZWMoIlJlcXVpcmVzIikudG9f
# cwogIGVuZAplbmQKCmNsYXNzIFBrZ0NvbmZpZwogIGF0dHJfcmVhZGVyIDpp
# ZCwgOmxpYnMsIDpjZmxhZ3MsIDpyZXF1aXJlcwogIAogIGRlZiBpbml0aWFs
# aXplKGlkKQogICAgQGlkID0gaWQKICAgIEBzZWFyY2hfcGF0aCA9IFtdCiAg
# ICBAcmVxdWlyZXMgPSBbXQogIGVuZAogIAogIGRlZiBmaW5kKHBrZykKICAg
# IHBrZ3BhdGggPSBmaW5kX3BhdGhfZm9yX3BhY2thZ2UocGtnKQogICAgCiAg
# ICBpZiBub3QgcGtncGF0aC5uaWw/CiAgICAgIEBjZmxhZ3MgPSAiIgogICAg
# ICBAbGlicyA9ICIiCiAgICAgIHBhcnNlKHBrZ3BhdGgsIHBrZykKICAgICAg
# CiAgICAgIHJldHVybiB0cnVlCiAgICBlbmQKICAgIAogICAgZmFsc2UKICBl
# bmQKICAKICBkZWYgYWRkX3NlYXJjaF9wYXRoKHBhdGgpCiAgICBpZiBwYXRo
# LmtpbmRfb2Y/KEFycmF5KQogICAgICBAc2VhcmNoX3BhdGguY29uY2F0KHBh
# dGgpCiAgICBlbHNlCiAgICAgIEBzZWFyY2hfcGF0aCA8PCBwYXRoLnRvX3MK
# ICAgIGVuZAogIGVuZAogIAogIGRlZiBpbmNsdWRlX3BhdGgKICAgIGluY3Bh
# dGggPSBbXQogICAgY2ZsYWdzID0gQGNmbGFncy5kdXAKICAgIHdoaWxlIGNm
# bGFncyA9fiAvKC1JKChcdzopezAsMX0oW1wvXFxdXFMrKSspKS8KICAgICAg
# aW5jcGF0aCA8PCAkMgogICAgICBjZmxhZ3Muc3ViISgkMSwgIiIpCiAgICBl
# bmQKICAgIAogICAgaW5jcGF0aC51bmlxCiAgZW5kCiAgCiAgZGVmIGRlZmlu
# ZXMKICAgIGRlZmluZXMgPSBbXQogICAgY2ZsYWdzID0gQGNmbGFncy5kdXAK
# ICAgIHdoaWxlIGNmbGFncyA9fiAvKC1EXHMqKChcV1x3KykrKSkvCiAgICAg
# IGRlZmluZXMgPDwgJDIKICAgICAgZGVmaW5lcy5zdWIhKCQxLCAiIikKICAg
# IGVuZAogICAgCiAgICBkZWZpbmVzLnVuaXEKICBlbmQKICAKICBkZWYgZmlu
# ZF93aW4zMl9wYXRocwogICAgcGF0aHMgPSBmaW5kX2Vudl9wYXRocwogICAg
# CiAgICAKICAgIHBhdGhzCiAgZW5kCiAgCiAgZGVmIGZpbmRfdW5peF9wYXRo
# cwogICAgcGF0aHMgPSBmaW5kX2Vudl9wYXRocwogICAgaWYgRmlsZS5leGlz
# dHM/KCIvZXRjL2xkLnNvLmNvbmYiKQogICAgICBGaWxlLm9wZW4oIi9ldGMv
# bGQuc28uY29uZiIpIGRvIHxmaWxlfAogICAgICAgIGZpbGUuZWFjaCB7IHxs
# aW5lfAogICAgICAgICAgZGlyID0gbGluZS5zdHJpcCsiL3BrZ2NvbmZpZyIK
# ICAgICAgICAgIGlmIEZpbGUuZXhpc3RzPyhkaXIpCiAgICAgICAgICAgIHBh
# dGhzIDw8IGRpcgogICAgICAgICAgZW5kCiAgICAgICAgfQogICAgICBlbmQK
# ICAgIGVuZAogICAgCiAgICBwYXRocyA8PCAiL3Vzci9saWIvcGtnY29uZmln
# IiA8PCAiL3Vzci9sb2NhbC9saWIvcGtnY29uZmlnIiA8PCBFTlZbIkhPTUUi
# XS50b19zKyIvbG9jYWwvbGliL3BrZ2NvbmZpZyIKICAgIAogICAgcGF0aHMK
# ICBlbmQKICAKICBkZWYgZmluZF9lbnZfcGF0aHMKICAgIHNlcHBhdGggPSAo
# RGV0ZWN0T1Mub3M/ID09IDEpID8gIjsiIDogIjoiCiAgICAKICAgIHBhdGhz
# ID0gWyAiLiIgXQogICAgKEVOVlsiUEtHX0NPTkZJR19QQVRIIl0udG9fcytz
# ZXBwYXRoK0VOVlsiTERfTElCUkFSWV9QQVRIIl0udG9fcykuc3BsaXQoc2Vw
# cGF0aCkuZWFjaCB7IHxwYXRofAogICAgICBpZiBGaWxlLmV4aXN0cz8ocGF0
# aCkKICAgICAgICBwYXRocyA8PCBwYXRoCiAgICAgIGVuZAogICAgfQogICAg
# CiAgICBwYXRocwogIGVuZAogIAogIHByaXZhdGUKICBkZWYgcGFyc2UocGtn
# cGF0aCwgcGtnKQogICAgcmV0dXJuIGlmIHBrZ3BhdGgubmlsPwogICAgCiAg
# ICBwYXJzZXIgPSBQa2dDb25maWdQYXJzZXIubmV3CiAgICBwYXJzZXIucGFy
# c2UocGtncGF0aCkKICAgIAogICAgcGFyc2VyLnJlcXVpcmVzLnNwbGl0KC8s
# fFxzLykuZWFjaCB7IHxyZXF8CiAgICAgIHJlcS5zdHJpcCEKICAgICAgCiAg
# ICAgIHBhcnNlKGZpbmRfcGF0aF9mb3JfcGFja2FnZShyZXEpLCByZXEpCiAg
# ICB9CiAgICAKICAgIGNmbGFncyA9IHBhcnNlci5jZmxhZ3MKICAgIGxpYnMg
# PSBwYXJzZXIubGlicwogICAgCiAgICBpZiBpZCAhPSBwa2cgYW5kIG5vdCAo
# Y2ZsYWdzLmVtcHR5PyBvciBsaWJzLmVtcHR5PykKICAgICAgQHJlcXVpcmVz
# IDw8IHBrZwogICAgZW5kCiAgICAKICAgIEBjZmxhZ3MgPSAiI3tjZmxhZ3N9
# ICN7QGNmbGFnc30iCiAgICBAbGlicyA9ICIje2xpYnN9ICN7QGxpYnN9Igog
# ICAgCiAgICBuaWwKICBlbmQKICAKICBkZWYgZmluZF9wYXRoX2Zvcl9wYWNr
# YWdlKHBrZykKICAgIGlmIG5vdCBwa2cgPX4gL1wucGMkLwogICAgICBwa2cg
# Kz0gIi5wYyIKICAgIGVuZAogICAgCiAgICBwYXRocyA9IEBzZWFyY2hfcGF0
# aAogICAgCiAgICBpZiBEZXRlY3RPUy5vcz8gPT0gMSAjIFdpbmRvd3MKICAg
# ICAgcGF0aHMuY29uY2F0IGZpbmRfd2luMzJfcGF0aHMKICAgIGVsc2UKICAg
# ICAgcGF0aHMuY29uY2F0IGZpbmRfdW5peF9wYXRocwogICAgZW5kCiAgICAK
# ICAgIHBrZ3BhdGggPSBuaWwKICAgIHBhdGhzLmVhY2ggeyB8cGF0aHwKICAg
# ICAgdG1wcGF0aCA9IHBhdGgrIi8iK3BrZwogICAgICBpZiBGaWxlLmV4aXN0
# cz8odG1wcGF0aCkKICAgICAgICBwa2dwYXRoID0gdG1wcGF0aAogICAgICBl
# bHNpZiAkREVCVUcKICAgICAgICBwdXRzICJGYWlsZWQ6ICN7dG1wcGF0aH0i
# CiAgICAgIGVuZAogICAgfQogICAgCiAgICBwa2dwYXRoCiAgZW5kCmVuZAoK
# ZW5kCgoKaWYgJDAgPT0gX19GSUxFX18KICBwa2djb25maWcgPSBRb25mOjpQ
# a2dDb25maWcubmV3CiAgcGtnY29uZmlnLmZpbmQoInRlc3RfcW9uZiIpCiAg
# CiAgcHV0cyBwa2djb25maWcubGlicwplbmQKCgAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABxb25maWd1cmUv
# dGtfZnJvbnRlbmQucmIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# MDAwMDY0NAAwMDAxNzUxADAwMDE3NTEAMDAwMDAwMDYyNzMAMTA2NzM3NDAz
# MDMAMDE0MDYxACAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAHVzdGFyICAAa3Jhd2VrAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAoKCgpyZXF1aXJlICdmcm9udGVuZCcKCm1vZHVsZSBRb25mCiAg
# Y2xhc3MgVGtGcm9udGVuZCA8IEZyb250ZW5kCiAgICBkZWYgaW5pdGlhbGl6
# ZQogICAgZW5kCiAgICAKICAgIGRlZiBwYXJzZShvcHRzKQogICAgICByb290
# ID0gVGtSb290Lm5ldygpIHsgdGl0bGUgIkNvbmZpZ3VyZS4uLiIgfQogICAg
# ICBvcHRzLmVhY2ggeyB8b3B0aW9ufAogICAgICAgIGxhYmVsID0gVGtMYWJl
# bC5uZXcocm9vdCkgeyB0ZXh0IG9wdGlvbls6bmFtZV0gfQogICAgICAgIAog
# ICAgICAgIGNhc2Ugb3B0aW9uWzp0eXBlXS50b19zLmRvd25jYXNlCiAgICAg
# ICAgICB3aGVuICJzdHJpbmciCiAgICAgICAgICAgIGVudHJ5ID0gVGtFbnRy
# eS5uZXcocm9vdCkKICAgICAgICAgICAgZW50cnkuaW5zZXJ0KDAsIChvcHRp
# b25bOnZhbHVlXS5uaWw/ID8gb3B0aW9uWzpkZWZhdWx0XS50b19zIDogb3B0
# aW9uWzp2YWx1ZV0udG9fcyAgKSkKICAgICAgICAgICAgVGtHcmlkLmdyaWQo
# bGFiZWwsIGVudHJ5LCAiY29sdW1uc3BhbiIgPT4gMikKICAgICAgICAgICAg
# CiAgICAgICAgICAgIG9wdGlvbls6aW5wdXRdID0gZW50cnkKICAgICAgICAg
# IHdoZW4gInBhdGgiCiAgICAgICAgICAgIGVudHJ5ID0gVGtFbnRyeS5uZXco
# cm9vdCkKICAgICAgICAgICAgZW50cnkuaW5zZXJ0KDAsIChvcHRpb25bOnZh
# bHVlXS5uaWw/ID8gb3B0aW9uWzpkZWZhdWx0XS50b19zIDogb3B0aW9uWzp2
# YWx1ZV0udG9fcyAgKSkKICAgICAgICAgICAgCiAgICAgICAgICAgIGdldEZp
# bGUgPSBUa0J1dHRvbi5uZXcocm9vdCkgewogICAgICAgICAgICAgIHRleHQg
# IkJyb3dzZS4uLiIKICAgICAgICAgICAgICBjb21tYW5kIHsKICAgICAgICAg
# ICAgICAgIHZhbCA9IFRrLmNob29zZURpcmVjdG9yeQogICAgICAgICAgICAg
# ICAgCiAgICAgICAgICAgICAgICBlbnRyeS52YWx1ZSA9IHZhbCBpZiBub3Qg
# dmFsLnRvX3MuZW1wdHk/CiAgICAgICAgICAgICAgfQogICAgICAgICAgICB9
# CiAgICAgICAgICAgIFRrR3JpZC5ncmlkKGxhYmVsLCBlbnRyeSwgZ2V0Rmls
# ZSwgImNvbHVtbnNwYW4iID0+IDIpCiAgICAgICAgICAgIAogICAgICAgICAg
# ICBvcHRpb25bOmlucHV0XSA9IGVudHJ5CiAgICAgICAgICBlbHNlCiAgICAg
# ICAgICAgIGdpdmVuID0gKG9wdGlvbls6dmFsdWVdLm5pbD8gPyBvcHRpb25b
# OmRlZmF1bHRdLnRvX3MgOiBvcHRpb25bOnZhbHVlXS50b19zICApCiAgICAg
# ICAgICAgIAogICAgICAgICAgICBpZiBnaXZlbiA9PSAiZmFsc2UiIG9yIGdp
# dmVuID09ICJubyIgb3IgZ2l2ZW4uZW1wdHk/CiAgICAgICAgICAgICAgdmFs
# dWUgPSAwCiAgICAgICAgICAgIGVsc2UKICAgICAgICAgICAgICB2YWx1ZSA9
# IDEKICAgICAgICAgICAgZW5kCiAgICAgICAgICAgIAogICAgICAgICAgICBt
# eWNoZWNrID0gVGtWYXJpYWJsZS5uZXcodmFsdWUpCiAgICAgICAgICAgIGNo
# ZWNrID0gVGtDaGVja0J1dHRvbi5uZXcocm9vdCkgewogICAgICAgICAgICAg
# IHZhcmlhYmxlIG15Y2hlY2sKICAgICAgICAgICAgfQogICAgICAgICAgICAK
# ICAgICAgICAgICAgVGtHcmlkLmdyaWQobGFiZWwsIGNoZWNrLCAiY29sdW1u
# c3BhbiIgPT4gMikKICAgICAgICAgICAgCiAgICAgICAgICAgIG9wdGlvbls6
# aW5wdXRdID0gbXljaGVjawogICAgICAgIGVuZAogICAgICB9CiAgICAgIAog
# ICAgICBvayA9IFRrQnV0dG9uLm5ldyhyb290KSB7CiAgICAgICAgdGV4dCAi
# T2siCiAgICAgICAgY29tbWFuZCAgewogICAgICAgICAgb2sgPSB0cnVlCiAg
# ICAgICAgICByZXF1aXJlZF9maWVsZHMgPSBbXQogICAgICAgICAgCiAgICAg
# ICAgICBvcHRzLmVhY2ggeyB8b3B0aW9ufAogICAgICAgICAgICB2YWx1ZSA9
# IG9wdGlvbls6ZGVmYXVsdF0KICAgICAgICAgICAgY2FzZSBvcHRpb25bOnR5
# cGVdLmRvd25jYXNlCiAgICAgICAgICAgICAgd2hlbiAic3RyaW5nIgogICAg
# ICAgICAgICAgICAgdmFsdWUgPSBvcHRpb25bOmlucHV0XS52YWx1ZQogICAg
# ICAgICAgICAgIHdoZW4gInBhdGgiCiAgICAgICAgICAgICAgICB2YWx1ZSA9
# IEZpbGUuZXhwYW5kX3BhdGgob3B0aW9uWzppbnB1dF0udmFsdWUpCiAgICAg
# ICAgICAgICAgZWxzZQogICAgICAgICAgICAgICAgdmFsdWUgPSBvcHRpb25b
# OmlucHV0XS52YWx1ZQogICAgICAgICAgICBlbmQKICAgICAgICAgICAgCiAg
# ICAgICAgICAgIGlmIG9wdGlvbls6b3B0aW9uYWxdID09IGZhbHNlIGFuZCB2
# YWx1ZS50b19zLmVtcHR5PwogICAgICAgICAgICAgIHJlcXVpcmVkX2ZpZWxk
# cyA8PCBvcHRpb25bOm5hbWVdCiAgICAgICAgICAgICAgb2sgPSBmYWxzZQog
# ICAgICAgICAgICBlbHNlCiAgICAgICAgICAgICAgb3B0aW9uWzp2YWx1ZV0g
# PSB2YWx1ZQogICAgICAgICAgICBlbmQKICAgICAgICAgICAgCiAgICAgICAg
# ICAgIG9rID0gb2sgJiYgdHJ1ZQogICAgICAgICAgfQogICAgICAgICAgCiAg
# ICAgICAgICBpZiBvawogICAgICAgICAgICBUay5leGl0CiAgICAgICAgICBl
# bHNlCiAgICAgICAgICAgIFRrV2FybmluZy5zaG93KCJUaGUgZmllbGRzICcj
# e3JlcXVpcmVkX2ZpZWxkcy5qb2luKCIsICIpfScgYXJlIHJlcXVpcmVkLiIp
# CiAgICAgICAgICBlbmQKICAgICAgICB9CiAgICAgIH0KICAgICAgCiAgICAg
# IGNhbmNlbCA9IFRrQnV0dG9uLm5ldyhyb290KSB7CiAgICAgICAgdGV4dCAi
# Q2FuY2VsIgogICAgICAgIGNvbW1hbmQgIHsKICAgICAgICAgIFRrLmV4aXQK
# ICAgICAgICAgIGV4aXQgMAogICAgICAgIH0KICAgICAgfQogICAgICAgIAog
# ICAgICBUa0dyaWQuZ3JpZChvaywgY2FuY2VsLCAiY29sdW1uc3BhbiIgPT4g
# MykKICAgICAgCiAgICAgIFRrLm1haW5sb29wCiAgICAgIAogICAgICB0cnVl
# CiAgICBlbmQKICBlbmQKZW5kCgoKaWYgX19GSUxFX18gPT0gJDAgIyBUZXN0
# CiAgb3B0cyA9IFtdCiAgb3B0cyA8PCB7Om5hbWUgPT4gIm5hbWUxIiwgOnR5
# cGUgPT4gIlN0cmluZyIsIDpkZWZhdWx0ID0+ICJ2YWx1ZSIsIDpvcHRpb25h
# bCA9PiBmYWxzZSB9CiAgb3B0cyA8PCB7Om5hbWUgPT4gIm5hbWU0IiwgOnR5
# cGUgPT4gIlBhdGgiLCA6ZGVmYXVsdCA9PiAidmFsdWUiIH0KICBvcHRzIDw8
# IHs6bmFtZSA9PiAibmFtZTIiLCA6dHlwZSA9PiAiQm9vbCIsIDpkZWZhdWx0
# ID0+IHRydWUgfQogIG9wdHMgPDwgezpuYW1lID0+ICJuYW1lMyIsIDp0eXBl
# ID0+ICJWb2lkIiwgOmRlZmF1bHQgPT4gZmFsc2UgfQogIAogIHRrID0gUW9u
# Zjo6VGtGcm9udGVuZC5uZXcKICB0ay5wYXJzZShvcHRzKQogIAogIHB1dHMg
# b3B0cy5maXJzdFs6dmFsdWVdCmVuZAoKCgoAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcW9uZmlndXJl
# L3Rlc3QucmJ+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# ADAwMDA2NDQAMDAwMTc1MQAwMDAxNzUxADAwMDAwMDA1MTc3ADEwNjczMDIy
# NzA0ADAxMjcyMQAgMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB1c3RhciAgAGtyYXdlawAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAByZXF1aXJlICd0bXBkaXInCgptb2R1bGUgUW9uZgogIGNsYXNz
# IFRlc3QKICAgIGF0dHJfcmVhZGVyIDppbmNsdWRlX3BhdGgsIDpsaWJzLCA6
# ZGVmaW5lcwogICAgCiAgICBkZWYgaW5pdGlhbGl6ZShvcHRpb25zKQogICAg
# ICBAb3B0aW9ucyA9IG9wdGlvbnMKICAgICAgCiAgICAgIEBpbmNsdWRlX3Bh
# dGggPSBbXQogICAgICBAbGlicyA9IFtdCiAgICAgIEBkZWZpbmVzID0gW10K
# ICAgICAgCiAgICAgIEBpbmNsdWRlX3BhdGggPSBvcHRpb25zWzppbmNsdWRl
# cGF0aF0gaWYgbm90IG9wdGlvbnNbOmluY2x1ZGVwYXRoXS5uaWw/CiAgICAg
# IEBsaWJzIDw8IG9wdGlvbnNbOmxpYnNdIGlmIG5vdCBvcHRpb25zWzpsaWJz
# XS5uaWw/CiAgICAgIEBkZWZpbmVzID0gb3B0aW9uc1s6ZGVmaW5lc10gaWYg
# bm90IG9wdGlvbnNbOmRlZmluZXNdLm5pbD8KICAgIGVuZAogICAgCiAgICBk
# ZWYgcnVuKHFtYWtlKQogICAgICAjIEdlbmVyYXRlCiAgICAgIHJldCA9IGZh
# bHNlCiAgICAgIGN3ZCA9IERpci5nZXR3ZAogICAgICAKICAgICAgcW9uZnRl
# c3RfZGlyID0gIiN7RGlyOjp0bXBkaXJ9L3FvbmZfdGVzdHMuI3tQcm9jZXNz
# LnBpZH0iCiAgICAgIGJhc2VwYXRoID0gcW9uZnRlc3RfZGlyKyIvdGVzdF8j
# e1Byb2Nlc3MucGlkfSIKICAgICAgCiAgICAgIGZpbGVwYXRoID0gYmFzZXBh
# dGgrIi5jcHAiCiAgICAgIHByb3BhdGggPSBiYXNlcGF0aCsiLnBybyIKICAg
# ICAgdGFyZ2V0ID0gInRlc3RfI3tQcm9jZXNzLnBpZH1fYXBwIgogICAgICAK
# ICAgICAgYmVnaW4KICAgICAgICBEaXIubWtkaXIocW9uZnRlc3RfZGlyKSBp
# ZiBub3QgRmlsZS5leGlzdHM/KHFvbmZ0ZXN0X2RpcikKICAgICAgICBGaWxl
# Lm9wZW4oZmlsZXBhdGgsICJ3IikgeyB8ZnwKICAgICAgICAgIAogICAgICAg
# ICAgd3JpdGVfdGVzdChmKQogICAgICAgICAgCiAgICAgICAgICBGaWxlLm9w
# ZW4ocHJvcGF0aCwgInciKSB7IHxwZnwKICAgICAgICAgICAgcGYgPDwgIlNP
# VVJDRVMgKz0gIiA8PCBmaWxlcGF0aCA8PCAiXG4iCiAgICAgICAgICAgIHBm
# IDw8ICJURU1QTEFURSA9IGFwcCIgPDwgIlxuIgogICAgICAgICAgICBwZiA8
# PCAiVEFSR0VUID0gIiA8PCAiXG4iCiAgICAgICAgICAgIHBmIDw8ICJJTkNM
# VURFUEFUSCArPSAiIDw8IEBpbmNsdWRlX3BhdGguam9pbigiICIpIDw8ICJc
# biIKICAgICAgICAgICAgcGYgPDwgIkxJQlMgKz0gIiA8PCBAbGlicy5qb2lu
# KCIgIikgPDwgIlxuIgogICAgICAgICAgICBwZiA8PCAiREVGSU5FUyArPSAi
# IDw8IEBkZWZpbmVzLmpvaW4oIiAiKSA8PCAiXG4iCiAgICAgICAgICB9CiAg
# ICAgICAgfQogICAgICAgIAogICAgICAgIAogICAgICAgIERpci5jaGRpcihx
# b25mdGVzdF9kaXIpCiAgICAgICAgCiAgICAgICAgcW1ha2UucnVuKHByb3Bh
# dGgpCiAgICAgICAgcmV0ID0gcW1ha2UuY29tcGlsZQogICAgICAgIHFtYWtl
# LmNsZWFuX3VwCiAgICAgICAgCiAgICAgICAgRGlyLmNoZGlyKGN3ZCkKICAg
# ICAgICAKICAgICAgICBGaWxlLnVubGluayh0YXJnZXQpIGlmIEZpbGUuZXhp
# c3RzPyh0YXJnZXQpICMgRklYTUU6IHdpbmRvd3MKICAgICAgICBGaWxlLnVu
# bGluayhwcm9wYXRoKSBpZiBGaWxlLmV4aXN0cz8ocHJvcGF0aCkKICAgICAg
# ICBGaWxlLnVubGluayhmaWxlcGF0aCkgaWYgRmlsZS5leGlzdHM/KGZpbGVw
# YXRoKQogICAgICAgIEZpbGVVdGlscy5ybV9yZihxb25mdGVzdF9kaXIpCiAg
# ICAgICAgCiAgICAgIHJlc2N1ZSA9PiBlCiAgICAgICAgcHV0cyBlCiAgICAg
# ICAgcHV0cyBlLmJhY2t0cmFjZQogICAgICAgIAogICAgICAgIERpci5jaGRp
# cihjd2QpCiAgICAgICAgRmlsZS51bmxpbmsodGFyZ2V0KSBpZiBGaWxlLmV4
# aXN0cz8odGFyZ2V0KSAjIEZJWE1FOiB3aW5kb3dzCiAgICAgICAgRmlsZS51
# bmxpbmsocHJvcGF0aCkgaWYgRmlsZS5leGlzdHM/KHByb3BhdGgpCiAgICAg
# ICAgRmlsZS51bmxpbmsoZmlsZXBhdGgpIGlmIEZpbGUuZXhpc3RzPyhmaWxl
# cGF0aCkKICAgICAgICBGaWxlVXRpbHMucm1fcmYocW9uZnRlc3RfZGlyKQog
# ICAgICAgIAogICAgICAgIHJldHVybiBmYWxzZQogICAgICBlbnN1cmUKICAg
# ICAgZW5kCiAgICAgIAogICAgICByZXQKICAgIGVuZAogICAgCiAgICBkZWYg
# YWRkX2luY2x1ZGVfcGF0aChpbmNwYXRoKQogICAgICBAaW5jbHVkZV9wYXRo
# IDw8IGluY3BhdGgKICAgIGVuZAogICAgCiAgICBkZWYgYWRkX2xpYihsaWIp
# CiAgICAgIEBsaWJzIDw8IGxpYgogICAgZW5kCiAgICAKICAgIGRlZiBhZGRf
# ZGVmaW5lKGRlZmluZSkKICAgICAgQGRlZmluZXMgPDwgZGVmaW5lCiAgICBl
# bmQKICAgIAogICAgcHJpdmF0ZQogICAgZGVmIHdyaXRlX3Rlc3QoZikKICAg
# ICAgaGVhZGVycyA9ICIiCiAgICAgIGlmIG5vdCBAb3B0aW9uc1s6aGVhZGVy
# c10ubmlsPwogICAgICAgIEBvcHRpb25zWzpoZWFkZXJzXS5lYWNoIHsgfGhl
# YWRlcnwKICAgICAgICAgIGhlYWRlcnMgKz0gIiNpbmNsdWRlIDwje2hlYWRl
# cn0+XG4iCiAgICAgICAgfQogICAgICBlbmQKICAgICAgY29kZSA9ICVACmlu
# dCBtYWluKCkKewogIHJldHVybiAwOwp9CkAKICAgICAgaWYgbm90IEBvcHRp
# b25zWzpjdXN0b21dLm5pbD8KICAgICAgICBjb2RlID0gQG9wdGlvbnNbOmN1
# c3RvbV0KICAgICAgZW5kCiAgICAgIAogICAgICBmIDw8IGhlYWRlcnMgPDwg
# IlxuIiA8PCBjb2RlIDw8ICJcbiIKICAgIGVuZAogIGVuZAplbmQKCgAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAABxb25maWd1cmUvdGVzdC5yYgAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMDAwMDY0NAAwMDAxNzUx
# ADAwMDE3NTEAMDAwMDAwMDYzMjAAMTA3MDIzMTMyMjMAMDEyNTAyACAwAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAHVzdGFyICAAa3Jhd2VrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHJlcXVpcmUg
# J3RtcGRpcicKcmVxdWlyZSAnZmlsZXV0aWxzJwoKbW9kdWxlIFFvbmYKICBj
# bGFzcyBUZXN0CiAgICBhdHRyX3JlYWRlciA6aW5jbHVkZV9wYXRoLCA6bGli
# cywgOmRlZmluZXMsIDppZAogICAgCiAgICBkZWYgaW5pdGlhbGl6ZShvcHRp
# b25zLCBjb25maWcsIGxvZ19maWxlID0gbmlsKQogICAgICBAb3B0aW9ucyA9
# IG9wdGlvbnMKICAgICAgCiAgICAgIEBpbmNsdWRlX3BhdGggPSBbXQogICAg
# ICBAbGlicyA9IFtdCiAgICAgIEBkZWZpbmVzID0gW10KICAgICAgQGlkID0g
# b3B0aW9uc1s6aWRdCiAgICAgIAogICAgICBAaW5jbHVkZV9wYXRoID0gb3B0
# aW9uc1s6aW5jbHVkZXBhdGhdIGlmIG5vdCBvcHRpb25zWzppbmNsdWRlcGF0
# aF0ubmlsPwogICAgICBAbGlicyA8PCBvcHRpb25zWzpsaWJzXSBpZiBub3Qg
# b3B0aW9uc1s6bGlic10ubmlsPwogICAgICBAZGVmaW5lcyA9IG9wdGlvbnNb
# OmRlZmluZXNdIGlmIG5vdCBvcHRpb25zWzpkZWZpbmVzXS5uaWw/CiAgICAg
# IAogICAgICBAY29uZmlnID0gY29uZmlnCiAgICAgIAogICAgICBAbG9nX2Zp
# bGUgPSBsb2dfZmlsZQogICAgZW5kCiAgICAKICAgIGRlZiBydW4ocW1ha2Up
# CiAgICAgICMgR2VuZXJhdGUKICAgICAgcmV0ID0gZmFsc2UKICAgICAgY3dk
# ID0gRGlyLmdldHdkCiAgICAgIAogICAgICBxb25mdGVzdF9kaXIgPSAiI3tE
# aXI6OnRtcGRpcn0vcW9uZl90ZXN0cy4je1Byb2Nlc3MucGlkfSIKICAgICAg
# YmFzZXBhdGggPSBxb25mdGVzdF9kaXIrIi90ZXN0XyN7UHJvY2Vzcy5waWR9
# IgogICAgICAKICAgICAgZmlsZXBhdGggPSBiYXNlcGF0aCsiLmNwcCIKICAg
# ICAgcHJvcGF0aCA9IGJhc2VwYXRoKyIucHJvIgogICAgICB0YXJnZXQgPSAi
# dGVzdF8je1Byb2Nlc3MucGlkfV9hcHAiCiAgICAgIAogICAgICBiZWdpbgog
# ICAgICAgIERpci5ta2Rpcihxb25mdGVzdF9kaXIpIGlmIG5vdCBGaWxlLmV4
# aXN0cz8ocW9uZnRlc3RfZGlyKQogICAgICAgIEZpbGUub3BlbihmaWxlcGF0
# aCwgInciKSB7IHxmfAogICAgICAgICAgCiAgICAgICAgICB3cml0ZV90ZXN0
# KGYpCiAgICAgICAgICAKICAgICAgICAgIEZpbGUub3Blbihwcm9wYXRoLCAi
# dyIpIHsgfHBmfAogICAgICAgICAgICBwZiA8PCAiU09VUkNFUyArPSAiIDw8
# IGZpbGVwYXRoIDw8ICJcbiIKICAgICAgICAgICAgcGYgPDwgIlRFTVBMQVRF
# ID0gYXBwIiA8PCAiXG4iCiAgICAgICAgICAgIHBmIDw8ICJUQVJHRVQgPSAi
# IDw8ICJcbiIKICAgICAgICAgICAgcGYgPDwgIklOQ0xVREVQQVRIICs9ICIg
# PDwgQGluY2x1ZGVfcGF0aC5qb2luKCIgIikgPDwgIiAiIDw8IEBjb25maWcu
# aW5jbHVkZV9wYXRoLmpvaW4oIiAiKSA8PCAiXG4iCiAgICAgICAgICAgIHBm
# IDw8ICJMSUJTICs9ICIgPDwgQGxpYnMuam9pbigiICIpIDw8ICIgIiA8PCAg
# QGNvbmZpZy5saWJzLmpvaW4oIiAiKSA8PCAiXG4iCiAgICAgICAgICAgIHBm
# IDw8ICJERUZJTkVTICs9ICIgPDwgQGRlZmluZXMuam9pbigiICIpIDw8ICIg
# IiA8PCBAY29uZmlnLmRlZmluZXMuam9pbigiICIpIDw8ICJcbiIKICAgICAg
# ICAgICAgcGYgPDwgIkNPTkZJRyAtPSBkZWJ1ZyIgPDwgIlxuIgogICAgICAg
# ICAgICBwZiA8PCAiQ09ORklHICo9IHJlbGVhc2UiIDw8ICJcbiIKICAgICAg
# ICAgICAgcGYgPDwgIkRFU1RESVIgPSAje3FvbmZ0ZXN0X2Rpcn0iIDw8ICJc
# biIKICAgICAgICAgIH0KICAgICAgICB9CiAgICAgICAgCiAgICAgICAgCiAg
# ICAgICAgRGlyLmNoZGlyKHFvbmZ0ZXN0X2RpcikKICAgICAgICAKICAgICAg
# ICBxbWFrZS5ydW4ocHJvcGF0aCkKICAgICAgICByZXQgPSBxbWFrZS5jb21w
# aWxlCiAgICAgICAgcW1ha2UuY2xlYW5fdXAKICAgICAgICAKICAgICAgICBE
# aXIuY2hkaXIoY3dkKQogICAgICAgIAogICAgICAgIEZpbGUudW5saW5rKHRh
# cmdldCkgaWYgRmlsZS5leGlzdHM/KHRhcmdldCkgIyBGSVhNRTogd2luZG93
# cwogICAgICAgIEZpbGUudW5saW5rKHByb3BhdGgpIGlmIEZpbGUuZXhpc3Rz
# Pyhwcm9wYXRoKQogICAgICAgIEZpbGUudW5saW5rKGZpbGVwYXRoKSBpZiBG
# aWxlLmV4aXN0cz8oZmlsZXBhdGgpCiAgICAgICAgRmlsZVV0aWxzLnJtX3Jm
# KHFvbmZ0ZXN0X2RpcikKICAgICAgICAKICAgICAgcmVzY3VlID0+IGUKICAg
# ICAgICBwdXRzIGUKICAgICAgICBwdXRzIGUuYmFja3RyYWNlCiAgICAgICAg
# CiAgICAgICAgRGlyLmNoZGlyKGN3ZCkKICAgICAgICBGaWxlLnVubGluayh0
# YXJnZXQpIGlmIEZpbGUuZXhpc3RzPyh0YXJnZXQpICMgRklYTUU6IHdpbmRv
# d3MKICAgICAgICBGaWxlLnVubGluayhwcm9wYXRoKSBpZiBGaWxlLmV4aXN0
# cz8ocHJvcGF0aCkKICAgICAgICBGaWxlLnVubGluayhmaWxlcGF0aCkgaWYg
# RmlsZS5leGlzdHM/KGZpbGVwYXRoKQogICAgICAgIEZpbGVVdGlscy5ybV9y
# Zihxb25mdGVzdF9kaXIpCiAgICAgICAgCiAgICAgICAgcmV0dXJuIGZhbHNl
# CiAgICAgIGVuc3VyZQogICAgICBlbmQKICAgICAgCiAgICAgIHJldAogICAg
# ZW5kCiAgICAKICAgIGRlZiBhZGRfaW5jbHVkZV9wYXRoKGluY3BhdGgpCiAg
# ICAgIEBpbmNsdWRlX3BhdGggPDwgaW5jcGF0aAogICAgZW5kCiAgICAKICAg
# IGRlZiBhZGRfbGliKGxpYikKICAgICAgQGxpYnMgPDwgbGliCiAgICBlbmQK
# ICAgIAogICAgZGVmIGFkZF9kZWZpbmUoZGVmaW5lKQogICAgICBAZGVmaW5l
# cyA8PCBkZWZpbmUKICAgIGVuZAogICAgCiAgICBwcml2YXRlCiAgICBkZWYg
# d3JpdGVfdGVzdChmKQogICAgICBoZWFkZXJzID0gIiIKICAgICAgaWYgbm90
# IEBvcHRpb25zWzpoZWFkZXJzXS5uaWw/CiAgICAgICAgQG9wdGlvbnNbOmhl
# YWRlcnNdLmVhY2ggeyB8aGVhZGVyfAogICAgICAgICAgaGVhZGVycyArPSAi
# I2luY2x1ZGUgPCN7aGVhZGVyfT5cbiIKICAgICAgICB9CiAgICAgIGVuZAog
# ICAgICBjb2RlID0gJUAKaW50IG1haW4oKQp7CiAgcmV0dXJuIDA7Cn0KQAog
# ICAgICBpZiBub3QgQG9wdGlvbnNbOmN1c3RvbV0ubmlsPwogICAgICAgIGNv
# ZGUgPSBAb3B0aW9uc1s6Y3VzdG9tXQogICAgICBlbmQKICAgICAgCiAgICAg
# IGYgPDwgaGVhZGVycyA8PCAiXG4iIDw8IGNvZGUgPDwgIlxuIgogICAgICAK
# ICAgICAgaWYoIG5vdCBAbG9nX2ZpbGUubmlsPyApCiAgICAgICAgRmlsZS5v
# cGVuKEBsb2dfZmlsZSwgImEiKSBkbyB8bGZpbGV8CiAgICAgICAgICBsZmls
# ZSA8PCAiXG5cblxuLS0tLS0tLS0tLS0tXG4iIDw8IGhlYWRlcnMgPDwgIlxu
# IiA8PCBjb2RlIDw8ICJcbi0tLS0tLS0tLS0tLVxuXG5cbiIKICAgICAgICBl
# bmQKICAgICAgZW5kCiAgICBlbmQKICBlbmQKZW5kCgoAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAcW9uZmlndXJlL2NvbmZpZy5yYn4AAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAwMDA2NDQAMDAwMTc1
# MQAwMDAxNzUxADAwMDAwMDA0NjE0ADEwNzAyMzEzNTEyADAxMzE3MwAgMAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAB1c3RhciAgAGtyYXdlawAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKbW9kdWxl
# IFFvbmYKCmNsYXNzIFBhY2thZ2UKICBhdHRyX3JlYWRlciA6aW5jbHVkZV9w
# YXRoLCA6bGlicywgOm1vZHVsZXMsIDpkZWZpbmVzLCA6bmFtZQogIGRlZiBp
# bml0aWFsaXplKG5hbWUpCiAgICBAaW5jbHVkZV9wYXRoID0gW10KICAgIEBs
# aWJzID0gW10KICAgIEBtb2R1bGVzID0gW10KICAgIEBkZWZpbmVzID0gW10K
# ICAgIEBuYW1lID0gbmFtZQogIGVuZAogIAogIGRlZiBhZGRfaW5jbHVkZV9w
# YXRoKHBhdGgpCiAgICBAaW5jbHVkZV9wYXRoIDw8IHBhdGgKICBlbmQKICAK
# ICBkZWYgYWRkX2xpYihsaWIpCiAgICBAbGlicyA8PCBsaWIudG9fcwogIGVu
# ZAogIAogIGRlZiBhZGRfcXRtb2R1bGUobW9kKQogICAgQG1vZHVsZXMgPDwg
# bW9kCiAgZW5kCiAgCiAgZGVmIGFkZF9kZWZpbmUoZGVmaW5lKQogICAgQGRl
# ZmluZXMgPDwgZGVmaW5lCiAgZW5kCiAgCiAgZGVmIHdyaXRlKGYpCiAgICBp
# ZiBub3QgQGluY2x1ZGVfcGF0aC5lbXB0eT8KICAgICAgICBmIDw8ICJJTkNM
# VURFUEFUSCArPSAiIDw8IEBpbmNsdWRlX3BhdGgudW5pcS5qb2luKCIgIikg
# PDwgIlxuIgogICAgZW5kCiAgICAKICAgIGlmIG5vdCBAbGlicy5lbXB0eT8K
# ICAgICAgICBmIDw8ICJMSUJTICs9ICIgPDwgQGxpYnMudW5pcS5qb2luKCIg
# IikgPDwgIlxuIgogICAgZW5kCiAgICAKICAgIGlmIG5vdCBAbW9kdWxlcy5l
# bXB0eT8KICAgICAgICBmIDw8ICJRVCArPSAiIDw8IEBtb2R1bGVzLnVuaXEu
# am9pbigiICIpIDw8ICJcbiIKICAgIGVuZAogICAgCiAgICBpZiBub3QgQGRl
# ZmluZXMuZW1wdHk/CiAgICAgICAgZiA8PCAiREVGSU5FUyArPSAiIDw8IEBk
# ZWZpbmVzLnVuaXEuam9pbigiICIpIDw8ICJcbiIKICAgIGVuZAogIGVuZApl
# bmQKCmNsYXNzIENvbmZpZyA8IFBhY2thZ2UKICBkZWYgaW5pdGlhbGl6ZQog
# ICAgc3VwZXIobmlsKQogICAgQHFvbmZfZmVhdHVyZXMgPSBbXQogIGVuZAog
# IAogIGRlZiBhZGRfcW9uZl9mZWF0dXJlKGZ0cikKICAgIEBxb25mX2ZlYXR1
# cmVzIDw8IGZ0cgogIGVuZCAKCiAgZGVmIGluY2x1ZGVfcGF0aAogICAgaW5j
# cGF0aCA9IEBpbmNsdWRlX3BhdGgKICAgIAogICAgQHFvbmZfZmVhdHVyZXMu
# ZWFjaCB7IHxwa2d8CiAgICAgICAgaW5jcGF0aC5jb25jYXQgcGtnLmluY2x1
# ZGVfcGF0aAogICAgfQogICAgCiAgICBpbmNwYXRoCiAgZW5kCiAgCiAgZGVm
# IGxpYnMKICAgIGxpYiA9IEBsaWJzCiAgICBAcW9uZl9mZWF0dXJlcy5lYWNo
# IHsgfHBrZ3wKICAgICAgICBsaWIuY29uY2F0IHBrZy5saWJzCiAgICB9CiAg
# ICAKICAgIGxpYgogIGVuZAogIAogIGRlZiBkZWZpbmVzCiAgICBkZWZzID0g
# QGRlZmluZXMKICAgIAogICAgQHFvbmZfZmVhdHVyZXMuZWFjaCB7IHxwa2d8
# CiAgICAgICAgZGVmcy5jb25jYXQgcGtnLmRlZmluZXMKICAgIH0KICAgIAog
# ICAgZGVmcwogIGVuZAogIAogIGRlZiBzYXZlKHBhdGgpCiAgICBpZiBwYXRo
# WzBdLmNociA9PSBGaWxlOjpTRVBBUkFUT1IKICAgICAgICBwYXRoID0gcGF0
# aAogICAgZWxzZQogICAgICAgIHBhdGggPSAiI3tEaXIuZ2V0d2R9LyN7cGF0
# aH0iCiAgICBlbmQKICAgIAogICAgRmlsZS5vcGVuKHBhdGgsICJ3IikgeyB8
# ZnwKICAgICAgICBmIDw8ICIjIEdlbmVyYXRlZCBhdXRvbWF0aWNhbGx5IGF0
# ICN7VGltZS5ub3d9ISBQTEVBU0UgRE8gTk9UIEVESVQgTUFOVUFMTFkhIjw8
# ICJcbiIKICAgICAgICAKICAgICAgICB3cml0ZShmKQogICAgICAgIAogICAg
# ICAgIAogICAgICAgIGYgPDwgIlFPTkZfRkVBVFVSRVMgKz0gIiA8PCBAcW9u
# Zl9mZWF0dXJlcy5tYXAgeyB8ZXwgZS5uYW1lfS5qb2luKCIgIikgIDw8ICJc
# biIKICAgICAgICBmIDw8ICJRT05GX0ZFQVRVUkVTICs9ICIgPDwgQHFvbmZf
# ZmVhdHVyZXMuam9pbigiICIpICA8PCAiXG4iCiAgICAgICAgCiAgICAgICAg
# ZiA8PCAiXG5kZWZpbmVUZXN0KGxpbmtfd2l0aCkgeyIgPDwgIlxuIgogICAg
# ICAgIGYgPDwgIiAgcmV0ID0gZmFsc2VcbiIKICAgICAgICBAcW9uZl9mZWF0
# dXJlcy5lYWNoIHsgfHFvbmZ8CiAgICAgICAgICBmIDw8ICVAICBjb250YWlu
# cygkJGxpc3QoJCRzcGxpdCgxKSkgLCIje3FvbmYubmFtZX0iKSB7XG5ACiAg
# ICAgICAgICBmIDw8ICIgICAgcmV0ID0gdHJ1ZVxuIgogICAgICAgICAgZiA8
# PCAiICAgIG1lc3NhZ2UoPT4gTGlua2luZyBxb25mIG1vZHVsZTogI3txb25m
# Lm5hbWV9KVxuIgogICAgICAgICAgcW9uZi53cml0ZShmKQogICAgICAgICAg
# ZiA8PCAiICB9XG4iCiAgICAgICAgfQogICAgICAgIGYgPDwgIiAgZXhwb3J0
# KElOQ0xVREVQQVRIKVxuIgogICAgICAgIGYgPDwgIiAgZXhwb3J0KExJQlMp
# XG4iCiAgICAgICAgZiA8PCAiICBleHBvcnQoUVQpXG4iCiAgICAgICAgZiA8
# PCAiICBleHBvcnQoREVGSU5FUylcbiIKICAgICAgICBmIDw8ICIgIHJldHVy
# bigkJHJldClcbiIKICAgICAgICBmIDw8ICJ9XG4iCiAgICB9CiAgZW5kCmVu
# ZAoKZW5kCgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcW9uZmlndXJlL3Nl
# dHVwX3JiLnRwbAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAw
# MDA2NDQAMDAwMTc1MQAwMDAxNzUxADAwMDAwMDA2MjM3ADEwNjczNTAyMDQz
# ADAxMzU2MAAgMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAB1c3RhciAgAGtyYXdlawAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAKCgojIHNldHVwIGlzIGEgb2JqZWN0IHRoYXQgaGVscHMgdG8gY29u
# ZmlndXJlIHRoZSBwcm9qZWN0CiMgSXQgY29udGFpbnMgc29tZSBtZXRob2Rz
# IGxpa2U6CiMgICBhZGRfb3B0aW9uCiMgICBmaW5kX3BhY2thZ2UKIyAgIGFk
# ZF90ZXN0CiMgICBnZW5lcmF0ZV9wa2djb25maWcKZGVmIGNvbmZpZ3VyZShz
# ZXR1cCkKIyMjIyAKIyMjIyBWZXJpZnkgZm9yIFF0IHZlcnNpb24uCiMjIyMg
# CiMgc2V0dXAucXQgPj0gIjQuMi4wIgoKIyMjIyAKIyMjIyBhZGRfb3B0aW9u
# IGFkZHMgYW4gb3B0aW9uIHRvIHRoZSBzY3JpcHQsIGl0IGhhcyB0aGUgZm9s
# bG93aW5nIG9wdGlvbnM6CiMjIyMgOm5hbWUgICAgICAgICBUaGUgbmFtZSBv
# ZiB0aGUgb3B0aW9uLgojIyMjIDp0eXBlICAgICAgICAgVGhlIHR5cGUgb2Yg
# dGhlIG9wdGlvbiAoInBhdGgiLCAic3RyaW5nIiwgImJvb2wiIG9yICJ2b2lk
# IikuCiMjIyMgOm9wdGlvbmFsICAgICBTYXlzIGlmIHRoZSBvcHRpb24gaXMg
# cmVxdWlyZWQgb3Igbm90LgojIyMjIDpkZXNjcmlwdGlvbiAgRGVzY3JpcHRp
# b24gb2YgdGhlIG9wdGlvbgojIyMjIDpkZWZhdWx0ICAgICAgVGhlIGRlZmF1
# bHQgdmFsdWUKIyMjIyAKIyAgIHNldHVwLmFkZF9vcHRpb24oOm5hbWUgPT4g
# Im9wdGlvbi1uYW1lIiwgOnR5cGUgPT4gInBhdGgiLCA6b3B0aW9uYWwgPT4g
# ZmFsc2UsIDpkZXNjcmlwdGlvbiA9PiAiU2V0cyBhbiBvcHRpb24iKQogIAoj
# IyMjIAojIyMjIGZpbmRfcGFja2FnZSBzZWFyY2ggZm9yIGEgcGtnY29uZmln
# IHNwZWMgaW4geW91ciBjb21wdXRlciwgaXQgc2VhcmNoIGluIHRoZSBQS0df
# Q09ORklHX1BBVEggZW52aXJvbm1lbnQgdmFyaWFibGUgb3IgTERfTElCUkFS
# WV9QQVRICiMjIyMgVGhlIG9wdGlvbnMgYXJlOgojIyMjIDpuYW1lICAgICAg
# IFBhY2thZ2UgbmFtZQojIyMjIDpvcHRpb25hbCAgIFRydWUgaWYgdGhlIHBh
# Y2thZ2UgaXMgb3B0aW9uYWwKIyMjIyA6Z2xvYmFsICAgICBMaW5rIGdsb2Jh
# bGx5CiMjIyMgCiMgICBzZXR1cC5maW5kX3BhY2thZ2UoOm5hbWUgPT4gImRi
# dXMtMSIsIDpvcHRpb25hbCA9PiBmYWxzZSwgOmdsb2JhbCA9PiB0cnVlKQoK
# IyMjIyAKIyMjIyBhZGRfdGVzdCBhZGRzIGFuZCBydW4gYSB0ZXN0IGZvciBz
# b21lIGZlYXR1cmUgb3IgcGFja2FnZSwgdGhlIG9wdGlvbnMgYXJlOgojIyMj
# ICAgOmlkICAgICAgICAgICBUaGUgaWQgb2YgdGhlIHRlc3QKIyMjIyAgIDpu
# YW1lICAgICAgICAgVGhlIHRlc3QgbmFtZQojIyMjICAgOmRlZmluZXMgICAg
# ICBEZWZpbmVzIHRvIHRlc3QKIyMjIyAgIDppbmNsdWRlcGF0aCAgQSBsaXN0
# IHdpdGggdGhlIGluY2x1ZGUgcGF0aAojIyMjICAgOmxpYnMgICAgICAgICBU
# aGUgbGlicyByZXF1aXJlZCBmb3IgdGhlIHRlc3QKIyMjIyAgIDpvcHRpb25h
# bCAgICAgVHJ1ZSBpZiB0aGUgdGVzdCBpcyBvcHRpb25hbAojIyMjICAgOmN1
# c3RvbSAgICAgICBBIGNvZGUgdG8gdGVzdAojIyMjICAgOmdsb2JhbCAgICAg
# ICBMaW5rIGdsb2JhbGx5CiMjIyMgCiMgc2V0dXAuYWRkX3Rlc3QoOmlkID0+
# ICJmZWF0dXJlIiwgOm5hbWUgPT4gIkZlYXR1cmUgc3VwcG9ydCIsIDpkZWZp
# bmVzID0+IFsiSEFWRV9GRUFUVVJFIl0sIDpvcHRpb25hbCA9PiB0cnVlLCA6
# aGVhZGVycyA9PiBbImZlYXR1cmUuaCJdLCA6bGlicyA9PiAiLWxmZWF0dXJl
# IiwgOmdsb2JhbCA9PiB0cnVlICApCgojIyMjIAojIyMjIGdlbmVyYXRlX3Br
# Z2NvbmZpZyBHZW5lcmF0ZXMgYSBzaW1wbGUgcGtnY29uZmlnIGZpbGUKIyMj
# IyBUaGUgb3B0aW9ucyBhcmU6CiMjIyMgICA6cGFja2FnZV9uYW1lICBUaGUg
# cGFja2FnZSBuYW1lIAojIyMjICAgOm5hbWUgICAgICAgICAgVGhlIGFwcGxp
# Y2F0aW9uIG5hbWUKIyMjIyAgIDpkZXNjcmlwdGlvbiAgIFRoZSBhcHBsaWNh
# dGlvbiBkZXNjcmlwdGlvbgojIyMjICAgOnZlcnNpb24gICAgICAgVGhlIHZl
# cnNpb24gb2YgdGhlIGFwcGxpY2F0aW9uCiMjIyMgICA6bGliZGlyICAgICAg
# ICBUaGUgZGlyZWN0b3J5IHdoZXJlIGxpYnJhcmllcyBhcmUsIGJ5IGRlZmF1
# bHQgaXMgJChwcmVmaXgpL2xpYgojIyMjICAgOmluY2x1ZGVkaXIgICAgVGhl
# IGRpcmVjdG9yeSB3aGVyZSBoZWFkZXJzIGFyZSwgYnkgZGVmYXVsdCBpcyAk
# KHByZWZpeCkvaW5jbHVkZQojIyMjICAgOmxpYnMgICAgICAgICAgTGlicmFy
# aWVzIHRoYXQgbGlua3MgdGhlIHBhY2thZ2UKIyMjIyAgIDpjZmxhZ3MgICAg
# ICAgIENmbGFncyBmb3IgdGhlIHBhY2thZ2UKIyMjIyAgIDpyZXF1aXJlcyAg
# ICAgIEEgbGlzdCBvZiBwYWNrYWdlIG5hbWVzIHJlcXVpcmVkIGZvciB0aGlz
# IHBhY2thZ2UuCiMjIyMgCiMgc2V0dXAuZ2VuZXJhdGVfcGtnY29uZmlnKDpw
# YWNrYWdlX25hbWUgPT4gImRsaWItZWRpdG9yIiwgOm5hbWUgPT4gIkRMaWIg
# UXQgbGlicmFyeSIsIDpkZXNjcmlwdGlvbiA9PiAiQSBleHRlbnNpb24gZm9y
# IFF0NCIsIDp2ZXJzaW9uID0+ICIwLjFiZXRhIiwgOmxpYmRpciA9PiBuaWws
# IDppbmNsdWRlZGlyID0+IG5pbCwgOmxpYnMgPT4gIi1MJHtsaWJkaXJ9IC1s
# ZGNvcmUgLWxkZ3VpIC1sZGVkaXRvciIsIDpjZmxhZ3MgPT4gIi1JJHtpbmNs
# dWRlZGlyfSIsIDpyZXF1aXJlcyA9PiBbInNvbWVwYWNrYWdlIiwgIm90aGVy
# cGFja2FnZSIgXSApCmVuZAoKIyBUaGlzIG1ldGhvZCBpcyBjYWxsZWQgYmVm
# b3JlIHRvIHNlYXJjaCBhIHBhY2thZ2UKZGVmIHNldHVwX3BrZ2NvbmZpZyhw
# a2djb25maWcsIGFyZ3MpCiMgICBjYXNlIHBrZ2NvbmZpZy5pZAojICAgICB3
# aGVuICJkYnVzLTEiCiMgICAgICAgcGtnY29uZmlnLmFkZF9zZWFyY2hfcGF0
# aCgiL2EvcGF0aC90by90aGUvcGMiKQojICAgZW5kCmVuZAoKZGVmIHNldHVw
# X3Rlc3QoaWQsIHRzdCwgYXJncykKIyAgIGNhc2UgaWQKIyAgICAgd2hlbiAi
# ZmVhdHVyZSIKIyAgICAgICB0c3QuYWRkX2RlZmluZSgiTVlERUZJTkUiKQoj
# ICAgICAgIHRzdC5hZGRfbGliKCItbG15bGliIikKIyAgICAgICB0c3QuYWRk
# X2luY2x1ZGVfcGF0aCgiL3NvbWUvaW5jbHVkZS9wYXRoL3JlcXVpcmVkL3Rv
# L2NvbXBpbGUvdGhlL3Rlc3QiKQojICAgZW5kCmVuZAoKZGVmIHNldHVwX2Nv
# bmZpZyhjZmcsIGFyZ3MpCiMgICBjZmcuYWRkX2luY2x1ZGVfcGF0aCBGaWxl
# LmV4cGFuZF9wYXRoKCJzcmMiKQojICAgY2ZnLmFkZF9kZWZpbmUoIl9fU1RE
# Q19DT05TVEFOVF9NQUNST1MiKQojICAgY2ZnLmFkZF9xdG1vZHVsZSgieG1s
# IikKIyAgIGNmZy5hZGRfcXRtb2R1bGUoIm5ldHdvcmsiKQplbmQKCgoAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHFvbmZpZ3VyZS9x
# bWFrZS5yYgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAw
# MDAwNjQ0ADAwMDE3NTEAMDAwMTc1MQAwMDAwMDAwNzMzMwAxMDcwNTAyMDUy
# NwAwMTI2MzMAIDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAdXN0YXIgIABrcmF3ZWsAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAACnJlcXVpcmUgJ29wZW4zJwpyZXF1aXJlICdkZXRlY3RvcycKCm1v
# ZHVsZSBRb25mCgpjbGFzcyBRTWFrZQogIGF0dHJfcmVhZGVyIDpwYXRoCiAg
# CiAgZGVmIGluaXRpYWxpemUocGF0aHMgPSBbXSwgbG9nX2ZpbGUgPSBuaWwp
# CiAgICBAcGF0aHMgPSBbICJxbWFrZS1xdDQiLCAicW1ha2U0IiwgInFtYWtl
# IiBdCiAgICBAcGF0aHMuY29uY2F0KHBhdGhzKQogICAgCiAgICBpZiggJERF
# QlVHICkKICAgICAgJHN0ZGVyciA8PCAiUU1ha2UgcGF0aHM6ICIgPDwgQHBh
# dGhzLmpvaW4oIiAiKSA8PCAiXG4iCiAgICBlbmQKICAgIAogICAgaWYgbm90
# IGZpbmRfcW1ha2UoIjQuMC4wIikKICAgICAgICAgICAgcmFpc2UgIkNhbid0
# IGZpbmQgdmFsaWQgcW1ha2UgLSBxdDQiCiAgICBlbmQKICAgIAogICAgQGxv
# Z19maWxlID0gbG9nX2ZpbGUKICAgIGlmKCBAbG9nX2ZpbGUubmlsPyApCiAg
# ICAgIEBsb2dfZmlsZSA9IG9sZGxvY2F0aW9uKCJjb25maWcubG9nIikKICAg
# IGVuZAogICAgCiAgICBAbWFrZSA9ICJtYWtlIgogICAgCiAgICBpZiBEZXRl
# Y3RPUy5vcz8gPT0gMQogICAgICBAbWFrZV9vcHRpb25zID0gIjI+PiAje0Bs
# b2dfZmlsZX0iCiAgICBlbHNlCiAgICAgIEBtYWtlX29wdGlvbnMgPSAiPj4g
# I3tAbG9nX2ZpbGV9IDI+PiAje0Bsb2dfZmlsZX0iCiAgICBlbmQKICBlbmQK
# ICAgICAgICAKICBkZWYgZmluZF9xbWFrZShtaW5xdHZlcnNpb24sIG9wID0g
# MikKICAgIAogICAgCiAgICBtaW52ZXIgPSBtaW5xdHZlcnNpb24uc3BsaXQo
# Ii4iKQogICAgQHBhdGhzLmVhY2ggeyB8cGF0aHwKICAgICAgYmVnaW4KICAg
# ICAgICB2ZXJzaW9uID0gW10KICAgICAgICBvayA9IHRydWUKICAgICAgICAK
# ICAgICAgICBJTy5wb3BlbigiI3twYXRofSAtcXVlcnkgUVRfVkVSU0lPTiAy
# PiAje25ld2xvY2F0aW9uKCJxbWFrZV9sb2ciKX0iKSB7IHxwcmN8CiAgICAg
# ICAgICB2ZXJzaW9uID0gcHJjLnJlYWRsaW5lcy5qb2luKCIiKS5zcGxpdCgi
# LiIpCiAgICAgICAgfQogICAgICAgIAogICAgICAgIG5leHQgaWYgJD8gIT0g
# MAogICAgICAgIAogICAgICAgIHZlcnNpb24uc2l6ZS50aW1lcyB7IHxpfAog
# ICAgICAgICAgaWYgdmVyc2lvbi5zaXplIDwgaSBhbmQgbWludmVyLnNpemUg
# Pj0gMwogICAgICAgICAgICAgICAgICBicmVhawogICAgICAgICAgZW5kCiAg
# ICAgICAgICAKICAgICAgICAgIGNhc2Ugb3AKICAgICAgICAgICAgd2hlbiAw
# CiAgICAgICAgICAgICAgaWYgdmVyc2lvbltpXSAhPSBtaW52ZXJbaV0KICAg
# ICAgICAgICAgICAgIG9rID0gZmFsc2UKICAgICAgICAgICAgICAgIGJyZWFr
# CiAgICAgICAgICAgICAgZW5kCiAgICAgICAgICAgIHdoZW4gMQogICAgICAg
# ICAgICAgIGlmIHZlcnNpb25baV0gPCBtaW52ZXJbaV0KICAgICAgICAgICAg
# ICAgIG9rID0gZmFsc2UKICAgICAgICAgICAgICAgIGJyZWFrCiAgICAgICAg
# ICAgICAgZW5kCiAgICAgICAgICAgIHdoZW4gMgogICAgICAgICAgICAgIGlm
# IHZlcnNpb25baV0gPCBtaW52ZXJbaV0KICAgICAgICAgICAgICAgIG9rID0g
# ZmFsc2UKICAgICAgICAgICAgICAgIGJyZWFrCiAgICAgICAgICAgICAgZW5k
# CiAgICAgICAgICBlbmQKICAgICAgICB9CiAgICAgICAgCiAgICAgICAgaWYg
# b2sKICAgICAgICAgIEBwYXRoID0gcGF0aAogICAgICAgICAgCiAgICAgICAg
# ICBpZiAkREVCVUcKICAgICAgICAgICAgcHV0cyAiVXNpbmcgcW1ha2U6ICN7
# QHBhdGh9ICN7dmVyc2lvbi5qb2luKCIuIil9IgogICAgICAgICAgZW5kCiAg
# ICAgICAgICAKICAgICAgICAgIHJldHVybiBvawogICAgICAgIGVuZAogICAg
# ICByZXNjdWUKICAgICAgZW5zdXJlCiAgICAgIGVuZAogICAgfQogIAogICAg
# cmV0dXJuIGZhbHNlCiAgZW5kCiAgICAgICAgCiAgICAgICAgZGVmIHF1ZXJ5
# KHZhcikKICAgICAgICAgICAgICAgIG91dHB1dCA9IGAje0BwYXRofSAtcXVl
# cnkgI3t2YXJ9YAogICAgICAgICAgICAgICAgb3V0cHV0CiAgICAgICAgZW5k
# CiAgICAgICAgCiAgICAgICAgZGVmIHJ1bihhcmdzID0gIiIsIHJlY3VyID0g
# ZmFsc2UpCiAgICAgICAgICAgICAgICBpZiAkREVCVUcKICAgICAgICAgICAg
# ICAgICAgcHV0cyAiQVJHUyA9ICN7YXJnc30iCiAgICAgICAgICAgICAgICBl
# bmQKICAgICAgICAgICAgICAgIG9wdGlvbnMgPSAiIgogICAgICAgICAgICAg
# ICAgaWYgcmVjdXIKICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucyAr
# PSAiLXIiCiAgICAgICAgICAgICAgICBlbmQKICAgICAgICAgICAgICAgIAog
# ICAgICAgICAgICAgICAgb3V0cHV0ID0gYCN7QHBhdGh9ICN7YXJnc30gI3tv
# cHRpb25zfSBgCiAgICAgICAgICAgICAgICBpZiBvdXRwdXQuc3RyaXAuZW1w
# dHk/CiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlCiAgICAg
# ICAgICAgICAgICBlbmQKCiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2UK
# ICAgICAgICBlbmQKICAgICAgICAKICAgICAgICBkZWYgY29tcGlsZQogICAg
# ICAgICAgICAgICAgYmVnaW4KICAgICAgICAgICAgICAgICAgICAgICAgSU8u
# cG9wZW4oIiN7QG1ha2V9IGNsZWFuICN7QG1ha2Vfb3B0aW9uc30iKSB7IHxj
# fAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG91dHB1dCA9IGMu
# cmVhZGxpbmVzCiAgICAgICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAg
# ICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgIHRpbWVz
# ID0gMAogICAgICAgICAgICAgICAgICAgICAgICBlbmRjb2RlID0gMgogICAg
# ICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAg
# RmlsZS5vcGVuKEBsb2dfZmlsZSwgImEiICkgZG8gfGZ8CiAgICAgICAgICAg
# ICAgICAgICAgICAgICAgZiA8PCAiPT09PT0+IENPTVBJTElORyBcbiIKICAg
# ICAgICAgICAgICAgICAgICAgICAgZW5kCiAgICAgICAgICAgICAgICAgICAg
# ICAgIAogICAgICAgICAgICAgICAgICAgICAgICB3aGlsZSBlbmRjb2RlID09
# IDIgYW5kIHRpbWVzIDw9IDMKICAgICAgICAgICAgICAgICAgICAgICAgICAg
# ICAgICBJTy5wb3BlbigiI3tAbWFrZX0gI3tAbWFrZV9vcHRpb25zfSIsICJy
# IikgeyB8Y3wKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
# ICAgIG91dHB1dCA9IGMucmVhZGxpbmVzCiAgICAgICAgICAgICAgICAgICAg
# ICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
# IAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAg
# ICAgICAgICAgICAgICAgICAgICAgIGVuZGNvZGUgPSAkPyA+PiA4CiAgICAg
# ICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAg
# ICAgICAgICAgICAgICAgdGltZXMgKz0gMQogICAgICAgICAgICAgICAgICAg
# ICAgICBlbmQKICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAg
# ICAgICByZXNjdWUKICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZh
# bHNlCiAgICAgICAgICAgICAgICBlbmQKICAgICAgICAgICAgICAgIAogICAg
# ICAgICAgICAgICAgaWYgZW5kY29kZSAhPSAwCiAgICAgICAgICAgICAgICAg
# ICAgICAgIHJldHVybiBmYWxzZQogICAgICAgICAgICAgICAgZW5kCiAgICAg
# ICAgICAgICAgICAKICAgICAgICAgICAgICAgIHJldHVybiB0cnVlCiAgICAg
# ICAgZW5kCiAgICAgICAgCiAgICAgICAgZGVmIGNsZWFuX3VwCiAgICAgIElP
# LnBvcGVuKCIje0BtYWtlfSBkaXN0Y2xlYW4gMj4+IGRpc3RjbGVhbiIpIHsg
# fGN8CiAgICAgICAgICBvdXRwdXQgPSBjLnJlYWRsaW5lcwogICAgICB9CiAg
# ICAgICAgZW5kCmVuZAoKZW5kICNtb2R1bGUKCgoAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAHFvbmZpZ3VyZS9wa2djb25maWcucmJ+AAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAwMDAwNjQ0ADAwMDE3NTEAMDAwMTc1MQAwMDAw
# MDAwNjcxNQAxMDcyMzM0MzEyMgAwMTM3MDQAIDAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdXN0YXIg
# IABrcmF3ZWsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACgpyZXF1aXJlICdkZXRlY3RvcycK
# Cm1vZHVsZSBRb25mCgpjbGFzcyBQa2dDb25maWdQYXJzZXIKICBkZWYgaW5p
# dGlhbGl6ZQogICAgQHZhcmlhYmxlcyA9IHt9CiAgICBAc3BlY3MgPSB7fQog
# IGVuZAogIAogIGRlZiBwYXJzZShwYXRoKQogICAgRmlsZS5vcGVuKHBhdGgp
# IHsgfGZpbGV8CiAgICAgIGZpbGUuZWFjaCB7IHxsaW5lfAogICAgICAgIGlm
# IGxpbmUgPX4gLyhcdyspPSguKikkLwogICAgICAgICAgQHZhcmlhYmxlc1sk
# MV0gPSAkMgogICAgICAgIGVsc2lmIGxpbmUgPX4gLyhcdyspOlxzKC4qKSQv
# CiAgICAgICAgICBAc3BlY3NbJDFdID0gJDIKICAgICAgICBlbmQKICAgICAg
# fQogICAgfQogIGVuZAogIAogIGRlZiB2YWx1ZSh2YXJpYWJsZSkKICAgIHZh
# bCA9IEB2YXJpYWJsZXNbdmFyaWFibGVdCiAgICB3aGlsZSB2YWwgPX4gLyhc
# JFx7KFx3KylcfSkvCiAgICAgIHZhbC5nc3ViISgkMSwgdmFsdWUoJDIpKQog
# ICAgZW5kCiAgICAKICAgIHZhbAogIGVuZAogIAogIGRlZiBzcGVjKHZhcmlh
# YmxlKQogICAgdmFsID0gQHNwZWNzW3ZhcmlhYmxlXQogICAgd2hpbGUgdmFs
# ID1+IC8oXCRceyhcdyspXH0pLwogICAgICB2YWwuZ3N1YiEoJDEsIHZhbHVl
# KCQyKSkKICAgIGVuZAogICAgCiAgICB2YWwKICBlbmQKICAKICBkZWYgbGli
# cwogICAgc3BlYygiTGlicyIpCiAgZW5kCiAgCiAgZGVmIGNmbGFncwogICAg
# c3BlYygiQ2ZsYWdzIikKICBlbmQKICAKICBkZWYgcmVxdWlyZXMKICAgIHNw
# ZWMoIlJlcXVpcmVzIikudG9fcwogIGVuZAplbmQKCmNsYXNzIFBrZ0NvbmZp
# ZwogIGF0dHJfcmVhZGVyIDppZCwgOmxpYnMsIDpjZmxhZ3MsIDpyZXF1aXJl
# cwogIAogIGRlZiBpbml0aWFsaXplKGlkKQogICAgQGlkID0gaWQKICAgIEBz
# ZWFyY2hfcGF0aCA9IFtdCiAgICBAcmVxdWlyZXMgPSBbXQogIGVuZAogIAog
# IGRlZiBmaW5kKHBrZykKICAgIHBrZ3BhdGggPSBmaW5kX3BhdGhfZm9yX3Bh
# Y2thZ2UocGtnKQogICAgCiAgICBpZiBub3QgcGtncGF0aC5uaWw/CiAgICAg
# IEBjZmxhZ3MgPSAiIgogICAgICBAbGlicyA9ICIiCiAgICAgIHBhcnNlKHBr
# Z3BhdGgsIHBrZykKICAgICAgCiAgICAgIHJldHVybiB0cnVlCiAgICBlbmQK
# ICAgIAogICAgZmFsc2UKICBlbmQKICAKICBkZWYgYWRkX3NlYXJjaF9wYXRo
# KHBhdGgpCiAgICBpZiBwYXRoLmtpbmRfb2Y/KEFycmF5KQogICAgICBAc2Vh
# cmNoX3BhdGguY29uY2F0KHBhdGgpCiAgICBlbHNlCiAgICAgIEBzZWFyY2hf
# cGF0aCA8PCBwYXRoLnRvX3MKICAgIGVuZAogIGVuZAogIAogIGRlZiBpbmNs
# dWRlX3BhdGgKICAgIGluY3BhdGggPSBbXQogICAgY2ZsYWdzID0gQGNmbGFn
# cy5kdXAKICAgIHdoaWxlIGNmbGFncyA9fiAvKC1JKChcdzopezAsMX0oW1wv
# XFxdXFMrKSspKS8KICAgICAgaW5jcGF0aCA8PCAkMgogICAgICBjZmxhZ3Mu
# c3ViISgkMSwgIiIpCiAgICBlbmQKICAgIAogICAgaW5jcGF0aC51bmlxCiAg
# ZW5kCiAgCiAgZGVmIGRlZmluZXMKICAgIGRlZmluZXMgPSBbXQogICAgY2Zs
# YWdzID0gQGNmbGFncy5kdXAKICAgIHdoaWxlIGNmbGFncyA9fiAvKC1EXHMq
# KChcV1x3KykrKSkvCiAgICAgIGRlZmluZXMgPDwgJDIKICAgICAgZGVmaW5l
# cy5zdWIhKCQxLCAiIikKICAgIGVuZAogICAgCiAgICBkZWZpbmVzLnVuaXEK
# ICBlbmQKICAKICBkZWYgZmluZF93aW4zMl9wYXRocwogICAgcGF0aHMgPSBm
# aW5kX2Vudl9wYXRocwogICAgCiAgICAKICAgIHBhdGhzCiAgZW5kCiAgCiAg
# ZGVmIGZpbmRfdW5peF9wYXRocwogICAgcGF0aHMgPSBmaW5kX2Vudl9wYXRo
# cwogICAgaWYgRmlsZS5leGlzdHM/KCIvZXRjL2xkLnNvLmNvbmYiKQogICAg
# ICBGaWxlLm9wZW4oIi9ldGMvbGQuc28uY29uZiIpIGRvIHxmaWxlfAogICAg
# ICAgIGZpbGUuZWFjaCB7IHxsaW5lfAogICAgICAgICAgZGlyID0gbGluZS5z
# dHJpcCsiL3BrZ2NvbmZpZyIKICAgICAgICAgIGlmIEZpbGUuZXhpc3RzPyhk
# aXIpCiAgICAgICAgICAgIHBhdGhzIDw8IGRpcgogICAgICAgICAgZW5kCiAg
# ICAgICAgfQogICAgICBlbmQKICAgIGVuZAogICAgCiAgICBwYXRocyA8PCAi
# L3Vzci9saWIvcGtnY29uZmlnIiA8PCAiL3Vzci9sb2NhbC9saWIvcGtnY29u
# ZmlnIiA8PCBFTlZbIkhPTUUiXS50b19zKyIvbG9jYWwvbGliL3BrZ2NvbmZp
# ZyIKICAgIAogICAgcGF0aHMKICBlbmQKICAKICBkZWYgZmluZF9lbnZfcGF0
# aHMKICAgIHNlcHBhdGggPSAoRGV0ZWN0T1Mub3M/ID09IDEpID8gIjsiIDog
# IjoiCiAgICAKICAgIHBhdGhzID0gWyAiLiIgXQogICAgKEVOVlsiUEtHX0NP
# TkZJR19QQVRIIl0udG9fcytzZXBwYXRoK0VOVlsiTERfTElCUkFSWV9QQVRI
# Il0udG9fcykuc3BsaXQoc2VwcGF0aCkuZWFjaCB7IHxwYXRofAogICAgICBp
# ZiBGaWxlLmV4aXN0cz8ocGF0aCkKICAgICAgICBwYXRocyA8PCBwYXRoCiAg
# ICAgIGVuZAogICAgfQogICAgCiAgICBwYXRocwogIGVuZAogIAogIHByaXZh
# dGUKICBkZWYgcGFyc2UocGtncGF0aCwgcGtnKQogICAgcmV0dXJuIGlmIHBr
# Z3BhdGgubmlsPwogICAgCiAgICBwYXJzZXIgPSBQa2dDb25maWdQYXJzZXIu
# bmV3CiAgICBwYXJzZXIucGFyc2UocGtncGF0aCkKICAgIAogICAgcGFyc2Vy
# LnJlcXVpcmVzLnNwbGl0KC8sfFxzLykuZWFjaCB7IHxyZXF8CiAgICAgIHJl
# cS5zdHJpcCEKICAgICAgCiAgICAgIHBhcnNlKGZpbmRfcGF0aF9mb3JfcGFj
# a2FnZShyZXEpLCByZXEpCiAgICB9CiAgICAKICAgIGNmbGFncyA9IHBhcnNl
# ci5jZmxhZ3MKICAgIGxpYnMgPSBwYXJzZXIubGlicwogICAgCiAgICBpZiBp
# ZCAhPSBwa2cgYW5kIG5vdCAoY2ZsYWdzLmVtcHR5PyBvciBsaWJzLmVtcHR5
# PykKICAgICAgQHJlcXVpcmVzIDw8IHBrZwogICAgZW5kCiAgICAKICAgIEBj
# ZmxhZ3MgPSAiI3tjZmxhZ3N9ICN7QGNmbGFnc30iCiAgICBAbGlicyA9ICIj
# e2xpYnN9ICN7QGxpYnN9IgogICAgCiAgICBuaWwKICBlbmQKICAKICBkZWYg
# ZmluZF9wYXRoX2Zvcl9wYWNrYWdlKHBrZykKICAgIGlmIG5vdCBwa2cgPX4g
# L1wucGMkLwogICAgICBwa2cgKz0gIi5wYyIKICAgIGVuZAogICAgCiAgICBw
# YXRocyA9IEBzZWFyY2hfcGF0aAogICAgCiAgICBpZiBEZXRlY3RPUy5vcz8g
# PT0gMSAjIFdpbmRvd3MKICAgICAgcGF0aHMuY29uY2F0IGZpbmRfd2luMzJf
# cGF0aHMKICAgIGVsc2UKICAgICAgcGF0aHMuY29uY2F0IGZpbmRfdW5peF9w
# YXRocwogICAgZW5kCiAgICAKICAgIHBrZ3BhdGggPSBuaWwKICAgIHBhdGhz
# LmVhY2ggeyB8cGF0aHwKICAgICAgdG1wcGF0aCA9IHBhdGgrIi8iK3BrZwog
# ICAgICBpZiBGaWxlLmV4aXN0cz8odG1wcGF0aCkKICAgICAgICBwa2dwYXRo
# ID0gdG1wcGF0aAogICAgICBlbHNpZiAkREVCVUcKICAgICAgICBwdXRzICJG
# YWlsZWQ6ICN7dGVtcHBhdGh9IgogICAgICBlbmQKICAgIH0KICAgIAogICAg
# cGtncGF0aAogIGVuZAplbmQKCmVuZAoKCmlmICQwID09IF9fRklMRV9fCiAg
# cGtnY29uZmlnID0gUW9uZjo6UGtnQ29uZmlnLm5ldwogIHBrZ2NvbmZpZy5m
# aW5kKCJ0ZXN0X3FvbmYiKQogIAogIHB1dHMgcGtnY29uZmlnLmxpYnMKZW5k
# CgoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAABxb25maWd1cmUvZnJvbnRlbmQucmIAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAMDAwMDY0NAAwMDAxNzUxADAwMDE3NTEAMDAw
# MDAwMDAxMjQAMTA2NzM3NDAzMDMAMDEzMzUwACAwAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHVzdGFy
# ICAAa3Jhd2VrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAptb2R1bGUgUW9uZgoKY2xhc3Mg
# RnJvbnRlbmQKICBkZWYgaW5pdGlhbGl6ZQogIGVuZAogIAogIGRlZiBwYXJz
# ZQogIGVuZAplbmQKCmVuZAoKCgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcW9u
# ZmlndXJlL2Zyb250ZW5kZmFjdG9yeS5yYgAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAADAwMDA2NDQAMDAwMTc1MQAwMDAxNzUxADAwMDAwMDAwNjQzADEw
# NjczNzQwMzAzADAxNDc0NgAgMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB1c3RhciAgAGtyYXdlawAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAKcmVxdWlyZSAncGxhaW5fZnJvbnRlbmQnCgptb2R1
# bGUgUW9uZgoKY2xhc3MgRnJvbnRlbmRGYWN0b3J5CiAgZGVmIHNlbGYuY3Jl
# YXRlKG5hbWUgPSAidGsiKQogICAgY2FzZSBuYW1lCiAgICAgIHdoZW4gInBs
# YWluIgogICAgICAgIFBsYWluRnJvbnRlbmQubmV3CiAgICAgIHdoZW4gInRr
# IgogICAgICAgIGhhdmVfdGsgPSBmYWxzZQoKICAgICAgICBiZWdpbgogICAg
# ICAgICAgaGF2ZV90ayA9IHJlcXVpcmUgJ3RrJwogICAgICAgIHJlc2N1ZSBM
# b2FkRXJyb3IKICAgICAgICAgIHJldHVybiBjcmVhdGUoInBsYWluIikKICAg
# ICAgICBlbmQKICAgICAgICAKICAgICAgICByZXF1aXJlICd0a19mcm9udGVu
# ZCcKICAgICAgICBUa0Zyb250ZW5kLm5ldwogICAgICBlbHNlCiAgICAgICAg
# bmlsCiAgICBlbmQKICBlbmQKZW5kCgplbmQKCgAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHFvbmZpZ3VyZS9mcm9u
# dGVuZC5yYn4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwMDAw
# NjQ0ADAwMDE3NTEAMDAwMTc1MQAwMDAwMDAwMDEyMwAxMDY3MjU3MTIxNgAw
# MTM1NTEAIDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAdXN0YXIgIABrcmF3ZWsAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAACm1vZHVsZSBRb25mCgpjbGFzcyBCYWNrZW5kCiAgZGVmIGluaXRpYWxp
# emUKICBlbmQKICAKICBkZWYgcGFyc2UKICBlbmQKZW5kCgplbmQKCgoAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAABxb25maWd1cmUvc2V0dXBfcmIudHBsfgAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMDAwMDY0NAAwMDAxNzUx
# ADAwMDE3NTEAMDAwMDAwMDU3MTIAMTA2NzMwMjc3NDYAMDEzNzY3ACAwAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAHVzdGFyICAAa3Jhd2VrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAoKCiMgc2V0
# dXAgaXMgYSBvYmplY3QgdGhhdCBoZWxwcyB0byBjb25maWd1cmUgdGhlIHBy
# b2plY3QKIyBJdCBjb250YWlucyBzb21lIG1ldGhvZHMgbGlrZToKIyAgIGFk
# ZF9vcHRpb24KIyAgIGZpbmRfcGFja2FnZQojICAgYWRkX3Rlc3QKIyAgIGdl
# bmVyYXRlX3BrZ2NvbmZpZwpkZWYgY29uZmlndXJlKHNldHVwKQojIyMjIAoj
# IyMjIGFkZF9vcHRpb24gYWRkcyBhbiBvcHRpb24gdG8gdGhlIHNjcmlwdCwg
# aXQgaGFzIHRoZSBmb2xsb3dpbmcgb3B0aW9uczoKIyMjIyA6bmFtZSAgICAg
# ICAgIFRoZSBuYW1lIG9mIHRoZSBvcHRpb24uCiMjIyMgOnR5cGUgICAgICAg
# ICBUaGUgdHlwZSBvZiB0aGUgb3B0aW9uICgicGF0aCIsICJzdHJpbmciLCAi
# Ym9vbCIgb3IgInZvaWQiKS4KIyMjIyA6b3B0aW9uYWwgICAgIFNheXMgaWYg
# dGhlIG9wdGlvbiBpcyByZXF1aXJlZCBvciBub3QuCiMjIyMgOmRlc2NyaXB0
# aW9uICBEZXNjcmlwdGlvbiBvZiB0aGUgb3B0aW9uCiMjIyMgOmRlZmF1bHQg
# ICAgICBUaGUgZGVmYXVsdCB2YWx1ZQojIyMjIAojICAgc2V0dXAuYWRkX29w
# dGlvbig6bmFtZSA9PiAib3B0aW9uLW5hbWUiLCA6dHlwZSA9PiAicGF0aCIs
# IDpvcHRpb25hbCA9PiBmYWxzZSwgOmRlc2NyaXB0aW9uID0+ICJTZXRzIGFu
# IG9wdGlvbiIpCiAgCiMjIyMgCiMjIyMgZmluZF9wYWNrYWdlIHNlYXJjaCBm
# b3IgYSBwa2djb25maWcgc3BlYyBpbiB5b3VyIGNvbXB1dGVyLCBpdCBzZWFy
# Y2ggaW4gdGhlIFBLR19DT05GSUdfUEFUSCBlbnZpcm9ubWVudCB2YXJpYWJs
# ZSBvciBMRF9MSUJSQVJZX1BBVEgKIyMjIyAKIyMjIyBGaXJzdCBvcHRpb24g
# aXMgdGhlIHBhY2thZ2UgbmFtZSBhbmQgaWYgaXQgaXMgb3B0aW9uYWwgb3Ig
# bm90CiMjIyMgCiMgICBzZXR1cC5maW5kX3BhY2thZ2UoImRidXMtMSIsIGZh
# bHNlKQoKIyMjIyAKIyMjIyBhZGRfdGVzdCBhZGRzIGFuZCBydW4gYSB0ZXN0
# IGZvciBzb21lIGZlYXR1cmUgb3IgcGFja2FnZSwgdGhlIG9wdGlvbnMgYXJl
# OgojIyMjICAgOmlkICAgICAgICAgICBUaGUgaWQgb2YgdGhlIHRlc3QKIyMj
# IyAgIDpuYW1lICAgICAgICAgVGhlIHRlc3QgbmFtZQojIyMjICAgOmRlZmlu
# ZXMgICAgICBEZWZpbmVzIHRvIHRlc3QKIyMjIyAgIDppbmNsdWRlcGF0aCAg
# QSBsaXN0IHdpdGggdGhlIGluY2x1ZGUgcGF0aAojIyMjICAgOmxpYnMgICAg
# ICAgICBUaGUgbGlicyByZXF1aXJlZCBmb3IgdGhlIHRlc3QKIyMjIyAgIDpv
# cHRpb25hbCAgICAgVHJ1ZSBpZiB0aGUgdGVzdCBpcyBvcHRpb25hbAojIyMj
# ICAgOmN1c3RvbSAgICAgICBBIGNvZGUgdG8gdGVzdAojIyMjIAojIHNldHVw
# LmFkZF90ZXN0KDppZCA9PiAiZmVhdHVyZSIsIDpuYW1lID0+ICJGZWF0dXJl
# IHN1cHBvcnQiLCA6ZGVmaW5lcyA9PiBbIkhBVkVfRkVBVFVSRSJdLCA6b3B0
# aW9uYWwgPT4gdHJ1ZSwgOmhlYWRlcnMgPT4gWyJmZWF0dXJlLmgiXSwgOmxp
# YnMgPT4gIi1sZmVhdHVyZSIgICkKCiMjIyMgCiMjIyMgZ2VuZXJhdGVfcGtn
# Y29uZmlnIEdlbmVyYXRlcyBhIHNpbXBsZSBwa2djb25maWcgZmlsZQojIyMj
# IFRoZSBvcHRpb25zIGFyZToKIyMjIyAgIDpwYWNrYWdlX25hbWUgIFRoZSBw
# YWNrYWdlIG5hbWUgCiMjIyMgICA6bmFtZSAgICAgICAgICBUaGUgYXBwbGlj
# YXRpb24gbmFtZQojIyMjICAgOmRlc2NyaXB0aW9uICAgVGhlIGFwcGxpY2F0
# aW9uIGRlc2NyaXB0aW9uCiMjIyMgICA6dmVyc2lvbiAgICAgICBUaGUgdmVy
# c2lvbiBvZiB0aGUgYXBwbGljYXRpb24KIyMjIyAgIDpsaWJkaXIgICAgICAg
# IFRoZSBkaXJlY3Rvcnkgd2hlcmUgbGlicmFyaWVzIGFyZSwgYnkgZGVmYXVs
# dCBpcyAkKHByZWZpeCkvbGliCiMjIyMgICA6aW5jbHVkZWRpciAgICBUaGUg
# ZGlyZWN0b3J5IHdoZXJlIGhlYWRlcnMgYXJlLCBieSBkZWZhdWx0IGlzICQo
# cHJlZml4KS9pbmNsdWRlCiMjIyMgICA6bGlicyAgICAgICAgICBMaWJyYXJp
# ZXMgdGhhdCBsaW5rcyB0aGUgcGFja2FnZQojIyMjICAgOmNmbGFncyAgICAg
# ICAgQ2ZsYWdzIGZvciB0aGUgcGFja2FnZQojIyMjICAgOnJlcXVpcmVzICAg
# ICAgQSBsaXN0IG9mIHBhY2thZ2UgbmFtZXMgcmVxdWlyZWQgZm9yIHRoaXMg
# cGFja2FnZS4KIyMjIyAKIyBzZXR1cC5nZW5lcmF0ZV9wa2djb25maWcoOnBh
# Y2thZ2VfbmFtZSA9PiAiZGxpYi1lZGl0b3IiLCA6bmFtZSA9PiAiRExpYiBR
# dCBsaWJyYXJ5IiwgOmRlc2NyaXB0aW9uID0+ICJBIGV4dGVuc2lvbiBmb3Ig
# UXQ0IiwgOnZlcnNpb24gPT4gIjAuMWJldGEiLCA6bGliZGlyID0+IG5pbCwg
# OmluY2x1ZGVkaXIgPT4gbmlsLCA6bGlicyA9PiAiLUwke2xpYmRpcn0gLWxk
# Y29yZSAtbGRndWkgLWxkZWRpdG9yIiwgOmNmbGFncyA9PiAiLUkke2luY2x1
# ZGVkaXJ9IiwgOnJlcXVpcmVzID0+IFsic29tZXBhY2thZ2UiLCAib3RoZXJw
# YWNrYWdlIiBdICkKZW5kCgojIFRoaXMgbWV0aG9kIGlzIGNhbGxlZCBiZWZv
# cmUgdG8gc2VhcmNoIGEgcGFja2FnZQpkZWYgc2V0dXBfcGtnY29uZmlnKHBr
# Z2NvbmZpZywgYXJncykKIyAgIGNhc2UgcGtnY29uZmlnLmlkCiMgICAgIHdo
# ZW4gImRidXMtMSIKIyAgICAgICBwa2djb25maWcuYWRkX3NlYXJjaF9wYXRo
# KCIvYS9wYXRoL3RvL3RoZS9wYyIpCiMgICBlbmQKZW5kCgpkZWYgc2V0dXBf
# dGVzdChpZCwgdHN0LCBhcmdzKQojICAgY2FzZSBpZAojICAgICB3aGVuICJm
# ZWF0dXJlIgojICAgICAgIHRzdC5hZGRfZGVmaW5lKCJNWURFRklORSIpCiMg
# ICAgICAgdHN0LmFkZF9saWIoIi1sbXlsaWIiKQojICAgICAgIHRzdC5hZGRf
# aW5jbHVkZV9wYXRoKCIvc29tZS9pbmNsdWRlL3BhdGgvcmVxdWlyZWQvdG8v
# Y29tcGlsZS90aGUvdGVzdCIpCiMgICBlbmQKZW5kCgpkZWYgc2V0dXBfY29u
# ZmlnKGNmZywgYXJncykKIyAgIGNmZy5hZGRfaW5jbHVkZV9wYXRoIEZpbGUu
# ZXhwYW5kX3BhdGgoInNyYyIpCiMgICBjZmcuYWRkX2RlZmluZSgiX19TVERD
# X0NPTlNUQU5UX01BQ1JPUyIpCiMgICBjZmcuYWRkX3F0bW9kdWxlKCJ4bWwi
# KQojICAgY2ZnLmFkZF9xdG1vZHVsZSgibmV0d29yayIpCmVuZAoKCgAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAHFvbmZpZ3VyZS9tYWtlZmlsZS5yYgAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAwMDAwNjQ0ADAwMDE3NTEAMDAwMTc1MQAwMDAwMDAw
# MzI3NQAxMDcxMzc0NTAyNQAwMTMzMjEAIDAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdXN0YXIgIABr
# cmF3ZWsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAADQpyZXF1aXJlICdyYmNvbmZpZycNCg0K
# bW9kdWxlIFFvbmYNCiAgY2xhc3MgTWFrZWZpbGUNCiAgICBkZWYgc2VsZi5m
# aW5kX21ha2VmaWxlcyhwYXRoKQ0KICAgICAgbWFrZWZpbGVzID0gW10NCiAg
# ICAgIERpci5mb3JlYWNoKHBhdGgpIHsgfGZ8DQogICAgICAgIGZpbGUgPSAi
# I3twYXRofS8je2Z9Ig0KICAgICAgICBpZiBGaWxlLnN0YXQoZmlsZSkuZGly
# ZWN0b3J5Pw0KICAgICAgICAgIGlmIG5vdCBmID1+IC9eXC4vDQogICAgICAg
# ICAgICBtYWtlZmlsZXMuY29uY2F0IGZpbmRfbWFrZWZpbGVzKGZpbGUpDQog
# ICAgICAgICAgZW5kDQogICAgICAgIGVsc2lmIGYuZG93bmNhc2UgPX4gL15t
# YWtlZmlsZSguZGVidWd8LnJlbGVhc2UpezAsMX0vDQogICAgICAgICAgbWFr
# ZWZpbGVzIDw8IGZpbGUNCiAgICAgICAgZW5kDQogICAgICB9DQogICAgICBt
# YWtlZmlsZXMNCiAgICBlbmQNCiAgICANCiAgICBkZWYgc2VsZi5vdmVycmlk
# ZShtYWtlZmlsZSwgZGVzdGRpciwgc3RhdHVzRmlsZSkNCiAgICAgIG5ld21h
# a2VmaWxlID0gIiINCiAgICAgIA0KICAgICAgcnVieV9iaW4gPSAiI3s6OkNv
# bmZpZzo6Q09ORklHWyJiaW5kaXIiXX0vI3s6OkNvbmZpZzo6Q09ORklHWyJy
# dWJ5X2luc3RhbGxfbmFtZSJdfSINCiAgICAgIA0KICAgICAgRmlsZS5vcGVu
# KG1ha2VmaWxlLCAiciIpIHsgfGZ8DQogICAgICAgIGxpbmVzID0gZi5yZWFk
# bGluZXMNCiAgICAgICAgDQogICAgICAgIGlmIEZpbGU6OkFMVF9TRVBBUkFU
# T1INCiAgICAgICAgICBkZXN0ZGlyLmdzdWIhKCIvIiwgRmlsZTo6QUxUX1NF
# UEFSQVRPUikNCiAgICAgICAgZW5kDQogICAgICAgIA0KICAgICAgICBpbmRl
# eCA9IDANCiAgICAgICAgcmVwbGFjZWQgPSBmYWxzZQ0KICAgICAgICB3aGls
# ZSBpbmRleCA8IGxpbmVzLnNpemUNCiAgICAgICAgICBsaW5lID0gbGluZXNb
# aW5kZXhdDQogICAgICAgICAgaWYgbGluZSA9fiAvXlxzK1tAXXswLDF9XCRc
# KFFNQUtFXCkvDQogICAgICAgICAgICBuZXdtYWtlZmlsZSArPSBsaW5lDQog
# ICAgICAgICAgICBuZXdtYWtlZmlsZSArPSAiXHQje3J1YnlfYmlufSAje3N0
# YXR1c0ZpbGV9ICN7bWFrZWZpbGV9XG4iDQogICAgICAgICAgICBpbmRleCAr
# PSAxDQogICAgICAgICAgZWxzZQ0KICAgICAgICAgICAgaWYgbm90IHJlcGxh
# Y2VkDQogICAgICAgICAgICAgIGlmIGxpbmUgPX4gL15JTlNUQUxMX0RJUi8N
# CiAgICAgICAgICAgICAgICBuZXdtYWtlZmlsZSArPSAiSU5TVEFMTF9ST09U
# ID0gI3tkZXN0ZGlyfVxuIg0KICAgICAgICAgICAgICAgIHJlcGxhY2VkID0g
# dHJ1ZQ0KICAgICAgICAgICAgICBlbmQNCiAgICAgICAgICAgIGVsc2UNCiAg
# ICAgICAgICAgICAgaWYgbGluZSA9fiAvXCRcKElOU1RBTExfUk9PVFwpLw0K
# ICAgICAgICAgICAgICAgIGlmIEZpbGU6OkFMVF9TRVBBUkFUT1INCiAgICAg
# ICAgICAgICAgICAgIGxpbmUuZ3N1YiEoIi8iLCBGaWxlOjpBTFRfU0VQQVJB
# VE9SKQ0KICAgICAgICAgICAgICAgIGVuZA0KICAgICAgICAgICAgICBlbmQN
# CiAgICAgICAgICAgIGVuZA0KICAgICAgICAgICAgbmV3bWFrZWZpbGUgKz0g
# bGluZQ0KICAgICAgICAgIGVuZA0KICAgICAgICAgIGluZGV4ICs9IDENCiAg
# ICAgICAgZW5kDQogICAgICB9DQogICAgICANCiAgICAgIEZpbGUub3Blbiht
# YWtlZmlsZSwgInciKSB7IHxmfA0KICAgICAgICBmIDw8IG5ld21ha2VmaWxl
# DQogICAgICB9DQogICAgZW5kDQogIGVuZA0KZW5kDQoNCg0KAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABx
# b25maWd1cmUvaW5pdC5yYn4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAMDAwMDY0NAAwMDAxNzUxADAwMDE3NTEAMDAwMDAwMDAzMzAA
# MTA2NzM1MDIwNDMAMDEyNjY2ACAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHVzdGFyICAAa3Jhd2Vr
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAApyZXF1aXJlICdxb25mJwoKRGlyLmNoZGlyKEZp
# bGUuZGlybmFtZShvbGRsb2NhdGlvbigicW9uZi5yYiIpKSkKCnFvbmYgPSBR
# b25mOjpDb25maWd1cmUubmV3CnFvbmYuZ2V0X2FyZ3MoQVJHVikKcW9uZi5m
# aW5kX3BhY2thZ2VzCnFvbmYucnVuX3Rlc3RzCgpxb25mLnNhdmUoRmlsZS5l
# eHBhbmRfcGF0aCgiY29uZmlnLnByaSIpKQoKcW9uZi5nZW5lcmF0ZV9tYWtl
# ZmlsZXMKCgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcW9uZmlndXJlL2lu
# aXQucmIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAw
# MDA2NDQAMDAwMTc1MQAwMDAxNzUxADAwMDAwMDAwMzA2ADEwNjczNzQwMzAz
# ADAxMjQ3NgAgMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAB1c3RhciAgAGtyYXdlawAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAKcmVxdWlyZSAncW9uZicKCkRpci5jaGRpcihGaWxlLmRpcm5hbWUo
# b2xkbG9jYXRpb24oInFvbmYucmIiKSkpCgpxb25mID0gUW9uZjo6Q29uZmln
# dXJlLm5ldwpxb25mLmdldF9hcmdzKEFSR1YpCnFvbmYuZmluZF9wYWNrYWdl
# cwpxb25mLnJ1bl90ZXN0cwoKcW9uZi5zYXZlKCJjb25maWcucHJpIikKCnFv
# bmYuZ2VuZXJhdGVfbWFrZWZpbGVzCgoAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAHFvbmZpZ3VyZS9xb25mLnJifgAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwMDAwNjQ0ADAwMDE3
# NTEAMDAwMTc1MQAwMDAwMDAyNjQ2NAAxMDcwMjMxMzIyMwAwMTI2NzcAIDAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAdXN0YXIgIABrcmF3ZWsAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACnJlcXVp
# cmUgJ3Rlc3QnCnJlcXVpcmUgJ3BrZ2NvbmZpZycKcmVxdWlyZSAnZnJvbnRl
# bmRmYWN0b3J5JwpyZXF1aXJlICdjb25maWcnCnJlcXVpcmUgJ3FtYWtlJwpy
# ZXF1aXJlICdtYWtlZmlsZScKCm1vZHVsZSBRb25mCiAgY2xhc3MgUXRWZXJz
# aW9uCiAgICBhdHRyX3JlYWRlciA6dmVyc2lvbiwgOm9wICMgMDogZXF1YWwg
# MTogZ3JlYXRlciAyOiBncmVhdGVyIGVxdWFsCiAgICAKICAgIGRlZiBpbml0
# aWFsaXplKHZlcnNpb24pCiAgICAgIEBvcCA9IDIKICAgICAgcGFyc2UodmVy
# c2lvbikKICAgIGVuZAogICAgCiAgICBkZWYgPT0odmVyKQogICAgICBAb3Ag
# PSAwCiAgICAgIHBhcnNlKHZlcikKICAgIGVuZAogICAgCiAgICBkZWYgPih2
# ZXIpCiAgICAgIEBvcCA9IDEKICAgICAgcGFyc2UodmVyKQogICAgZW5kCiAg
# ICAKICAgIGRlZiA+PSh2ZXIpCiAgICAgIEBvcCA9IDIKICAgICAgcGFyc2Uo
# dmVyKQogICAgZW5kCiAgICAKICAgIGRlZiB0b19zCiAgICAgIHMgPSAiUXQg
# IgogICAgICBjYXNlIEBvcAogICAgICAgIHdoZW4gMAogICAgICAgICAgcys9
# Ij09IgogICAgICAgIHdoZW4gMQogICAgICAgICAgcys9Ij4iCiAgICAgICAg
# ZWxzZQogICAgICAgICAgcys9Ij49IgogICAgICBlbmQKICAgICAgcys9ICIg
# IitAdmVyc2lvbgogICAgICAKICAgICAgcwogICAgZW5kCiAgICAKICAgIHBy
# aXZhdGUKICAgIGRlZiBwYXJzZSh2ZXJzaW9uKQogICAgICBAdmVyc2lvbiA9
# IHZlcnNpb24udG9fcwogICAgICB2ZXIgPSBAdmVyc2lvbi50b19zLnNwbGl0
# KCcuJykKICAgICAgc2l6ZSA9IHZlci5zaXplCiAgICAgICAgCiAgICAgIGZp
# bmFsdmVyID0gW10KICAgICAgMy50aW1lcyB7IHxpfAogICAgICAgIHRwID0g
# dmVyW2ldCiAgICAgICAgCiAgICAgICAgaWYgdHAubmlsPwogICAgICAgICAg
# dHAgPSAiMCIKICAgICAgICBlbmQKICAgICAgICAKICAgICAgICBmaW5hbHZl
# ciA8PCB0cAogICAgICB9CiAgICAgIAogICAgICBAdmVyc2lvbiA9IGZpbmFs
# dmVyLmpvaW4oIi4iKQogICAgZW5kCiAgZW5kCiAgCiAgY2xhc3MgU2V0dXAK
# ICAgIGF0dHJfcmVhZGVyIDpvcHRpb25zLCA6cGFja2FnZXMsIDp0ZXN0cywg
# OnBrZ19jb25maWdzCiAgICBkZWYgaW5pdGlhbGl6ZQogICAgICBAb3B0aW9u
# cyA9IFtdCiAgICAgIEBwYWNrYWdlcyA9IFtdCiAgICAgIEB0ZXN0cyA9IFtd
# CiAgICAgIEBwa2dfY29uZmlncyA9IFtdCiAgICAgIEBxdF92ZXJzaW9uID0g
# UXRWZXJzaW9uLm5ldygiNC4wLjAiKQogICAgICAKICAgICAgYWRkX29wdGlv
# big6bmFtZSA9PiAicHJlZml4IiwgOnR5cGUgPT4gInBhdGgiLCA6ZGVmYXVs
# dCA9PiAiL3Vzci9sb2NhbC8iLCA6b3B0aW9uYWwgPT4gdHJ1ZSwgOmRlc2Ny
# aXB0aW9uID0+ICJTZXRzIHRoZSBpbnN0YWxsIHByZWZpeCIpCiAgICBlbmQK
# ICAgIAogICAgZGVmIGFkZF9vcHRpb24ob3B0aW9uKQogICAgICBpZiBub3Qg
# b3B0aW9uWzp0eXBlXS5uaWw/CiAgICAgICAgb3B0aW9uWzp0eXBlXS5kb3du
# Y2FzZSEKICAgICAgZW5kCiAgICAgIAogICAgICBAb3B0aW9ucyA8PCBvcHRp
# b24KICAgIGVuZAogICAgCiAgICBkZWYgZmluZF9wYWNrYWdlKG9wdGlvbnMp
# CiAgICAgIEBwYWNrYWdlcyA8PCBvcHRpb25zCiAgICBlbmQKICAgIAogICAg
# ZGVmIGFkZF90ZXN0KG9wdGlvbikKICAgICAgcmFpc2UgIlBsZWFzZSBzZXQg
# YSB2YWxpZCBuYW1lIHRvIHRoZSB0ZXN0IiBpZiBvcHRpb25bOm5hbWVdLnRv
# X3MuZW1wdHk/CiAgICAgIHJhaXNlICJQbGVhc2Ugc2V0IGEgdmFsaWQgaWQg
# dG8gdGhlIHRlc3QiIGlmIG9wdGlvbls6aWRdLnRvX3MuZW1wdHk/CiAgICAg
# IAogICAgICBAdGVzdHMgPDwgb3B0aW9uCiAgICBlbmQKICAgIAogICAgZGVm
# IGdlbmVyYXRlX3BrZ2NvbmZpZyhvcHRpb25zKQogICAgICByYWlzZSAiUGxl
# YXNlIHNldCBhIHZhbGlkIG5hbWUiIGlmIG9wdGlvbnNbOnBhY2thZ2VfbmFt
# ZV0udG9fcy5lbXB0eT8KICAgICAgQHBrZ19jb25maWdzIDw8IG9wdGlvbnMK
# ICAgIGVuZAogICAgCiAgICBkZWYgcXQKICAgICAgQHF0X3ZlcnNpb24KICAg
# IGVuZAogICAgCiAgZW5kCiAgCiAgY2xhc3MgQ29uZmlndXJlCiAgICBkZWYg
# aW5pdGlhbGl6ZQogICAgICBAYXJndW1lbnRzID0ge30KICAgICAgQGNvbmZp
# ZyA9IENvbmZpZy5uZXcKICAgICAgCiAgICAgIEBsb2dfZmlsZSA9IG9sZGxv
# Y2F0aW9uKCJjb25maWcubG9nIikKICAgICAgCiAgICAgIEZpbGUub3BlbihA
# bG9nX2ZpbGUsICJ3IikgZG8gfGZ8CiAgICAgICAgZiA8PCAiPT09PT09PT4g
# R2VuZXJhdGVkIGF0ICN7VGltZS5ub3d9IDw9PT09PT09XG4iCiAgICAgIGVu
# ZAogICAgICAKICAgICAgQHFtYWtlID0gUU1ha2UubmV3KEBsb2dfZmlsZSkK
# ICAgICAgCiAgICAgIGNmZyA9IFNldHVwLm5ldwogICAgICBjb25maWd1cmUo
# Y2ZnKQogICAgICAKICAgICAgcHJpbnQgIj0+IFNlYXJjaGluZyBmb3IgI3tj
# ZmcucXR9Li4uICIKICAgICAgaWYgQHFtYWtlLmZpbmRfcW1ha2UoY2ZnLnF0
# LnZlcnNpb24sIGNmZy5xdC5vcCkKICAgICAgICBwdXRzICJbWUVTXSIKICAg
# ICAgZWxzZQogICAgICAgIHB1dHMgIltOT10iCiAgICAgICAgZXhpdCAtMQog
# ICAgICBlbmQKICAgICAgCiAgICAgIEBvcHRpb25zID0gY2ZnLm9wdGlvbnMK
# ICAgICAgQHBhY2thZ2VzID0gY2ZnLnBhY2thZ2VzCiAgICAgIEB0ZXN0cyA9
# IGNmZy50ZXN0cwogICAgICBAcGtnX2NvbmZpZ3MgPSBjZmcucGtnX2NvbmZp
# Z3MKICAgICAgQHN0YXR1c0ZpbGUgPSBEaXIuZ2V0d2QrIi91cGRhdGUtbWFr
# ZWZpbGUiCiAgICBlbmQKICAgIAogICAgZGVmIGZpbmRfcGFja2FnZXMgIyBU
# T0RPOiBzb3BvcnRhciBpbmZvcm1hY2lvbiBwYXJhIGVsIHNpc3RlbWEgb3Bl
# cmF0aXZvIHNvYnJlIGNvbW8gaW5zdGFsYXIgZWwgcGFxdWV0ZQogICAgICBA
# cGFja2FnZXMuZWFjaCB7IHxwYWNrYWdlfAogICAgICAgIHByaW50ICI9PiBT
# ZWFyY2hpbmcgZm9yIHBhY2thZ2U6ICN7cGFja2FnZVs6bmFtZV19Li4uICAi
# CiAgICAgICAgcGtnY29uZmlnID0gUGtnQ29uZmlnLm5ldyhwYWNrYWdlWzpu
# YW1lXSkKICAgICAgICBiZWdpbgogICAgICAgICAgc2V0dXBfcGtnY29uZmln
# KHBrZ2NvbmZpZywgQGFyZ3VtZW50cykKICAgICAgICByZXNjdWUgTm9NZXRo
# b2RFcnJvciA9PiBlCiAgICAgICAgICBpZiBub3QgZS5tZXNzYWdlID1+IC9z
# ZXR1cF9wa2djb25maWcvCiAgICAgICAgICAgIHB1dHMgIkVycm9yOiAje2Uu
# bWVzc2FnZX0iCiAgICAgICAgICAgIHB1dHMgZS5iYWNrdHJhY2UKICAgICAg
# ICAgICAgZXhpdCAtMQogICAgICAgICAgZW5kCiAgICAgICAgZW5kCiAgICAg
# ICAgCiAgICAgICAgaWYgbm90IHBrZ2NvbmZpZy5maW5kKHBhY2thZ2VbOm5h
# bWVdKQogICAgICAgICAgcHV0cyAiW05PXSIKICAgICAgICAgIAogICAgICAg
# ICAgb3B0aW9uYWwgPSBwYWNrYWdlWzpvcHRpb25hbF0ubmlsPyA/IGZhbHNl
# IDogcGFja2FnZVs6b3B0aW9uYWxdCiAgICAgICAgICAKICAgICAgICAgIGlm
# IG5vdCBvcHRpb25hbAogICAgICAgICAgICBwdXRzICI9PiBUaGUgcGFja2Fn
# ZSAnI3twYWNrYWdlWzpuYW1lXX0nIGlzIHJlcXVpcmVkLiIKICAgICAgICAg
# ICAgZXhpdCAtMQogICAgICAgICAgZW5kCiAgICAgICAgZWxzZQogICAgICAg
# ICAgcHV0cyAiW1lFU10iCiAgICAgICAgICBnbG9iYWwgPSBwYWNrYWdlWzpn
# bG9iYWxdLm5pbD8gPyBmYWxzZSA6IHBhY2thZ2VbOmdsb2JhbF0KICAgICAg
# ICAgIAogICAgICAgICAgcW9uZnBrZyA9IEBjb25maWcKICAgICAgICAgIAog
# ICAgICAgICAgaWYgbm90IGdsb2JhbAogICAgICAgICAgICBxb25mcGtnID0g
# UGFja2FnZS5uZXcocGFja2FnZVs6bmFtZV0pCiAgICAgICAgICBlbmQKICAg
# ICAgICAgIAogICAgICAgICAgCiMgICAgICAgICAgIHBrZ2NvbmZpZy5yZXF1
# aXJlcy5lYWNoIHsgfHJlcXwKIyAgICAgICAgICAgICBAY29uZmlnLmFkZF9x
# b25mX3BhY2thZ2UocmVxKQojICAgICAgICAgICB9CiAgICAgICAgICAKICAg
# ICAgICAgIHFvbmZwa2cuYWRkX2xpYihwa2djb25maWcubGlicykKICAgICAg
# ICAgIAogICAgICAgICAgcGtnY29uZmlnLmluY2x1ZGVfcGF0aC5lYWNoIHsg
# fGluY3BhdGh8CiAgICAgICAgICAgIHFvbmZwa2cuYWRkX2luY2x1ZGVfcGF0
# aChpbmNwYXRoKQogICAgICAgICAgfQogICAgICAgICAgCiAgICAgICAgICBw
# a2djb25maWcuZGVmaW5lcy5lYWNoIHsgfGRlZmluZXwKICAgICAgICAgICAg
# cW9uZnBrZy5hZGRfZGVmaW5lKGRlZmluZSkKICAgICAgICAgIH0KICAgICAg
# ICAgIAogICAgICAgICAgaWYgbm90IGdsb2JhbAogICAgICAgICAgICBAY29u
# ZmlnLmFkZF9xb25mX3BhY2thZ2UocW9uZnBrZykKICAgICAgICAgIGVsc2UK
# ICAgICAgICAgICAgQGNvbmZpZy5hZGRfcW9uZl9mZWF0dXJlKHBhY2thZ2Vb
# Om5hbWVdKQogICAgICAgICAgZW5kCiAgICAgICAgZW5kCiAgICAgIH0KICAg
# IGVuZAogICAgCiAgICBkZWYgcnVuX3Rlc3RzCiAgICAgIEB0ZXN0cy5lYWNo
# IHsgfHRlc3RvcHR8CiAgICAgICAgcHJpbnQgIj0+IFRlc3RpbmcgI3t0ZXN0
# b3B0WzpuYW1lXX0uLi4gICIKICAgICAgICAKICAgICAgICB0c3QgPSBUZXN0
# Lm5ldyh0ZXN0b3B0LCBAY29uZmlnLCBAbG9nX2ZpbGUpCiAgICAgICAgCiAg
# ICAgICAgYmVnaW4KICAgICAgICAgIHNldHVwX3Rlc3QodGVzdG9wdFs6aWRd
# LCB0c3QsIEBhcmd1bWVudHMpCiAgICAgICAgcmVzY3VlIE5vTWV0aG9kRXJy
# b3IgPT4gZQogICAgICAgICAgaWYgbm90IGUubWVzc2FnZSA9fiAvc2V0dXBf
# dGVzdC8KICAgICAgICAgICAgcHV0cyAiRXJyb3I6ICN7ZX0iCiAgICAgICAg
# ICAgIHB1dHMgZS5iYWNrdHJhY2UKICAgICAgICAgICAgZXhpdCAtMQogICAg
# ICAgICAgZW5kCiAgICAgICAgZW5kCiAgICAgICAgCiAgICAgICAgb3B0aW9u
# YWwgPSB0ZXN0b3B0WzpvcHRpb25hbF0ubmlsPyA/IGZhbHNlIDogdGVzdG9w
# dFs6b3B0aW9uYWxdCiAgICAgICAgaWYgdHN0LnJ1bihAcW1ha2UpCiAgICAg
# ICAgICBnbG9iYWwgPSB0ZXN0b3B0WzpnbG9iYWxdLm5pbD8gPyBmYWxzZSA6
# IHRlc3RvcHRbOmdsb2JhbF0KICAgICAgICAgIAogICAgICAgICAgcW9uZnBr
# ZyA9IEBjb25maWcKICAgICAgICAgIAogICAgICAgICAgaWYgbm90IGdsb2Jh
# bAogICAgICAgICAgICBxb25mcGtnID0gUGFja2FnZS5uZXcodHN0LmlkKQog
# ICAgICAgICAgZW5kCiAgICAgICAgICAKICAgICAgICAgIHRzdC5pbmNsdWRl
# X3BhdGguZWFjaCB7IHxpbmNwYXRofAogICAgICAgICAgICBxb25mcGtnLmFk
# ZF9pbmNsdWRlX3BhdGgoaW5jcGF0aCkKICAgICAgICAgIH0KICAgICAgICAg
# IAogICAgICAgICAgdHN0LmRlZmluZXMuZWFjaCB7IHxkZWZpbmV8CiAgICAg
# ICAgICAgIHFvbmZwa2cuYWRkX2RlZmluZShkZWZpbmUpCiAgICAgICAgICB9
# CiAgICAgICAgICAKICAgICAgICAgIHFvbmZwa2cuYWRkX2xpYih0c3QubGli
# cy50b19zKQogICAgICAgICAgCiAgICAgICAgICBpZiBub3QgZ2xvYmFsCiAg
# ICAgICAgICAgIEBjb25maWcuYWRkX3FvbmZfcGFja2FnZShxb25mcGtnKQog
# ICAgICAgICAgZWxzZQogICAgICAgICAgICBAY29uZmlnLmFkZF9xb25mX2Zl
# YXR1cmUodHN0LmlkKQogICAgICAgICAgZW5kCiAgICAgICAgICBwdXRzICJb
# T0tdIgogICAgICAgIGVsc2UKICAgICAgICAgIHB1dHMgIltGQUlMRURdIgog
# ICAgICAgICAgCiAgICAgICAgICBpZiBub3Qgb3B0aW9uYWwKICAgICAgICAg
# ICAgcHV0cyAiPT4gI3t0ZXN0b3B0WzpuYW1lXX0gaXMgcmVxdWlyZWQiCiAg
# ICAgICAgICAgIGV4aXQgLTEKICAgICAgICAgIGVuZAogICAgICAgIGVuZAog
# ICAgICB9CiAgICBlbmQKICAgIAogICAgZGVmIGdlbmVyYXRlX21ha2VmaWxl
# cwogICAgICBwdXRzICI9PiBDcmVhdGluZyBtYWtlZmlsZXMuLi4iCiAgICAg
# IEBxbWFrZS5ydW4oIiIsIHRydWUpCiAgICAgIAogICAgICBwdXRzICI9PiBV
# cGRhdGluZyBtYWtlZmlsZXMuLi4iCiAgICAgIAogICAgICBAbWFrZWZpbGVz
# ID0gTWFrZWZpbGU6OmZpbmRfbWFrZWZpbGVzKERpci5nZXR3ZCkKICAgICAg
# QG1ha2VmaWxlcy5lYWNoIHsgfG1ha2VmaWxlfAogICAgICAgICAgTWFrZWZp
# bGU6Om92ZXJyaWRlKG1ha2VmaWxlLCBGaWxlLmV4cGFuZF9wYXRoKEBhcmd1
# bWVudHNbInByZWZpeCJdLnRvX3MpLCBAc3RhdHVzRmlsZSkKICAgICAgfQog
# ICAgICAKICAgICAgRmlsZS5vcGVuKEBzdGF0dXNGaWxlLCAidyIgKSB7IHxm
# aWxlfAogICAgICAgIGZpbGUgPDwgJUAKI3tGaWxlLnJlYWQobmV3bG9jYXRp
# b24oIm1ha2VmaWxlLnJiIikpfQpRb25mOjpNYWtlZmlsZTo6b3ZlcnJpZGUo
# IEFSR1ZbMF0udG9fcywgIiN7QGFyZ3VtZW50c1sicHJlZml4Il19IiwgIiN7
# QHN0YXR1c0ZpbGV9IiApCkAKICAgICAgfQogICAgZW5kCiAgICAKICAgIGRl
# ZiBnZXRfYXJncyhhcmd2KQogICAgICBvcHRjID0gMAogICAgICBsYXN0X29w
# dCA9ICIiCiAgICAgIAogICAgICBAb3B0aW9ucy5lYWNoIHsgfG9wdGlvbnwK
# ICAgICAgICBnaXZlbl9vcHRpb24gPSBuaWwKICAgICAgICAKICAgICAgICBh
# cmd2LmVhY2ggeyB8YXJnfAogICAgICAgICAgYXJnLnN0cmlwIQogICAgICAg
# ICAgZ2l2ZW4gPSBmYWxzZQogICAgICAgICAgCiAgICAgICAgICBjYXNlIG9w
# dGlvbls6dHlwZV0udG9fcwogICAgICAgICAgICB3aGVuICJzdHJpbmciCiAg
# ICAgICAgICAgICAgaWYgYXJnID1+IC8tLSN7b3B0aW9uWzpuYW1lXX09KFtc
# V1x3XSopLwogICAgICAgICAgICAgICAgZ2l2ZW4gPSB0cnVlCiAgICAgICAg
# ICAgICAgICBnaXZlbl9vcHRpb24gPSAkMQogICAgICAgICAgICAgIGVuZAog
# ICAgICAgICAgICB3aGVuICJwYXRoIgogICAgICAgICAgICAgIGlmIGFyZyA9
# fiAvLS0je29wdGlvbls6bmFtZV19PShbXFdcd10qKS8KICAgICAgICAgICAg
# ICAgIGdpdmVuID0gdHJ1ZQogICAgICAgICAgICAgICAgZ2l2ZW5fb3B0aW9u
# ID0gRmlsZS5leHBhbmRfcGF0aCgkMSkKICAgICAgICAgICAgICBlbmQKICAg
# ICAgICAgICAgd2hlbiAiYm9vbCIKICAgICAgICAgICAgICBpZiBhcmcgPX4g
# Ly0tdXNlLSN7b3B0aW9uWzpuYW1lXX09KFx3KykvCiAgICAgICAgICAgICAg
# ICBnaXZlbiA9IHRydWUKICAgICAgICAgICAgICAgIGdpdmVuX29wdGlvbiA9
# ICQxCiAgICAgICAgICAgICAgZW5kCiAgICAgICAgICAgIGVsc2UKICAgICAg
# ICAgICAgICBpZiBhcmcgPX4gLy0tI3tvcHRpb25bOm5hbWVdfS8KICAgICAg
# ICAgICAgICAgIGdpdmVuX29wdGlvbiA9ICJ5ZXMiCiAgICAgICAgICAgICAg
# ICBnaXZlbiA9IHRydWUKICAgICAgICAgICAgICBlbmQKICAgICAgICAgIGVu
# ZAogICAgICAgICAgCiAgICAgICAgICBpZiBnaXZlbgogICAgICAgICAgICBh
# cmd2LmRlbGV0ZShhcmcpCiAgICAgICAgICBlbmQKICAgICAgICB9CiAgICAg
# ICAgCiAgICAgICAgb3B0aW9uWzp2YWx1ZV0gPSBnaXZlbl9vcHRpb24KICAg
# ICAgfQogICAgICAKICAgICAgZnJvbnRlbmRfa2V5ID0gInRrIgogICAgICBh
# cmd2LmVhY2ggeyB8dW5rbm93bnwKICAgICAgICB1bmtub3duLnN0cmlwIQog
# ICAgICAgIGlmIHVua25vd24gPX4gLy0td2l0aC1jb25maWctZnJvbnRlbmQ9
# KFx3KykvCiAgICAgICAgICBmcm9udGVuZF9rZXkgPSAkMQogICAgICAgIGVs
# c2lmIHVua25vd24gPX4gLy0taGVscC8KICAgICAgICAgIHVzYWdlCiAgICAg
# ICAgICBleGl0IDAKICAgICAgICBlbmQKICAgICAgfQogICAgICAKICAgICAg
# ZnJvbnRlbmQgPSBGcm9udGVuZEZhY3RvcnkuY3JlYXRlKGZyb250ZW5kX2tl
# eSkKICAgICAgaWYgZnJvbnRlbmQubmlsPwogICAgICAgIHB1dHMgIj0+IElu
# dmFsaWQgZnJvbnRlbmQiCiAgICAgICAgdXNhZ2UoKQogICAgICAgIGV4aXQg
# LTEKICAgICAgZWxzaWYgbm90IGZyb250ZW5kLnBhcnNlKEBvcHRpb25zKQog
# ICAgICAgIHVzYWdlKCkKICAgICAgICBleGl0IC0xCiAgICAgIGVuZAogICAg
# ICAKICAgICAgQG9wdGlvbnMuZWFjaCB7IHxvcHRpb258CiAgICAgICAgdmFs
# dWUgPSBvcHRpb25bOnZhbHVlXS5uaWw/ID8gb3B0aW9uWzpkZWZhdWx0XS50
# b19zIDogb3B0aW9uWzp2YWx1ZV0KICAgICAgICBAYXJndW1lbnRzW29wdGlv
# bls6bmFtZV1dID0gdmFsdWUKICAgICAgfQogICAgICAKICAgIGVuZAogICAg
# CiAgICBkZWYgc2F2ZShwYXRoKQogICAgICBiZWdpbgogICAgICAgIHNldHVw
# X2NvbmZpZyhAY29uZmlnLCBAYXJndW1lbnRzKQogICAgICByZXNjdWUgTm9N
# ZXRob2RFcnJvciA9PiBlCiAgICAgICAgaWYgbm90IGUubWVzc2FnZSA9fiAv
# c2V0dXBfY29uZmlnLwogICAgICAgICAgcHV0cyAiRXJyb3I6ICN7ZX0iCiAg
# ICAgICAgICBwdXRzIGUuYmFja3RyYWNlCiAgICAgICAgICBleGl0IC0xCiAg
# ICAgICAgZW5kCiAgICAgIGVuZAogICAgICAKICAgICAgQGNvbmZpZy5hZGRf
# ZGVmaW5lKCVAX19QUkVGSVhfXz0nXFxcXCInI3tGaWxlLmV4cGFuZF9wYXRo
# IEBhcmd1bWVudHNbInByZWZpeCJdfSdcXFxcIidAKQogICAgICAKICAgICAg
# c2F2ZV9wa2djb25maWcKICAgICAgQGNvbmZpZy5zYXZlKHBhdGgpCiAgICBl
# bmQKICAgIAogICAgZGVmIHNhdmVfcGtnY29uZmlnCiAgICAgIGlmIG5vdCBA
# cGtnX2NvbmZpZ3MuZW1wdHk/CiAgICAgICAgQHBrZ19jb25maWdzLmVhY2gg
# eyB8cGtnX2NvbmZpZ3wKICAgICAgICAgIEZpbGUub3BlbiggcGtnX2NvbmZp
# Z1s6cGFja2FnZV9uYW1lXSsiLnBjIiwgInciKSBkbyB8ZmlsZXwKICAgICAg
# ICAgICAgZmlsZSA8PCAicHJlZml4PSIgPDwgQGFyZ3VtZW50c1sicHJlZml4
# Il0gPDwgIlxuIgogICAgICAgICAgICBmaWxlIDw8ICJleGVjX3ByZWZpeD0k
# e3ByZWZpeH0iIDw8ICJcbiIKICAgICAgICAgICAgZmlsZSA8PCAibGliZGly
# PSN7cGtnX2NvbmZpZ1s6bGliZGlyXS5uaWw/ID8gIiR7cHJlZml4fS9saWIi
# IDogcGtnX2NvbmZpZ1s6bGliZGlyXSB9IiA8PCAiXG4iCiAgICAgICAgICAg
# IGZpbGUgPDwgImluY2x1ZGVkaXI9I3twa2dfY29uZmlnWzppbmNsdWRlZGly
# XS5uaWw/ID8gIiR7cHJlZml4fS9pbmNsdWRlIiA6IHBrZ19jb25maWdbOmlu
# Y2x1ZGVkaXJdIH0iIDw8ICJcblxuXG4iCiAgICAgICAgICAgIAogICAgICAg
# ICAgICBmaWxlIDw8ICJOYW1lOiAiIDw8IHBrZ19jb25maWdbOnBhY2thZ2Vf
# bmFtZV0gPDwgIlxuIgogICAgICAgICAgICBmaWxlIDw8ICJEZXNjcmlwdGlv
# bjogIiA8PCBwa2dfY29uZmlnWzpkZXNjcmlwdGlvbl0gPDwgIlxuIgogICAg
# ICAgICAgICBmaWxlIDw8ICJSZXF1aXJlczogIiA8PCBwa2dfY29uZmlnWzpy
# ZXF1aXJlc10uam9pbigiICIpIDw8ICJcbiIgaWYgcGtnX2NvbmZpZ1s6cmVx
# dWlyZXNdLmNsYXNzID09IEFycmF5CiAgICAgICAgICAgIGZpbGUgPDwgIlZl
# cnNpb246ICIgPDwgcGtnX2NvbmZpZ1s6dmVyc2lvbl0gPDwgIlxuIgogICAg
# ICAgICAgICAKICAgICAgICAgICAgZmlsZSA8PCAiTGliczogIiA8PCBwa2df
# Y29uZmlnWzpsaWJzXSA8PCAiXG4iCiAgICAgICAgICAgIGZpbGUgPDwgIkNm
# bGFnczogIiA8PCBwa2dfY29uZmlnWzpjZmxhZ3NdIDw8ICJcbiIKICAgICAg
# ICAgIGVuZAogICAgICAgIH0KICAgICAgZW5kCiAgICBlbmQKICAgIAogICAg
# CiAgICBkZWYgbG9nKHR4dCkKICAgICAgRmlsZS5vcGVuKEBsb2dfZmlsZSwg
# ImEiKSBkbyB8ZnwKICAgICAgICBmIDw8IHR4dCA8PCAiXG4iCiAgICAgIGVu
# ZAogICAgZW5kCiAgICAKICAgIGRlZiB1c2FnZQogICAgICBwdXRzICJPcHRp
# b25zOiIKICAgICAgQG9wdGlvbnMuZWFjaCB7IHxvcHRpb258CiAgICAgICAg
# Y2FzZSBvcHRpb25bOnR5cGVdCiAgICAgICAgICB3aGVuICJzdHJpbmciCiAg
# ICAgICAgICAgIHByaW50ICJcbiAgLS0je29wdGlvbls6bmFtZV19PVt2YWx1
# ZV0iCiAgICAgICAgICAgIHByaW50ICJcdFx0Li4uLi4uI3tvcHRpb25bOmRl
# c2NyaXB0aW9uXX0gIgogICAgICAgICAgICAKICAgICAgICAgICAgaWYgbm90
# IG9wdGlvbls6ZGVmYXVsdF0ubmlsPwogICAgICAgICAgICAgIHByaW50ICJb
# ZGVmYXVsdD0je29wdGlvbls6ZGVmYXVsdF19XSIKICAgICAgICAgICAgZW5k
# CiAgICAgICAgICB3aGVuICJwYXRoIgogICAgICAgICAgICBwcmludCAiXG4g
# IC0tI3tvcHRpb25bOm5hbWVdfT1bL2EvcGF0aF0iCiAgICAgICAgICAgIHBy
# aW50ICJcdFx0Li4uLi4uI3tvcHRpb25bOmRlc2NyaXB0aW9uXX0gIgogICAg
# ICAgICAgICAKICAgICAgICAgICAgaWYgbm90IG9wdGlvbls6ZGVmYXVsdF0u
# bmlsPwogICAgICAgICAgICAgIHByaW50ICJbZGVmYXVsdD0je29wdGlvbls6
# ZGVmYXVsdF19XSIKICAgICAgICAgICAgZW5kCiAgICAgICAgICB3aGVuICJi
# b29sIgogICAgICAgICAgICBwcmludCAiXG4gIC0tdXNlLSN7b3B0aW9uWzpu
# YW1lXX09W3llc3xub10iCiAgICAgICAgICAgIGlmIG5vdCBvcHRpb25bOmRl
# ZmF1bHRdLm5pbD8KICAgICAgICAgICAgICBkZWZhdWx0ID0gb3B0aW9uWzpk
# ZWZhdWx0XQogICAgICAgICAgICAgIGlmIGRlZmF1bHQgPT0gdHJ1ZSBvciBk
# ZWZhdWx0ID09ICJ5ZXMiIG9yIGRlZmF1bHQgPT0gInRydWUiCiAgICAgICAg
# ICAgICAgICBkZWZhdWx0ID0gInllcyIKICAgICAgICAgICAgICBlbHNpZiBk
# ZWZhdWx0ID09IGZhbHNlIG9yIGRlZmF1bHQgPT0gIm5vIiBvciBkZWZhdWx0
# ID09ICJmYWxzZSIKICAgICAgICAgICAgICAgIGRlZmF1bHQgPSAibm8iCiAg
# ICAgICAgICAgICAgZWxzZQogICAgICAgICAgICAgICAgZGVmYXVsdCA9ICJu
# byIKICAgICAgICAgICAgICBlbmQKICAgICAgICAgICAgICAKICAgICAgICAg
# ICAgICBwcmludCAiXHRcdC4uLi4uLiN7b3B0aW9uWzpkZXNjcmlwdGlvbl19
# IFtkZWZhdWx0PSN7ZGVmYXVsdH1dIgogICAgICAgICAgICBlbmQKICAgICAg
# ICAgIGVsc2UKICAgICAgICAgICAgcHJpbnQgIlxuICAtLSN7b3B0aW9uWzpu
# YW1lXX0iCiAgICAgICAgICAgIHByaW50ICJcdFx0Li4uLi4uI3tvcHRpb25b
# OmRlc2NyaXB0aW9uXX0iCiAgICAgICAgZW5kCiAgICAgIH0KICAgICAgCiAg
# ICAgIHByaW50ICJcbiAgLS13aXRoLWNvbmZpZy1mcm9udGVuZD1bcGxhaW58
# dGtdIgogICAgICBwcmludCAiXHRcdC4uLi4uLlNldHMgdGhlIGZyb250ZW5k
# IHRvIGNvbmZpZ3VyZSB0aGUgYXBwbGljYXRpb24gW2RlZmF1bHQ9dGtdIgog
# ICAgICBwcmludCAiXG4gIC0taGVscCIKICAgICAgcHJpbnQgIlx0XHQuLi4u
# Li5TaG93cyB0aGlzIG1lc3NhZ2UiCiAgICAgIAogICAgICBwdXRzICIiCiAg
# ICBlbmQKICBlbmQKZW5kCgpiZWdpbgogIGxvYWQgInNldHVwLnJiIgpyZXNj
# dWUgTG9hZEVycm9yCiAgRmlsZS5vcGVuKG9sZGxvY2F0aW9uKCJzZXR1cC5y
# YiIpLCAidyIpIGRvIHxmaWxlfAogICAgZmlsZSA8PCBGaWxlLnJlYWQoInNl
# dHVwX3JiLnRwbCIpIDw8ICJcbiIKICBlbmQKICBwdXRzICI9PiBBIHRlbXBs
# YXRlIGZpbGUgd2FzIGdlbmVyYXRlZCBpbiBzZXR1cC5yYiwgcGxlYXNlIGVk
# aXQgaXQgYW5kIHJlcnVuIHRoZSBjb25maWd1cmUgc2NyaXB0LiIKICBleGl0
# IC0xCmVuZAoKaWYgX19GSUxFX18gPT0gJDAKICBxb25mID0gUW9uZjo6Q29u
# ZmlndXJlLm5ldwogIHFvbmYuZ2V0X2FyZ3MoQVJHVikKICBxb25mLmZpbmRf
# cGFja2FnZXMKICBxb25mLnJ1bl90ZXN0cwogIAogIHFvbmYuc2F2ZSgiY29u
# ZmlnLnByaSIpCiAgCiAgcW9uZi5nZW5lcmF0ZV9tYWtlZmlsZXMKZW5kCgoK
# CgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAA=
