
module Kernel
	def readline
		$stdout.flush
		$stderr.flush
		Kernel.readline
	end

	def puts(*args)
		Kernel.puts args
		$stdout.flush
		$stderr.flush
	end
end

$stdindup = $stdin.dup

module HackStdIn
	def gets(sep = $/)
		$stdout.flush
		$stderr.flush
		$stdindup.gets
	end
end

class IO
	def <<(obj)
		self.write(obj.to_s)
		
		$stdout.flush
		$stderr.flush
		
		self
	end
end


$stdin.extend HackStdIn


