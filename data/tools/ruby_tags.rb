
require 'rbconfig'

gemspath = ""

begin
  require 'rubygems'
  gemspath = Gem.dir
ensure
end


outputfile = ARGV.first

ARGV.clear
ARGV << "-R"

ARGV << Config::CONFIG["rubylibdir"] << Config::CONFIG["sitelibdir"] << gemspath << "-f" << outputfile


require 'rtags/rtags'



