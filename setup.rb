
def configure(setup)
  setup.qt >= 4.2
  setup.add_option(:name => "dlib-dir", :type => "path", :optional => true, :description => "Sets the dlib dir")
  setup.find_package(:name => "dlib-core", :optional => false, :global => true)
  setup.find_package(:name => "dlib-gui", :optional => false, :global => true)
  setup.find_package(:name => "dlib-editor", :optional => false, :global => true)
  setup.find_package(:name => "dlib-ideality", :optional => false, :global => true)
end

def setup_pkgconfig(pkgconfig, args)
  case pkgconfig.id
    when /dlib.*/
      pkgconfig.add_search_path(File.expand_path(args["dlib-dir"])+"/lib/pkgconfig")
  end
end

def setup_config(cfg, args)
	cfg.add_qtmodule("xml")
end

