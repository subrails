/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "createfiledialog.h"

#include <QVBoxLayout>
#include <QGridLayout>
#include <QGroupBox>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QLabel>
#include <QFileDialog>
#include <QPushButton>

#include <dgui/separator.h>
#include <dgui/osd.h>

namespace View {

CreateFileDialog::CreateFileDialog(const QString &baseDir, QWidget *parent)
 : QDialog(parent), m_baseDir(baseDir)
{
	setWindowTitle(tr("Create a new file..."));
	
	QVBoxLayout *layout = new QVBoxLayout(this);
	
	{
		QGroupBox *group = new QGroupBox;
		QGridLayout *boxLayout = new QGridLayout(group);
		
		boxLayout->addWidget(new QLabel(tr("File name")), 0, 0);
		m_fileName = new QLineEdit;
		
		m_fileName->setText("unnamed");
		
		boxLayout->addWidget(m_fileName, 0, 1, 1, 2);
		
		boxLayout->addWidget(new QLabel(tr("Location")), 1, 0);
		m_location = new QLineEdit;
		boxLayout->addWidget(m_location, 1, 1);
		
		m_location->setText(baseDir);
		
		QPushButton *choose = new QPushButton(tr("choose"));
		connect(choose, SIGNAL(clicked()), this, SLOT(chooseLocation()));
		
		boxLayout->addWidget(choose, 1, 2);
		
		layout->addWidget(group);
	}
	
	layout->addWidget(new DGui::Separator());
	QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	layout->addWidget(buttonBox);
	
	connect(buttonBox, SIGNAL(accepted()), this, SLOT(createFile()));
	connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}


CreateFileDialog::~CreateFileDialog()
{
}

void CreateFileDialog::setSuffix(const QString &suffix)
{
	m_suffix = suffix;
	
	QString text = m_fileName->text();
	
	
	m_fileName->setText( text + "." + suffix);
	m_fileName->setSelection(0, text.length());
}

QString CreateFileDialog::filePath() const
{
	return m_location->text()+"/"+m_fileName->text();
}

void CreateFileDialog::createFile()
{
	QString filePath = this->filePath();
	if( QFile::exists(filePath) )
	{
		DGui::Osd::self()->display(tr("File exists"));
		return;
	}
	
	QFile f(filePath);
	
	if( f.open(QIODevice::WriteOnly) )
	{
		f.close();
		accept();
	}
}

void CreateFileDialog::chooseLocation()
{
	QString location = QFileDialog::getExistingDirectory(this, m_baseDir);
	
	if( !location.isEmpty() )
	{
		m_location->setText(location);
	}
}

}

