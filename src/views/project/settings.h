/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef VIEW_PROJECTSETTINGS_H
#define VIEW_PROJECTSETTINGS_H

#include <dgui/configurationdialog.h>

namespace Ui {
	class GrammarPage;
}

namespace View {

class Page;

/**
	@author David Cuadrado <krawek@gmail.com>
*/
class ProjectSettings : public DGui::ConfigurationDialog
{
	Q_OBJECT;
	
	public:
		ProjectSettings(QWidget *parent = 0);
		~ProjectSettings();
		
		void setProjectDir(const QString &dir);
		
		void apply();
		void ok();
		
	private:
		void openSRConfig();
		void closeSRConfig();
		void verifyConfig();
		void addToConfig(const QString &require);
		
		void saveGrammarConfig();
		void loadGrammarConfig();
		
	private slots:
		void addPluralPattern();
		void removePluralPattern();
		void addSingularPattern();
		void removeSingularPattern();
		void addUncountable();
		void removeUncountable();
		void addIrregular();
		void removeIrregular();
		
	private:
		void setupGrammarPage();
		
	private:
		Ui::GrammarPage *grammar;
		QString m_projectDir;
};


}

#endif
