/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef VIEWPROJECT_H
#define VIEWPROJECT_H

#include <QWidget>
#include <QDir>
#include <QHash>

#include <common/viewbase.h>

class QTreeWidget;
class QTreeWidgetItem;
class QFileSystemWatcher;
class QMenu;

namespace DGui {
class TreeWidgetFilterLine;
}

namespace View {

class Tree;

/**
	@author David Cuadrado <krawek@gmail.com>
*/
class Project : public Common::ViewBase
{
	Q_OBJECT
	public:
		enum {
			IsFile = 3764,
			FileName,
			IsController,
			IsModel,
			IsView,
			IsHelper,
			IsCategory,
			ControllerName,
			ModelName,
			Extension
		};
		
		enum {
			GenerateController = 0x01,
			DestroyController,
			GenerateModel,
			DestroyModel,
			GenerateScaffold,
			DestroyScaffold,
			GenerateCustom,
			DestroyCustom
		};
		
		Project(QWidget *parent = 0);
		~Project();
		
		void setProjectDir(const QString &dir);
		
		void perspectiveChanged(int wps);
		
		QStringList models() const;
		QStringList controllers() const;
		
		QMenu *actionsMenu();
		
	public slots:
		void refresh();
		
	private slots:
		void openFileFromItem(QTreeWidgetItem *item, int);
		void showMenu(const QPoint &pos);
		void launchCurrentItem();
		
		void executeAction(QAction *act);
		
	signals:
		void generateController();
		void destroyController(const QString &name);
		void generateModel();
		void destroyModel(const QString &name);
		void generateScaffold();
		void generateCustom();
		void destroyCustom();
		
		void modelsChanged(const QStringList &klasses);
		
	private:
		void loadAppFiles(const QDir &appDir);
		void loadControllers(const QString &dir);
		void loadViews(const QString &dir);
		void loadModels(const QString &dir);
		void loadHelpers(const QString &dir);
		
		void findControllers(const QDir &dir, QTreeWidgetItem *parent);
		void findHelpers(const QDir &dir, QTreeWidgetItem *parent);
		void findViews(const QDir &dir, QTreeWidgetItem *parent);
		
		QTreeWidgetItem *createControllerItem(const QString &filePath);
		QTreeWidgetItem *createHelperItem(const QString &filePath);
		QTreeWidgetItem *createModelItem(const QString &filePath);
		
		void watch(const QString &directory, QTreeWidgetItem *item);
		
	private slots:
		void updateDirectory(const QString &dir);
		
	private:
		Tree *m_tree;
		DGui::TreeWidgetFilterLine *m_search;
		QFileSystemWatcher *m_fileWatcher;
		
		QString m_projectDir;
		
		QTreeWidgetItem *m_controllerItem;
		QTreeWidgetItem *m_modelItem;
		QTreeWidgetItem *m_viewItem;
		QTreeWidgetItem *m_helperItem;
		
		QHash<QString, QTreeWidgetItem *> m_watchedDirectories;
};

}

#endif

