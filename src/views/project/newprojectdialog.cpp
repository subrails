/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "newprojectdialog.h"

#include "ui_new_project.h"

#include <common/global.h>

#include <QFileDialog>
#include <QRegExpValidator>

#include <dgui/iconloader.h>

namespace View {

NewProjectDialog::NewProjectDialog(QWidget *parent)
 : QDialog(parent)
{
	ui = new Ui::NewProjectDialog;
	
	ui->setupUi(this);
	
	ui->adapter->setCurrentIndex(0);
	
	
	ui->projectName->setValidator(new QRegExpValidator(QRegExp("\\w*"), ui->projectName));
	ui->projectLocation->setText( Common::workingDirectory() );
	
	ui->generatedIn->setText(projectLocation());
	
	ui->choose->setIcon(DGui::IconLoader::self()->load("folder.svg"));
	
	connect(ui->choose, SIGNAL(clicked()), this, SLOT(chooseLocation()));
	
	connect(ui->projectName, SIGNAL(textChanged(const QString &)), this, SLOT(updatePath(const QString &)));
	connect(ui->projectLocation, SIGNAL(textChanged(const QString &)), this, SLOT(updatePath(const QString &)));
	
	updatePath();
}


NewProjectDialog::~NewProjectDialog()
{
	delete ui;
}

QString NewProjectDialog::projectLocation() const
{
	return QDir::convertSeparators(ui->projectLocation->text()+"/"+ui->projectName->text());
}

QString NewProjectDialog::adapter() const
{
	return ui->adapter->currentText();
}

bool NewProjectDialog::isValid() const
{
	QFileInfo locationInfo(ui->projectLocation->text());
	
	bool ok = locationInfo.isWritable() && locationInfo.isDir() && locationInfo.isAbsolute() && !ui->projectName->text().isEmpty();
	
	return ok;
}

bool NewProjectDialog::freeze() const
{
	return ui->freeze->isChecked();
}

void NewProjectDialog::chooseLocation()
{
	QString dir = QFileDialog::getExistingDirectory(this);
	
	ui->projectLocation->setText(dir);
}

void NewProjectDialog::updatePath(const QString &)
{
	ui->generatedIn->setText(projectLocation());
	
	QPalette pal = ui->generatedIn->palette();
	
	if( isValid() )
	{
		ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
		pal.setBrush(QPalette::Background, Qt::green);
	}
	else
	{
		ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
		pal.setBrush(QPalette::Background, Qt::red);
	}
	
	ui->generatedIn->setPalette(pal);
}

}


