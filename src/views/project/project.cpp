/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "project.h"

#include <QPushButton>
#include <QTreeWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QToolButton>
#include <QHeaderView>
#include <QTimer>
#include <QTextStream>
#include <QMenu>
#include <QItemDelegate>
#include <QPainter>
#include <QFileIconProvider>
#include <QFileSystemWatcher>

#include <dgui/treewidgetfilterline.h>
#include <dgui/iconloader.h>
#include <dcore/debug.h>

#include <common/global.h>

#include "createfiledialog.h"

namespace View {

class Tree : public QTreeWidget
{
	friend class Delegator;
};

class Delegator: public QItemDelegate
{
	public:
		Delegator(Tree *tree, QWidget *parent );
		~Delegator();
		virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
		virtual QSize sizeHint(const QStyleOptionViewItem &opt, const QModelIndex &index) const;
		
	private:
		Tree *m_tree;
		QFileIconProvider m_iconProvider;
};

Delegator::Delegator(Tree *tree, QWidget *parent)
	: QItemDelegate(parent), m_tree(tree)
{
	m_tree->setIndentation(-50);
}

Delegator::~Delegator()
{
}

void Delegator::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	const QAbstractItemModel *model = index.model();
	QString text = model->data(index, Qt::DisplayRole).toString();
	QRect r = option.rect;
	r.setX( 0 );
	
	if(index.isValid() )
	{
		QTreeWidgetItem *item = m_tree->itemFromIndex(index);
		
		int level = 1;
		
		QTreeWidgetItem *parent = item->parent();
		if( parent )
			while( (parent = parent->parent()) )
				level++;
		
		if ( !item->data(0, Project::IsFile).toBool() ) // Paint directories
		{
			QPalette palette = option.palette;
			
			if (!model->parent(index).isValid())
			{
				
			}
			else
			{
				QColor color = palette.highlight().color();
				color.setAlpha(20);
				palette.setBrush(QPalette::Button, color);
				
				if ( ! item->data(0, Project::IsFile).toBool() )
				{
					r.setX(10*level);
				}
				else
				{
					r.setX(20*level);
				}
			}
				
			painter->save();
			
			QStyleOptionButton buttonOption;
	
			buttonOption.state = option.state;
			buttonOption.state &= ~QStyle::State_HasFocus;
			
			buttonOption.rect = r;
			buttonOption.palette = palette;
			buttonOption.features = QStyleOptionButton::None;
			
			if(m_tree->selectedItems().contains(item) )
			{
				buttonOption.state |= QStyle::State_Sunken;
			}
			
			m_tree->style()->drawControl(QStyle::CE_PushButton, &buttonOption, painter, m_tree);
			
			if( item->parent() )
			{
				m_iconProvider.icon(QFileIconProvider::Folder).paint(painter, QRect(15*level , r.y()+2, r.height()-4, r.height()-4 ));
			}
			
			painter->restore();
			
			// draw text
			
			QRect textRect = QRect(r.left() + 20, r.top(), r.width() - (option.fontMetrics.width(text) /2), r.height());
			
			QString textToDraw = elidedText(option.fontMetrics, textRect.width(), Qt::ElideMiddle, text );
			
			m_tree->style()->drawItemText(painter, textRect, Qt::AlignCenter, option.palette, m_tree->isEnabled(), textToDraw);
		}
		else // Paint files
		{
			QStyleOptionViewItem newOption = option;
			
			QModelIndex i = model->parent(index);
			if ( i.isValid() )
			{
				QTreeWidgetItem *item = m_tree->itemFromIndex(index);
				
				int level = 1;
		
				QTreeWidgetItem *parent = item->parent();
				if( parent )
					while( (parent = parent->parent()) )
						level++;
				
				if ( ! item->parent()->data(0, Project::IsFile).toBool() )
				{
					r.setX(10*level);
				}
				else
				{
					r.setX(20*level);
				}
			}
			newOption.rect = r;
			QItemDelegate::paint(painter, newOption, index);
		}
	}
}


QSize Delegator::sizeHint(const QStyleOptionViewItem &opt, const QModelIndex &index) const
{
	QStyleOptionViewItem option = opt;
	QSize sz = QItemDelegate::sizeHint(opt, index) + QSize(2, 2);
	return sz;
}



Project::Project(QWidget *parent)
	: Common::ViewBase(parent)
{
	setWindowTitle(tr("Project"));
	setWindowIcon(DGui::IconLoader::self()->load("folder-saved-search.svg"));
	
	QVBoxLayout *layout = new QVBoxLayout(this);
	layout->setMargin(2);
	layout->setSpacing(1);
	
	QHBoxLayout *search = new QHBoxLayout;
	search->setSpacing(2);

	QToolButton *button = new QToolButton;
	button->setText(tr("clear"));
	button->setIcon(DGui::IconLoader::self()->load("edit-clear.svg"));
	
	search->addWidget(button);
	m_tree = new Tree;
	
	m_tree->setItemDelegate(new Delegator(m_tree, this));
	m_tree->setAnimated(true);
	m_tree->setHeaderLabels(QStringList() << "" );
	
	m_tree->setSortingEnabled(true);
// 	m_tree->setDragEnabled(true);
	
	{
		m_controllerItem = new QTreeWidgetItem(m_tree);
		m_controllerItem->setData(0, IsController, true);
		m_controllerItem->setData(0, IsFile, false);
		m_controllerItem->setData(0, Extension, "rb");
		
		m_controllerItem->setText(0, tr("Controllers") );
		m_controllerItem->setFlags(0);
		
		m_viewItem = new QTreeWidgetItem(m_tree);
		m_viewItem->setData(0, IsFile, false);
		m_viewItem->setData(0, IsView, true);
		m_viewItem->setText(0, tr("Views") );
		m_viewItem->setData(0, Extension, "rhtml");
		m_viewItem->setFlags(0);
		
		m_modelItem = new QTreeWidgetItem(m_tree);
		m_modelItem->setText(0, tr("Models") );
		m_modelItem->setData(0, IsModel, true );
		m_modelItem->setData(0, IsFile, false);
		m_modelItem->setData(0, Extension, "rb");
		m_modelItem->setFlags(0);
		
		m_helperItem = new QTreeWidgetItem(m_tree);
		m_helperItem->setText(0, tr("Helpers") );
		m_helperItem->setData(0, IsHelper, true);
		m_helperItem->setData(0, IsFile, false);
		m_helperItem->setData(0, Extension, "rb");
		m_helperItem->setFlags(0);
	}
		
	m_tree->setContextMenuPolicy(Qt::CustomContextMenu);
	
	connect(m_tree, SIGNAL(customContextMenuRequested( const QPoint& )), this, SLOT(showMenu(const QPoint &)));
	connect(m_tree, SIGNAL(itemActivated( QTreeWidgetItem*, int )), this, SLOT(openFileFromItem(QTreeWidgetItem*, int)));
	
	m_tree->header()->hide();
	
	
	m_search = new DGui::TreeWidgetFilterLine(this);
	m_search->setTreeWidget(m_tree);
	
	search->addWidget( m_search );
	connect(button, SIGNAL(clicked()), m_search, SLOT(clear()));
	
	layout->addLayout(search);
	layout->addWidget(m_tree);
	
	m_fileWatcher = new QFileSystemWatcher(this);
	connect(m_fileWatcher, SIGNAL(directoryChanged(const QString &)), this, SLOT(updateDirectory(const QString &)));
	
	m_tree->setItemsExpandable(false);
}


Project::~Project()
{
}


void Project::setProjectDir(const QString &dir)
{
	m_projectDir = dir;
	
	if( !dir.isEmpty() )
	{
		QTimer::singleShot(100, this, SLOT(refresh()));
	}
	else
	{
		emit modelsChanged(QStringList());
		delete m_fileWatcher;
		m_fileWatcher = new QFileSystemWatcher(this);
		
		m_modelItem->takeChildren();
		m_viewItem->takeChildren();
		m_controllerItem->takeChildren();
		m_helperItem->takeChildren();
		
		m_tree->reset();
	}
}

void Project::perspectiveChanged(int wps)
{
	setUpdatesEnabled(false);
	switch(wps)
	{
		case Common::All:
		{
			for(int i = 0; i < m_tree->topLevelItemCount(); i++)
			{
				QTreeWidgetItem *item = m_tree->topLevelItem(i);
				if( item->isHidden() )
				{
					item->setHidden(false);
				}
			}
		}
		break;
		case Common::Model:
		{
			for(int i = 0; i < m_tree->topLevelItemCount(); i++)
			{
				QTreeWidgetItem *item = m_tree->topLevelItem(i);
				if( item->data(0, IsModel).toBool() )
				{
					item->setHidden(false);
				}
				else
				{
					item->setHidden(true);
				}
			}
		}
		break;
		case Common::Controller:
		{
			for(int i = 0; i < m_tree->topLevelItemCount(); i++)
			{
				QTreeWidgetItem *item = m_tree->topLevelItem(i);
				if( item->data(0, IsController).toBool() || item->data(0, IsHelper).toBool() )
				{
					item->setHidden(false);
				}
				else
				{
					item->setHidden(true);
				}
			}
		}
		break;
		case Common::View:
		{
			for(int i = 0; i < m_tree->topLevelItemCount(); i++)
			{
				QTreeWidgetItem *item = m_tree->topLevelItem(i);
				if( item->data(0, IsView).toBool() || item->data(0, IsHelper).toBool() )
				{
					item->setHidden(false);
				}
				else
				{
					item->setHidden(true);
				}
			}
		}
		break;
	}
	
	m_tree->expandAll();
	setUpdatesEnabled(true);
}

QStringList Project::models() const
{
	QStringList models;
	
	for(int i = 0; i < m_modelItem->childCount(); i++)
	{
		models << m_modelItem->child(i)->text(0);
	}
	
	return models;
}

QStringList Project::controllers() const
{
	QStringList controllers;
	
	for(int i = 0; i < m_controllerItem->childCount(); i++)
	{
		controllers << m_controllerItem->child(i)->text(0);
	}
	
	return controllers;
}

QMenu *Project::actionsMenu()
{
	QTreeWidgetItem *curr = m_tree->currentItem();
	
	QMenu *menu = new QMenu(tr("Actions"));
	
	menu->addAction(tr("Generate controller..."))->setData(GenerateController);
	menu->addAction(tr("Generate model..."))->setData(GenerateModel);
	menu->addAction(tr("Generate scaffold..."))->setData(GenerateScaffold);
	menu->addAction(tr("Generate custom..."))->setData(GenerateCustom);
	menu->addSeparator();
	
	if( curr )
	{
		if( curr->data(0, IsController).toBool() )
		{
			QAction *launch = new QAction(menu);
			launch->setText(tr("Launch in browser..."));
			connect(launch, SIGNAL(triggered()), this, SLOT(launchCurrentItem()));
			
			QAction *before = menu->actions()[0];
			menu->insertAction( before, launch );
			menu->insertSeparator(before);
			
			if( curr->data(0, IsFile).toBool() )
			{
				QString cname = curr->data(0, ControllerName).toString();
				if( !cname.isEmpty() )
				{
					menu->addAction(tr("Destroy controller: ")+cname)->setData(DestroyController);
				}
			}
		}
		else if( curr->data(0, IsModel).toBool() )
		{
			if( curr->data(0, IsFile).toBool() )
			{
				QString cname = curr->data(0, ModelName).toString();
				if( !cname.isEmpty())
				{
					menu->addAction(tr("Destroy model: ")+cname)->setData(DestroyModel);
				}
			}
		}
	}
	
	menu->addAction(tr("Destroy custom..."))->setData(DestroyCustom);
	
	
	connect(menu, SIGNAL(triggered( QAction* )), this, SLOT(executeAction(QAction *)));
	
	return menu;
}

void Project::refresh()
{
	QDir dir(m_projectDir);
	
	if( dir.exists() )
	{
		dir.cd("app");
		loadAppFiles(dir);
		dir.cdUp();
	}
}

void Project::openFileFromItem(QTreeWidgetItem *item, int)
{
	if( item )
	{
		QString fileName;
		
		if( !item->data(0, IsFile).toBool() ) // Directory
		{
			QString dir = m_watchedDirectories.key(item);
			
			if( dir.isEmpty() && item->data(0, IsCategory).toBool() )
			{
				dir = m_watchedDirectories.key(item->parent());
			}
			
			CreateFileDialog createFileDialog( dir, this);
			
			createFileDialog.setSuffix( item->data(0, Extension).toString() );
			
			if ( createFileDialog.exec() != QDialog::Accepted ) return;
			
			fileName = createFileDialog.filePath();
		}
		else
		{
			fileName = item->data(0, FileName).toString();
		}
		
		
		
		if( !fileName.isEmpty() )
		{
			int workspace = Common::All;
			
			if( item->data(0, IsController).toBool() )
			{
				workspace = Common::Controller;
			}
			else if( item->data(0, IsView).toBool() )
			{
				workspace = Common::View;
			} 
			else if( item->data(0, IsModel).toBool() )
			{
				workspace = Common::Model;
			}
			
			emit openFile(fileName, workspace);
		}
	}
}

void Project::showMenu(const QPoint &pos)
{
	QMenu *menu = actionsMenu();
	
	menu->exec( m_tree->mapToGlobal(pos) );
	
	delete menu;
}

void Project::launchCurrentItem()
{
	QTreeWidgetItem *current = m_tree->currentItem();
	if( current )
	{
		if( current->data(0, IsController).toBool() )
		{
			QString controller = current->data(0, ControllerName).toString().toLower();
			
			if( controller == "application" )
				controller = "";
			
			emit runApplication(controller);
		}
	}
}

void Project::executeAction(QAction *act)
{
	switch(act->data().toInt() )
	{
		case GenerateController:
		{
			emit generateController();
		}
		break;
		case DestroyController:
		{
			if( QTreeWidgetItem *current = m_tree->currentItem() )
			{
				emit destroyController(current->text(0));
			}
		}
		break;
		case GenerateModel:
		{
			emit generateModel();
		}
		break;
		case DestroyModel:
		{
			if( QTreeWidgetItem *current = m_tree->currentItem() )
			{
				emit destroyModel(current->text(0));
			}
		}
		break;
		case GenerateScaffold:
		{
			emit generateScaffold();
		}
		break;
		case GenerateCustom:
		{
			emit generateCustom();
		}
		break;
		case DestroyCustom:
		{
			emit destroyCustom();
		}
		break;
	}
}

void Project::loadAppFiles(const QDir &appDir)
{
	//////////////////////////
	
	QDir controllers = QDir(appDir.absolutePath()+"/controllers/");
	loadControllers(controllers.absolutePath());
	
	//////////////////////////
	QDir helpers = QDir(appDir.absolutePath()+"/helpers/");
	loadHelpers(helpers.absolutePath());
	
	//////////////////////////
	QDir models = QDir(appDir.absolutePath()+"/models/");
	loadModels(models.absolutePath());
	
	
	//////////////////////////
	QDir views = QDir(appDir.absolutePath()+"/views/");
	loadViews(views.absolutePath());
	
	m_tree->expandAll();
}

void Project::loadControllers(const QString &dir)
{
	QDir controllers(dir);
	m_controllerItem->takeChildren();
	watch( controllers.absolutePath(), m_controllerItem);
	
	m_controllerItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
	findControllers(controllers, m_controllerItem);
}

void Project::loadViews(const QString &dir)
{
	QDir views(dir);
	m_viewItem->takeChildren();
	watch( views.absolutePath(), m_viewItem);
	
	m_viewItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
	findViews(views, m_viewItem);
}

void Project::loadModels(const QString &dir)
{
	QDir modelsDir(dir);
	
	if( !modelsDir.exists())
	{
		modelsDir.mkpath(modelsDir.absolutePath());
	}
	
	m_modelItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
	
	m_modelItem->takeChildren();
	
	watch( modelsDir.absolutePath(), m_modelItem);
	
	foreach(QString fileName, modelsDir.entryList(QStringList() << "*.rb" ) )
	{
		QTreeWidgetItem *item = createModelItem(modelsDir.absoluteFilePath(fileName));
		m_modelItem->addChild(item);
	}
	
	emit modelsChanged(this->models());
}

void Project::loadHelpers(const QString &dir)
{
	QDir helpers(dir);
	
	m_helperItem->takeChildren();
	m_helperItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
	m_watchedDirectories[helpers.absolutePath()] = m_helperItem;
	findHelpers(helpers, m_helperItem);
}

void Project::updateDirectory(const QString &dirPath)
{
	QDir dir(dirPath);
	
	QTreeWidgetItem *item = m_watchedDirectories.value(dirPath);
	
	if( item )
	{
		setUpdatesEnabled(false);
		if( item->data(0, IsController).toBool() )
		{
			if( item->parent() )
			{
				item->takeChildren();
				findControllers(dir, item);
			}
			else
			{
				loadControllers(dirPath);
			}
		}
		else if( item->data(0, IsHelper).toBool() )
		{
			if( item->parent() )
			{
				item->takeChildren();
				findHelpers(dir, item);
			}
			else
			{
				loadHelpers(dirPath);
			}
		}
		else if( item->data(0, IsModel).toBool() )
		{
			loadModels(dir.absolutePath());
		}
		else if( item->data(0, IsView).toBool() )
		{
			if( item->parent() )
			{
				item->takeChildren();
				findViews(dir, item);
			}
			else
			{
				loadViews(dirPath);
			}
		}
		
		m_tree->expandAll();
		
		setUpdatesEnabled(true);
	}
}

void Project::findControllers(const QDir &dir, QTreeWidgetItem *parent)
{
	foreach(QFileInfo finfo, dir.entryInfoList() )
	{
		if( finfo.isDir() && !finfo.isHidden() )
		{
			QTreeWidgetItem *subdir = new QTreeWidgetItem(parent);
			
			watch( finfo.absoluteFilePath(), subdir);
			
			subdir->setData(0, IsFile, false);
			subdir->setData(0, IsController, true);
			subdir->setData(0, Extension, "rb");
			subdir->setText(0, finfo.fileName());
			
			findControllers(QDir(finfo.absoluteFilePath()), subdir);
		}
		else if ( finfo.fileName().endsWith("_controller.rb" ) || finfo.fileName() == "application.rb" )
		{
			QTreeWidgetItem *item = createControllerItem(finfo.absoluteFilePath());
			parent->addChild(item);
		}
	}
}

void Project::findHelpers(const QDir &dir, QTreeWidgetItem *parent)
{
	foreach(QFileInfo finfo, dir.entryInfoList() )
	{
		if( finfo.isDir() && !finfo.isHidden() )
		{
			QTreeWidgetItem *subdir = new QTreeWidgetItem(parent);
			watch( finfo.absoluteFilePath(), subdir);
			
			subdir->setData(0, IsFile, false);
			subdir->setData(0, IsHelper, true);
			subdir->setData(0, Extension, "rb");
			subdir->setText(0, finfo.fileName());
			
			findHelpers(QDir(finfo.absoluteFilePath()), subdir);
		}
		else if ( finfo.fileName().endsWith("_helper.rb" ) )
		{
			QTreeWidgetItem *item = createHelperItem(finfo.absoluteFilePath());
			parent->addChild(item);
		}
	}
}

void Project::findViews(const QDir &dir, QTreeWidgetItem *parent)
{
	QTreeWidgetItem *rhtml = new QTreeWidgetItem;
	rhtml->setData(0, IsFile, false);
	rhtml->setData(0, IsView, true);
	rhtml->setData(0, Extension, "rhtml");
	rhtml->setData(0, IsCategory, true);
	
	rhtml->setText(0, tr("rhtml") );
	
	QTreeWidgetItem *rjs = new QTreeWidgetItem;
	rjs->setData(0, IsFile, false);
	rjs->setData(0, IsView, true);
	rjs->setData(0, IsCategory, true);
	rjs->setData(0, Extension, "rjs");
	rjs->setText(0, tr("rjs") );
	
	QTreeWidgetItem *rxml = new QTreeWidgetItem;
	rxml->setData(0, IsFile, false);
	rxml->setData(0, IsView, true);
	rxml->setData(0, IsCategory, true);
	rxml->setData(0, Extension, "rxml");
	rxml->setText(0, tr("rxml") );
			
	foreach(QFileInfo finfo, dir.entryInfoList() )
	{
		if( finfo.isDir() && !finfo.isHidden() )
		{
			QTreeWidgetItem *subdir = new QTreeWidgetItem(parent);
			subdir->setData(0, Extension, "rhtml");
			
			watch( finfo.absoluteFilePath(), subdir);
			
			subdir->setData(0, IsFile, false);
			subdir->setData(0, IsView, true);
			
			subdir->setText(0, finfo.fileName());
			
			findViews(QDir(finfo.absoluteFilePath()), subdir);
		}
		else
		{
			QTreeWidgetItem *item = new QTreeWidgetItem;
			
			item->setText(0, finfo.fileName());
			
			item->setData(0, IsFile, true);
			item->setData(0, IsView, true);
			item->setData(0, FileName, finfo.absoluteFilePath());
			
			if ( finfo.fileName().toLower().endsWith(".rhtml" ) )
			{
				rhtml->addChild(item);
			}
			else if( finfo.fileName().toLower().endsWith(".rjs" ) )
			{
				rjs->addChild(item);
			}
			else if( finfo.fileName().toLower().endsWith(".rxml" ) )
			{
				rxml->addChild(item);
			}
		}
	}
	
	if( rhtml->childCount() > 0 )
	{
		parent->addChild(rhtml);
	}
	else
	{
		delete rhtml;
	}
	
	if( rxml->childCount() > 0 )
	{
		parent->addChild(rxml);
	}
	else
	{
		delete rxml;
	}
	
	if( rjs->childCount() > 0 )
	{
		parent->addChild(rjs);
	}
	else
	{
		delete rjs;
	}
}

QTreeWidgetItem *Project::createControllerItem(const QString &filePath)
{
	QTreeWidgetItem *item = new QTreeWidgetItem;
	item->setData(0, FileName, filePath );
	item->setData(0, IsController, true);
	item->setData(0, IsFile, true);
	
	QFile f(filePath);
	
	if( f.open(QIODevice::ReadOnly | QIODevice::Text ) )
	{
		QFileInfo finfo(f);
		
		QTextStream ts(&f);
		
		QRegExp rx("class\\s+(\\w+)");
		QRegExp rx2("(\\w+)Controller");
		
		QString cname = finfo.fileName();
		
		while(!ts.atEnd())
		{
			QString line = ts.readLine();
			if( rx.indexIn(line) > -1 )
			{
				cname = rx.cap(1);
				if( rx2.indexIn(cname) > -1 )
				{
					cname = rx2.cap(1);
					item->setData(0, ControllerName, cname);
				}
				
				break;
			}
		}
		
		item->setText(0, cname);
		
		f.close();
	}
	
	return item;
}

QTreeWidgetItem *Project::createHelperItem(const QString &filePath)
{
	QTreeWidgetItem *item = new QTreeWidgetItem;
	item->setData(0, FileName, filePath );
	item->setData(0, IsHelper, true);
	item->setData(0, IsFile, true);
	
	QFile f(filePath);
	
	if( f.open(QIODevice::ReadOnly | QIODevice::Text ) )
	{
		QFileInfo finfo(f);
		
		QTextStream ts(&f);
		
		QRegExp rx("module\\s+(\\w+::){0,1}(\\w+)Helper");
		
		QString cname = finfo.fileName();
		
		while(!ts.atEnd())
		{
			QString line = ts.readLine();
			
			if( rx.indexIn(line) > -1 )
			{
				cname = rx.cap(2);
				break;
			}
		}
		
		item->setText(0, cname);
		
		f.close();
	}
	
	return item;
}

QTreeWidgetItem *Project::createModelItem(const QString &filePath)
{
	QTreeWidgetItem *item = new QTreeWidgetItem;
	
	item->setData(0, FileName, filePath );
	item->setData(0, IsModel, true);
	item->setData(0, IsFile, true);
	
	QFile f(filePath);
	
	if( f.open(QIODevice::ReadOnly | QIODevice::Text ) )
	{
		QFileInfo finfo(f);
		QTextStream ts(&f);
		
		QRegExp rx("class\\s+(\\w+)");
		
		QString cname = finfo.fileName();
		
		while(!ts.atEnd())
		{
			QString line = ts.readLine();
			
			if( rx.indexIn(line) > -1 )
			{
				cname = rx.cap(1);
				item->setData(0, ModelName, cname);
				break;
			}
		}
		
		item->setText(0, cname);
		
		f.close();
	}
	
	
	return item;
}

void Project::watch(const QString &directory, QTreeWidgetItem *item)
{
	m_fileWatcher->addPath(directory);
	m_watchedDirectories[directory] = item;
}

}


