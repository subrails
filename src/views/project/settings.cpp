/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "settings.h"

#include <QHeaderView>
#include <QFile>
#include <QTextStream>

#include <dcore/applicationproperties.h>
#include <dcore/debug.h>

#include <dgui/iconloader.h>

#include "ui_settings_grammar.h"

namespace View {

class Page : public QWidget
{
	public:
		Page(QWidget *parent = 0) : QWidget(parent)
		{
		}
		~Page() {}
};

ProjectSettings::ProjectSettings(QWidget *parent)
	: DGui::ConfigurationDialog(parent)
{
	setupGrammarPage();
	
	resize( 820, 480);
}


ProjectSettings::~ProjectSettings()
{
}

void ProjectSettings::setProjectDir(const QString &dir)
{
	m_projectDir = dir;
	
	verifyConfig();
	
	openSRConfig();
	
	loadGrammarConfig();
	
	closeSRConfig();
}

void ProjectSettings::apply()
{
	// Current page
	if( this->currentPageTitle() == tr("Grammar"))
	{
		saveGrammarConfig();
	}
	else
	{
	}
	
	DGui::ConfigurationDialog::apply();
}

void ProjectSettings::ok()
{
	// All pages
	saveGrammarConfig();
	
	DGui::ConfigurationDialog::ok();
}

void ProjectSettings::openSRConfig()
{
	QFile srconfig(m_projectDir+"/config/sr_config.rb");
	if( srconfig.open(QIODevice::WriteOnly) )
	{
		QTextStream ts(&srconfig);
		
		ts << "# Generated automatically. Please do no edit" << endl;
	}
}

void ProjectSettings::closeSRConfig()
{
	QFile srconfig(m_projectDir+"/config/sr_config.rb");
	if( srconfig.open(QIODevice::Append) )
	{
		QTextStream ts(&srconfig);
		ts << "# EOF" << endl;
	}
}


void ProjectSettings::verifyConfig()
{
	QFile f( m_projectDir+"/config/environment.rb");
	
	QString require = "require \'sr_config\'";
	bool included = false;
	
	if( f.open(QIODevice::ReadOnly) )
	{
		QTextStream ts(&f);
		
		while( !ts.atEnd() )
		{
			QString line = ts.readLine();
			
			if( line.startsWith(require) )
			{
				included = true;
				break;
			}
		}
		
		f.reset();
		f.close();
	}
	
	if( !included )
	{
		if( f.open(QIODevice::Append ) )
		{
			QTextStream ts(&f);
			
			ts << require << endl;
		}
	}
}

void ProjectSettings::addToConfig(const QString &require)
{
	QFile srconfig(m_projectDir+"/config/sr_config.rb");
	if( srconfig.open(QIODevice::Append) )
	{
		QTextStream ts(&srconfig);
		ts << "begin" << endl;
		ts << "require \'" << require << "\'" << endl;
		ts << "ensure" << endl;
		ts << "end" << endl;
	}
}

void ProjectSettings::saveGrammarConfig()
{
	QFile f(m_projectDir+"/config/sr_grammar.rb");
		
	if( f.open(QIODevice::WriteOnly) )
	{
		QTextStream ts(&f);
		
		ts << "# Generated automatically. Please do no edit manually!" << endl;
		ts << "Inflector.inflections do |inflect|" << endl;
		
		for(int i = 0; i < grammar->plural_table->rowCount(); i++)
		{
			QString singularPattern = grammar->plural_table->item(i, 0)->text();
			QString pluralPattern = grammar->plural_table->item(i, 2)->text();
			
			int mode = qobject_cast<QComboBox *>(grammar->plural_table->cellWidget(i, 1))->currentIndex();
			
			QString pattern = "/";
			
			if( mode == 1 )
			{
				pattern += "^";
			}
			
			pattern += singularPattern;
			
			if( mode == 0 )
			{
				pattern += "$";
			}
			
			pattern += "/i";
			
			ts << "  inflect.plural(" << pattern << ", " << "\'"+pluralPattern+"\'" << " )" << endl;
		}
		
		for(int i = 0; i < grammar->singular_table->rowCount(); i++)
		{
			QString singularPattern = grammar->singular_table->item(i, 2)->text();
			QString pluralPattern = grammar->singular_table->item(i, 0)->text();
			
			int mode = qobject_cast<QComboBox *>(grammar->singular_table->cellWidget(i, 1))->currentIndex();
			
			QString pattern = "/";
			
			if( mode == 1 )
			{
				pattern += "^";
			}
			
			pattern += pluralPattern;
			
			if( mode == 0 )
			{
				pattern += "$";
			}
			
			pattern += "/i";
			
			ts << "  inflect.singular(" << pattern << ", " << "\'"+singularPattern+"\'" << " )" << endl;
		}
		
		for(int i = 0; i < grammar->irregularTable->rowCount(); i++)
		{
			QString normal = grammar->irregularTable->item(i, 0)->text();
			QString irregular = grammar->irregularTable->item(i, 1)->text();
			
			
			ts << "  inflect.irregular(" << "\'"+normal+"\'" <<  ", " << "\'"+irregular+"\'" << " )" << endl;
		}
		
		QStringList uncountable;
		
		for(int i = 0; i < grammar->uncountableList->count(); i++)
		{
			uncountable << grammar->uncountableList->item(i)->text();
		}
		
		if( ! uncountable.isEmpty())
		{
			ts << "  inflect.uncountable( %w(" << uncountable.join(" ") << "))" << endl;
		}
		
		ts << "end" << endl;
	}
}

void ProjectSettings::loadGrammarConfig()
{
	QString filePath = m_projectDir+"/config/sr_grammar.rb";
	if( QFile::exists(filePath) )
	{
		QFile f(filePath);
		if( f.open(QIODevice::ReadOnly) )
		{
			QRegExp plural("^inflect.plural\\(/(\\^){0,1}(.*)(\\$){0,1}/i,\\s\\'(.*)\\'\\s\\)");
			QRegExp singular("^inflect.singular\\(/(\\^){0,1}(.*)(\\$){0,1}/i,\\s\\'(.*)\\'\\s\\)");
			QRegExp irregular("^inflect.irregular\\(\\s*\\'(.*)\\',\\s*\\'(.*)\\'\\s\\)");
			QRegExp uncountable("^inflect.uncountable\\(\\s*%w\\((.*)\\)\\)");
			
			QTextStream ts(&f);
			
			while( !ts.atEnd() )
			{
				QString line = ts.readLine().simplified();
				
				if( plural.indexIn(line) > -1 )
				{
					QString pattern = plural.cap(2);
					grammar->plural_pluralPattern->setText(plural.cap(4));
					
					if( !plural.cap(3).isEmpty() )
					{
						grammar->plural_singularPatternMode->setCurrentIndex(0);
					}
					else if( pattern.endsWith("$") )
					{
						pattern.remove(pattern.length()-1, 1);
						grammar->plural_singularPatternMode->setCurrentIndex(0);
					}
					else if( !plural.cap(1).isEmpty() )
					{
						grammar->plural_singularPatternMode->setCurrentIndex(1);
					}
					
					grammar->plural_singularPattern->setText(pattern);
					
					addPluralPattern();
				}
				else if( singular.indexIn(line) > -1 )
				{
					grammar->singular_pluralPattern->setText(singular.cap(2));
					grammar->singular_singularPattern->setText(singular.cap(4));
					
					if( !singular.cap(3).isEmpty() )
					{
						grammar->singular_pluralPatternMode->setCurrentIndex(0);
					}
					else if( !singular.cap(1).isEmpty() )
					{
						grammar->singular_pluralPatternMode->setCurrentIndex(1);
					}
					
					
					addSingularPattern();
				}
				else if( irregular.indexIn(line) > -1 )
				{
					grammar->normal->setText(irregular.cap(1));
					grammar->irregular->setText(irregular.cap(2));
					addIrregular();
				}
				else if( uncountable.indexIn(line) > -1 )
				{
					foreach( QString text, uncountable.cap(1).split(" "))
					{
						grammar->uncountable->setText(text);
						addUncountable();
					}
				}
			}
		}
	}
	else
	{
		QFile f(filePath);
		f.open(QIODevice::WriteOnly);
	}
	
	addToConfig("sr_grammar");
}

void ProjectSettings::addPluralPattern()
{
	int row = grammar->plural_table->rowCount();
	
	grammar->plural_table->insertRow(row);
	
	QTableWidgetItem *singular = new QTableWidgetItem();
	singular->setText(grammar->plural_singularPattern->text());
	
	grammar->plural_table->setItem(row, 0, singular);
	
	QComboBox *combo = new QComboBox;
	for(int i = 0; i < grammar->plural_singularPatternMode->count(); i++)
	{
		combo->addItem(grammar->plural_singularPatternMode->itemText(i));
	}
	combo->setCurrentIndex(grammar->plural_singularPatternMode->currentIndex());
	
	grammar->plural_table->setCellWidget(row, 1, combo);
	
	QTableWidgetItem *plural = new QTableWidgetItem();
	plural->setText(grammar->plural_pluralPattern->text());
	
	grammar->plural_table->setItem(row, 2, plural);
	
	grammar->plural_singularPattern->setText(QString());
	grammar->plural_singularPatternMode->setCurrentIndex(0);
	grammar->plural_pluralPattern->setText(QString());
}

void ProjectSettings::removePluralPattern()
{
	grammar->plural_table->removeRow(grammar->plural_table->currentRow());
}

void ProjectSettings::addSingularPattern()
{
	int row = grammar->singular_table->rowCount();
	
	grammar->singular_table->insertRow(row);
	
	QTableWidgetItem *plural = new QTableWidgetItem();
	plural->setText(grammar->singular_pluralPattern->text());
	grammar->singular_table->setItem(row, 0, plural);
	
	
	QComboBox *combo = new QComboBox;
	for(int i = 0; i < grammar->singular_pluralPatternMode->count(); i++)
	{
		combo->addItem(grammar->singular_pluralPatternMode->itemText(i));
	}
	combo->setCurrentIndex(grammar->singular_pluralPatternMode->currentIndex());
	grammar->singular_table->setCellWidget(row, 1, combo);
	
	
	QTableWidgetItem *singular = new QTableWidgetItem();
	singular->setText(grammar->singular_singularPattern->text());
	grammar->singular_table->setItem(row, 2, singular);
	
	grammar->singular_singularPattern->setText(QString());
	grammar->singular_pluralPatternMode->setCurrentIndex(0);
	grammar->singular_pluralPattern->setText(QString());
}

void ProjectSettings::removeSingularPattern()
{
	grammar->singular_table->removeRow(grammar->singular_table->currentRow());
}

void ProjectSettings::addUncountable()
{
	grammar->uncountableList->addItem(grammar->uncountable->text());
	grammar->uncountable->setText(QString());
}

void ProjectSettings::removeUncountable()
{
	QListWidgetItem *item = grammar->uncountableList->takeItem(grammar->uncountableList->currentRow());
	
	if( item ) delete item;
}

void ProjectSettings::addIrregular()
{
	int row = grammar->irregularTable->rowCount();
	
	grammar->irregularTable->insertRow(row);
	
	QTableWidgetItem *normal = new QTableWidgetItem();
	normal->setText(grammar->normal->text());
	
	grammar->irregularTable->setItem(row, 0, normal);
	
	
	QTableWidgetItem *irregular = new QTableWidgetItem();
	irregular->setText(grammar->irregular->text());
	
	grammar->irregularTable->setItem(row, 1, irregular);
	
	grammar->normal->setText(QString());
	grammar->irregular->setText(QString());
}

void ProjectSettings::removeIrregular()
{
	grammar->irregularTable->removeRow(grammar->irregularTable->currentRow());
}


void ProjectSettings::setupGrammarPage()
{
	Page *grammarPage = new Page;
	
	grammar = new Ui::GrammarPage();
	grammar->setupUi(grammarPage);
	
	grammar->plural_table->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
	grammar->plural_table->setHorizontalHeaderLabels(QStringList() << tr("Singular") << tr("Mode") << tr("Plural"));
	connect(grammar->plural_add, SIGNAL(clicked()), this, SLOT(addPluralPattern()));
	connect(grammar->plural_remove, SIGNAL(clicked()), this, SLOT(removePluralPattern()));
	
	grammar->singular_table->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
	grammar->singular_table->setHorizontalHeaderLabels(QStringList() << tr("Plural") << tr("Mode") << tr("Singular"));
	connect(grammar->singular_add, SIGNAL(clicked()), this, SLOT(addSingularPattern()));
	connect(grammar->singular_remove, SIGNAL(clicked()), this, SLOT(removeSingularPattern()));
	
	
	connect(grammar->addUncountable, SIGNAL(clicked()), this, SLOT(addUncountable()));
	connect(grammar->removeUncountable, SIGNAL(clicked()), this, SLOT(removeUncountable()));
	
	grammar->irregularTable->horizontalHeader()->hide();
	grammar->irregularTable->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
	grammar->irregularTable->setHorizontalHeaderLabels(QStringList() << tr("") << tr(""));
	connect(grammar->addIrregular, SIGNAL(clicked()), this, SLOT(addIrregular()));
	connect(grammar->removeIrregular, SIGNAL(clicked()), this, SLOT(removeIrregular()));
	
	addPage(grammarPage, tr("Grammar"), DGui::IconLoader::self()->load("applications-office.svg"));
}

}
