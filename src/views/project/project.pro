TEMPLATE = lib

CONFIG += staticlib \
warn_on
HEADERS += project.h \
createfiledialog.h \
newprojectdialog.h \
settings.h
SOURCES += project.cpp \
createfiledialog.cpp \
newprojectdialog.cpp \
settings.cpp
FORMS += ui/new_project.ui ui/settings_grammar.ui
include(../views_config.pri)

