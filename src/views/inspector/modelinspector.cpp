/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "modelinspector.h"

#include <QVBoxLayout>
#include <QListWidget>
#include <QTreeWidget>
#include <QSplitter>
#include <QHeaderView>
#include <QPainter>
#include <QTimeLine>

#include <common/modelxmlparser.h>

#include <dgui/treewidgetfilterline.h>
#include <dgui/indicatordrawer.h>
#include <dcore/debug.h>

namespace View {

class ModelList : public QListWidget
{
	Q_OBJECT;
	
	public:
		ModelList() : QListWidget(), m_waiting(false)
		{
			m_indicator = new DGui::IndicatorDrawer(this);
			connect(&m_timeline, SIGNAL(frameChanged( int )), this, SLOT(onFrameChanged(int)));
			
			int fps = 14;
			m_timeline.setDuration((1000/fps)*8);
			m_timeline.setLoopCount(0);
			m_timeline.setFrameRange(0, 8);
		}
		
		~ModelList() {}
		
		void setWaiting(bool e)
		{
			m_waiting = e;
			if( m_waiting )
			{
				if( m_timeline.state() != QTimeLine::Running )
					m_timeline.start();
			}
			else
			{
				m_timeline.stop();
			}
			
			viewport()->update();
		}
		
	private slots:
		void onFrameChanged(int frame)
		{
			m_indicator->setFrame(frame);
			viewport()->update();
		}
		
	protected:
		void paintEvent(QPaintEvent *e)
		{
			QListWidget::paintEvent(e);
			
			if( m_waiting)
			{
				QPainter painter(viewport());
				painter.setRenderHint(QPainter::Antialiasing, true);
				
				painter.translate(rect().center().x()-32, rect().center().y()-32);
				
				QRect r(0, 0, 64, 64);
				
				m_indicator->paint(&painter, r, palette().color(QPalette::Highlight) );
			}
		}
		
	private:
		bool m_waiting;
		DGui::IndicatorDrawer *m_indicator;
		QTimeLine m_timeline;
};

ModelInspector::ModelInspector(QWidget *parent)
 : Common::ViewBase(parent), m_expectingXml(false)
{
	setWindowTitle(tr("Model inspector"));
	
	QVBoxLayout *layout = new QVBoxLayout(this);
	
	QSplitter *splitter = new QSplitter(Qt::Vertical);
	
	m_list = new ModelList;
	connect(m_list, SIGNAL(itemActivated(QListWidgetItem *)), this, SLOT(onItemActivated(QListWidgetItem *)));
	
	QWidget *container = new QWidget;
	QVBoxLayout *containerLayout = new QVBoxLayout(container);
	containerLayout->setSpacing(2);
	containerLayout->setMargin(2);
	
	DGui::TreeWidgetFilterLine *line = new DGui::TreeWidgetFilterLine;
	line->setFilterCaseSensitive(Qt::CaseInsensitive);
	
	m_tree = new QTreeWidget;
	m_tree->setHeaderLabels(QStringList() << tr("name") << tr("value") << tr("type"));
	
	m_tree->header()->setResizeMode(QHeaderView::ResizeToContents);
	
	line->setTreeWidget(m_tree);
	
	containerLayout->addWidget(line);
	containerLayout->addWidget(m_tree);
	
	splitter->addWidget(m_list);
	splitter->addWidget(container);
	
	layout->addWidget(splitter);
}


ModelInspector::~ModelInspector()
{
}

void ModelInspector::setModels(const QStringList &models)
{
	m_list->clear();
	m_list->addItems(models);
	m_tree->clear();
}

void ModelInspector::onReaded(const QString &text)
{
	if( m_expectingXml )
	{
		m_expectingXml = false;
		m_list->setWaiting(false);
		
		Common::ModelXmlParser parser;
		
		if( parser.parse(text) )
		{
			m_tree->clear();
			QList<Common::ModelXmlParser::Fields> objectInfo = parser.objectInfo();
			
			int counter = 0;
			foreach(Common::ModelXmlParser::Fields fields, objectInfo)
			{
				counter++;
				QTreeWidgetItem *object = new QTreeWidgetItem(m_tree);
				object->setText(0, QString::number(counter));
				
				QHashIterator<QString, Common::ModelXmlParser::Field> it(fields);
				while (it.hasNext())
				{
					it.next();
					
					QString fieldName = it.key();
					QString type = it.value().type;
					QString value = it.value().value;
					
					QTreeWidgetItem *field = new QTreeWidgetItem(object);
					field->setText(0, fieldName);
					field->setText(1, value);
					field->setText(2, type);
				}
			}
		}
	}
}

void ModelInspector::onRequestFailed()
{
	m_expectingXml = false;
	m_list->setWaiting(false);
}

void ModelInspector::onItemActivated(QListWidgetItem *item)
{
	QString model = item->text();
	
	m_expectingXml = true;
	
	m_list->setWaiting(true);
	
	emit toXml(model);
}

}

#include "modelinspector.moc"
