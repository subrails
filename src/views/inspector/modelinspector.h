/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef VIEWMODELINSPECTOR_H
#define VIEWMODELINSPECTOR_H

#include <common/viewbase.h>

class QListWidget;
class QListWidgetItem;

class QTreeWidget;

namespace View {
class ModelList;

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class ModelInspector : public Common::ViewBase
{
	Q_OBJECT
	public:
		ModelInspector(QWidget *parent = 0);
		~ModelInspector();
		
	public slots:
		void setModels(const QStringList &models);
		void onReaded(const QString &text);
		void onRequestFailed();
		
	private slots:
		void onItemActivated(QListWidgetItem *item);
		
	signals:
		void toXml(const QString &model);
		
	private:
		bool m_expectingXml;
		
		ModelList *m_list;
		QTreeWidget *m_tree;
};

}

#endif
