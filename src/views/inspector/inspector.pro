TEMPLATE = lib

CONFIG += staticlib \
warn_on

include(../views_config.pri)

HEADERS += modelinspector.h

SOURCES += modelinspector.cpp

