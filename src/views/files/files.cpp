/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "files.h"

#include <QTreeView>
#include <QDirModel>

#include <QVBoxLayout>

#include <common/global.h>

#include <dgui/iconloader.h>

namespace View {

Files::Files(QWidget *parent) : Common::ViewBase(parent)
{
	setWindowTitle(tr("Files"));
	setWindowIcon(DGui::IconLoader::self()->load("system-file-manager.svg"));
	
	QVBoxLayout *layout = new QVBoxLayout(this);
	
	m_view = new QTreeView;
	connect(m_view, SIGNAL(activated(const QModelIndex &)), this, SLOT(openFileFromIndex(const QModelIndex &)));
	
	m_model = new QDirModel;
	
	m_view->setModel(m_model);
	m_view->setAnimated(true);
	
	layout->addWidget(m_view);
	
	for(int i = 1; i < m_model->columnCount(); i++)
	{
		m_view->hideColumn(i);
	}
}


Files::~Files()
{
}


void Files::refresh()
{
	m_model->refresh();
}

void Files::setProjectDir(const QString &dir)
{
	m_view->setRootIndex(m_model->index(dir));
}

void Files::openFileFromIndex(const QModelIndex &index)
{
	if( index.isValid() )
	{
		QFileInfo finfo = m_model->fileInfo(index);
		
		if( !finfo.isDir() )
		{
			emit openFile(finfo.absoluteFilePath(), Common::All);
		}
	}
}


}

