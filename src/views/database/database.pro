TEMPLATE = lib

CONFIG += staticlib \
warn_on

include(../views_config.pri)


FORMS += ui/configure_database.ui

HEADERS += database.h \
configuredatabasedialog.h
SOURCES += database.cpp \
configuredatabasedialog.cpp
