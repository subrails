/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef VIEWDATABASE_H
#define VIEWDATABASE_H

#include <common/viewbase.h>

class QListWidget;
class QListWidgetItem;
class QFileSystemWatcher;

namespace View {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class SR_EXPORT Database : public Common::ViewBase
{
	Q_OBJECT;
	
	public:
		enum {
			FileName = 3452
		};
		
		Database(QWidget *parent = 0);
		~Database();
		
		void setProjectDir(const QString &projectDir);
		
	public slots:
		void loadMigrates();
		void configureDatabase();
		
	private slots:
		void openMigrateFile(QListWidgetItem *item );
		
	signals:
		void migrate();
		
	private:
		QString m_projectDir;
		QListWidget *m_migrates;
		QFileSystemWatcher *m_watcher;
};

}

#endif
