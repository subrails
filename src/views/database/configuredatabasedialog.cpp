/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "configuredatabasedialog.h"
#include "ui_configure_database.h"

#include <QFile>
#include <QTextStream>

namespace View {

ConfigureDatabaseDialog::ConfigureDatabaseDialog(const QString &databaseConfig, QWidget *parent)
 : QDialog(parent), m_databaseConfig(databaseConfig)
{
	ui = new Ui::ConfigureDatabaseDialog;
	ui->setupUi(this);
	
	ui->tabWidget->setTabText(0, tr("Development"));
	ui->tabWidget->setTabText(1, tr("Production"));
	ui->tabWidget->setTabText(2, tr("Test"));
	
	loadConfig();
	
	connect(ui->copyDevel, SIGNAL(activated(const QString &)), this, SLOT(copyToDevel(const QString &)));
	connect(ui->copyProd, SIGNAL(activated(const QString &)), this, SLOT(copyToProd(const QString &)));
	connect(ui->copyTests, SIGNAL(activated(const QString &)), this, SLOT(copyToTests(const QString &)));
}


ConfigureDatabaseDialog::~ConfigureDatabaseDialog()
{
	delete ui;
}

void ConfigureDatabaseDialog::loadConfig()
{
	QFile f(m_databaseConfig);
	
	if( f.open(QIODevice::ReadOnly) )
	{
		QTextStream ts(&f);
		
		Common::Environment current;
		QRegExp adapter("^\\s+adapter:\\s+(\\w+)");
		QRegExp database("^\\s+database:\\s+(\\w+)");
		QRegExp username("^\\s+username:\\s+(\\w+)");
		QRegExp password("^\\s+password:\\s+(\\w*)");
		QRegExp host("^\\s+host:\\s+(\\w+)");
		
		while( !ts.atEnd() )
		{
			QString line = ts.readLine();
			
			if( line.startsWith("development:") )
			{
				current = Common::Development;
			}
			else if(line.startsWith("production:") )
			{
				current = Common::Production;
			}
			else if(line.startsWith("test:"))
			{
				current = Common::Tests;
			}
			else if(adapter.indexIn(line) > -1 )
			{
				switch(current)
				{
					case Common::Development:
					{
						ui->adapterDevel->setCurrentIndex(ui->adapterDevel->findText(adapter.cap(1)) );
					}
					break;
					case Common::Production:
					{
						ui->adapterProd->setCurrentIndex(ui->adapterProd->findText(adapter.cap(1)));
					}
					break;
					case Common::Tests:
					{
						ui->adapterTests->setCurrentIndex(ui->adapterTests->findText(adapter.cap(1)));
					}
					break;
				}
			}
			else if(database.indexIn(line) > -1 )
			{
				switch(current)
				{
					case Common::Development:
					{
						ui->databaseDevel->setText(database.cap(1) );
					}
					break;
					case Common::Production:
					{
						ui->databaseProd->setText(database.cap(1) );
					}
					break;
					case Common::Tests:
					{
						ui->databaseTests->setText(database.cap(1) );
					}
					break;
				}
			}
			else if(username.indexIn(line) > -1 )
			{
				switch(current)
				{
					case Common::Development:
					{
						ui->userDevel->setText(username.cap(1) );
					}
					break;
					case Common::Production:
					{
						ui->userProd->setText(username.cap(1) );
					}
					break;
					case Common::Tests:
					{
						ui->userTests->setText(username.cap(1) );
					}
					break;
				}
			}
			else if(password.indexIn(line) > -1 )
			{
				switch(current)
				{
					case Common::Development:
					{
						ui->passwdDevel->setText(password.cap(1) );
					}
					break;
					case Common::Production:
					{
						ui->passwdProd->setText(password.cap(1) );
					}
					break;
					case Common::Tests:
					{
						ui->passwdTests->setText(password.cap(1) );
					}
					break;
				}
			}
			else if(host.indexIn(line) > -1 )
			{
				switch(current)
				{
					case Common::Development:
					{
						ui->hostDevel->setText(host.cap(1) );
					}
					break;
					case Common::Production:
					{
						ui->hostProd->setText(host.cap(1) );
					}
					break;
					case Common::Tests:
					{
						ui->hostTests->setText(host.cap(1) );
					}
					break;
				}
			}
		}
		
		f.close();
	}
}

void ConfigureDatabaseDialog::save()
{
	QFile f(m_databaseConfig);
	
	f.copy("database.yml.backup");
	f.reset();
	
	if( f.open(QIODevice::WriteOnly) )
	{
		QTextStream ts(&f);
		
		
		save(&ts, Common::Development);
		save(&ts, Common::Production);
		save(&ts, Common::Tests);
		
		f.close();
	}
}

void ConfigureDatabaseDialog::save(QTextStream *ts, Common::Environment env)
{
	switch(env)
	{
		case Common::Development:
		{
			(*ts) << "development:" << endl;
		}
		break;
		case Common::Production:
		{
			(*ts) << "production:" << endl;
		}
		break;
		case Common::Tests:
		{
			(*ts) << "test:" << endl;
		}
		break;
	}
	
	QMap<QString, QString> values = configuration(env);
	
	QMapIterator<QString, QString> i(values);
	while (i.hasNext())
	{
		i.next();
		
		(*ts) << "  " << i.key() << ": " << i.value() << endl;
	}
}

void ConfigureDatabaseDialog::copyToDevel(const QString &from)
{
	if( from.toLower() == tr("Production").toLower())
	{
		QMap<QString, QString> values = configuration(Common::Production);
		
		ui->adapterDevel->setCurrentIndex(ui->adapterDevel->findText(values.value("adapter")));
		ui->databaseDevel->setText(values.value("database"));
		ui->userDevel->setText(values.value("username"));
		ui->passwdDevel->setText(values.value("password"));
		ui->hostDevel->setText(values.value("host"));
	}
	else if( from.toLower() == tr("Test").toLower())
	{
		QMap<QString, QString> values = configuration(Common::Tests);
		
		ui->adapterDevel->setCurrentIndex(ui->adapterDevel->findText(values.value("adapter")));
		ui->databaseDevel->setText(values.value("database"));
		ui->userDevel->setText(values.value("username"));
		ui->passwdDevel->setText(values.value("password"));
		ui->hostDevel->setText(values.value("host"));
	}
}

void ConfigureDatabaseDialog::copyToProd(const QString &from)
{
	if( from.toLower() == tr("Development").toLower())
	{
		QMap<QString, QString> values = configuration(Common::Development);
		
		ui->adapterProd->setCurrentIndex(ui->adapterProd->findText(values.value("adapter")));
		ui->databaseProd->setText(values.value("database"));
		ui->userProd->setText(values.value("username"));
		ui->passwdProd->setText(values.value("password"));
		ui->hostProd->setText(values.value("host"));
	}
	else if( from.toLower() == tr("Test").toLower())
	{
		QMap<QString, QString> values = configuration(Common::Tests);
		
		ui->adapterProd->setCurrentIndex(ui->adapterDevel->findText(values.value("adapter")));
		ui->databaseProd->setText(values.value("database"));
		ui->userProd->setText(values.value("username"));
		ui->passwdProd->setText(values.value("password"));
		ui->hostProd->setText(values.value("host"));
	}
}

void ConfigureDatabaseDialog::copyToTests(const QString &from)
{
	if( from.toLower() == tr("Development").toLower())
	{
		QMap<QString, QString> values = configuration(Common::Development);
		
		ui->adapterTests->setCurrentIndex(ui->adapterTests->findText(values.value("adapter")));
		ui->databaseTests->setText(values.value("database"));
		ui->userTests->setText(values.value("username"));
		ui->passwdTests->setText(values.value("password"));
		ui->hostTests->setText(values.value("host"));
	}
	else if( from.toLower() == tr("Production").toLower())
	{
		QMap<QString, QString> values = configuration(Common::Production);
		
		ui->adapterTests->setCurrentIndex(ui->adapterTests->findText(values.value("adapter")));
		ui->databaseTests->setText(values.value("database"));
		ui->userTests->setText(values.value("username"));
		ui->passwdTests->setText(values.value("password"));
		ui->hostTests->setText(values.value("host"));
	}
}

QMap<QString, QString> ConfigureDatabaseDialog::configuration(Common::Environment env)
{
	QMap<QString, QString> configuration;
	
	switch(env)
	{
		case Common::Development:
		{
			configuration["adapter"] = ui->adapterDevel->currentText();
			configuration["database"] = ui->databaseDevel->text();
			configuration["username"] = ui->userDevel->text();
			configuration["password"] = ui->passwdDevel->text();
			configuration["host"] = ui->hostDevel->text();
		}
		break;
		case Common::Production:
		{
			configuration["adapter"] = ui->adapterProd->currentText();
			configuration["database"] = ui->databaseProd->text();
			configuration["username"] = ui->userProd->text();
			configuration["password"] = ui->passwdProd->text();
			configuration["host"] = ui->hostProd->text();
		}
		break;
		case Common::Tests:
		{
			configuration["adapter"] = ui->adapterTests->currentText();
			configuration["database"] = ui->databaseTests->text();
			configuration["username"] = ui->userTests->text();
			configuration["password"] = ui->passwdTests->text();
			configuration["host"] = ui->hostTests->text();
		}
		break;
	}
	
	return configuration;
}

}

