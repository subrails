/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "database.h"

#include <QListWidget>
#include <QVBoxLayout>
#include <QDir>
#include <QLabel>
#include <QFileSystemWatcher>
#include <QPushButton>

#include <common/global.h>

#include "configuredatabasedialog.h"
#include <dgui/iconloader.h>

namespace View {

Database::Database(QWidget *parent)
 : Common::ViewBase(parent)
{
	setWindowTitle(tr("Database"));
	setWindowIcon(DGui::IconLoader::self()->load("x-office-presentation.svg"));
	
	QVBoxLayout *layout = new QVBoxLayout(this);
	
	layout->addWidget(new QLabel(tr("Migrates")));
	m_migrates = new QListWidget;
	
	connect(m_migrates, SIGNAL(itemActivated( QListWidgetItem* )), this, SLOT(openMigrateFile(QListWidgetItem* )));
	
	layout->addWidget(m_migrates);
	
	QHBoxLayout *buttons = new QHBoxLayout;
	
	QPushButton *configure = new QPushButton(tr("configure"));
	connect(configure, SIGNAL(clicked()), this, SLOT(configureDatabase()));
	buttons->addWidget(configure);
	
	QPushButton *migrate = new QPushButton(tr("migrate"));
	connect(migrate, SIGNAL(clicked()), this, SIGNAL(migrate()));
	buttons->addWidget(migrate);
	
	
	layout->addLayout(buttons);
	
	m_watcher = new QFileSystemWatcher(this);
	connect(m_watcher, SIGNAL(directoryChanged( const QString& )), this, SLOT(loadMigrates()));
}


Database::~Database()
{
}

void Database::setProjectDir(const QString &projectDir)
{
	m_watcher->removePath(m_projectDir+"/db/migrate");
	
	m_projectDir = projectDir;
	
	if( !m_projectDir.isEmpty() )
		loadMigrates();
	else
		m_migrates->clear();
}

void Database::loadMigrates()
{
	m_migrates->clear();
	
	QDir migrates(m_projectDir+"/db/migrate");
	
	if( !migrates.exists() )
	{
		migrates.mkdir(migrates.absolutePath());
	}
	
	m_watcher->addPath(migrates.absolutePath());
	
	foreach(QString file, migrates.entryList(QStringList() << "*_create_*.rb") )
	{
		QListWidgetItem *item = new QListWidgetItem(file, m_migrates);
		item->setData(FileName, migrates.absoluteFilePath(file));
	}
}

void Database::configureDatabase()
{
	ConfigureDatabaseDialog dialog(m_projectDir+"/config/database.yml");
	
	if( dialog.exec() == QDialog::Accepted )
	{
		dialog.save();
	}
}

void Database::openMigrateFile(QListWidgetItem *item )
{
	if( item )
	{
		QString file = item->data(FileName).toString();
		
		emit openFile(file, Common::Model);
	}
}

}


