TEMPLATE = lib

CONFIG += staticlib \
warn_on

include(../views_config.pri)

HEADERS += messages.h \
server.h
SOURCES += messages.cpp \
server.cpp
