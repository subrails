/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef VIEWSOURCENAVIGATION_H
#define VIEWSOURCENAVIGATION_H

#include <QHash>

#include <common/viewbase.h>

class QTreeWidget;
class QTreeWidgetItem;

namespace View {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class SR_EXPORT SourceNavigation : public Common::ViewBase
{
	Q_OBJECT;
	
	public:
		enum {
			FileName = 1235,
			Position,
			Line
		};
		SourceNavigation(QWidget *parent = 0);
		~SourceNavigation();
		
		void clear();
		
		void removeTags(const QString &fileName);
		void loadTags(const QString &fileName);
		
	private slots:
		void onItemActivated(QTreeWidgetItem *item, int);
		
	private:
		QTreeWidget *m_tree;
		QHash<QString, QList<QTreeWidgetItem *> > m_itemsForFile;
};

}

#endif
