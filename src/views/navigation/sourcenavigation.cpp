/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "sourcenavigation.h"

#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFile>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <QTimer>
#include <QHash>
#include <QHeaderView>
#include <QToolButton>


#include <common/global.h>
#include <dcore/debug.h>
#include <dgui/treewidgetfilterline.h>
#include <dgui/iconloader.h>

namespace View {

/**
 * 
 * @param parent 
 */
SourceNavigation::SourceNavigation(QWidget *parent)
 : Common::ViewBase(parent)
{
	setWindowTitle(tr("Navigation"));
	setWindowIcon(DGui::IconLoader::self()->load("system-search.svg"));
	
	QVBoxLayout *layout = new QVBoxLayout(this);
	
	m_tree = new QTreeWidget;
	
	m_tree->setHeaderLabels(QStringList() << "" );
	m_tree->header()->hide();
	
	connect(m_tree, SIGNAL(itemActivated( QTreeWidgetItem*, int )), this, SLOT(onItemActivated(QTreeWidgetItem*, int)));
	
	DGui::TreeWidgetFilterLine *m_search = new DGui::TreeWidgetFilterLine(this);
	m_search->setTreeWidget(m_tree);
	
	QHBoxLayout *search = new QHBoxLayout;
	search->setSpacing(2);

	QToolButton *button = new QToolButton;
	button->setText(tr("clear"));
	button->setIcon(DGui::IconLoader::self()->load("edit-clear.svg"));
	
	search->addWidget(button);
	
	search->addWidget( m_search );
	connect(button, SIGNAL(clicked()), m_search, SLOT(clear()));
	
	layout->addLayout(search);
	
	layout->addWidget(m_tree);
}


SourceNavigation::~SourceNavigation()
{
}

void SourceNavigation::clear()
{
	m_tree->clear();
}

/**
 * Remove all items referencing to fileName
 * @param fileName 
 * @return 
 */
void SourceNavigation::removeTags(const QString &fileName)
{
	if( m_itemsForFile.contains(fileName) )
	{
		m_tree->setUpdatesEnabled(false);
		m_tree->clearSelection();
		
		QList<QTreeWidgetItem *> items = m_itemsForFile[fileName];
		
		while( !items.isEmpty() )
		{
			QTreeWidgetItem *item = items.takeLast();
			
			if( item )
			{
				item->setSelected(false);
				delete item;
			}
		}
		
		
		m_itemsForFile.remove(fileName);
		
		m_tree->setUpdatesEnabled(true);
	}
}

void SourceNavigation::loadTags(const QString &fileName)
{
	QFile f(fileName);
	if( f.open(QIODevice::ReadOnly ) )
	{
		QRegExp entity("^(module|class|def)\\s(\\w+)\\s([\\w:]+)(#\\w+){0,1}\\s(\\d+),(\\d+)");
		QRegExp filerx("^(.+),\\d+\\s*$");
		
		setUpdatesEnabled(false);
		
		removeTags(fileName);
		
		QTextStream ts(&f);
		
		QHash<QString, QTreeWidgetItem *> containers;
		QString sourceFile;
		
		while(!ts.atEnd())
		{
			QString trimmed = ts.readLine();
			
			if( entity.indexIn(trimmed) > -1 )
			{
				QString type = entity.cap(1);
				
				if( type == "module" || type == "class" )
				{
					QString name = entity.cap(2);
					QString container = entity.cap(3);
					
					QStringList splitted = container.split("::");
					splitted.removeLast();
					
					QString mcontainer = splitted.join("::");
					
					QTreeWidgetItem *item = 0;
					if( containers.contains(mcontainer) )
					{
						item = new QTreeWidgetItem(containers.value(mcontainer) );
					}
					else
					{
						item = new QTreeWidgetItem(m_tree);
					}
					
					item->setText(0, type +" "+name);
					
					item->setData(0, FileName, sourceFile);
					item->setData(0, Position, entity.cap(6).toInt());
					item->setData(0, Line, entity.cap(5).toInt());
					
					containers.insert(container, item);
					
					m_itemsForFile[fileName] << item;
				}
				else if(entity.cap(1) == "def" )
				{
					QString name = entity.cap(2);
					QString container = entity.cap(3);
					
					QTreeWidgetItem *item = 0;
					if( containers.contains(container) )
					{
						item = new QTreeWidgetItem(containers.value(container) );
					}
					else
					{
						item = new QTreeWidgetItem(m_tree);
					}
					
					item->setText(0, "def "+name);
					item->setData(0, FileName, sourceFile);
					item->setData(0, Position, entity.cap(6).toInt());
					item->setData(0, Line, entity.cap(5).toInt());
					
					m_itemsForFile[fileName] << item;
				}
			}
			else if( filerx.indexIn(trimmed) > -1 )
			{
				sourceFile = filerx.cap(1);
			}
		}
		
		f.close();
		
		setUpdatesEnabled(true);
	}
	
}

void SourceNavigation::onItemActivated(QTreeWidgetItem *item, int)
{
	if( item )
	{
		QString file = item->data(0, FileName).toString();
		int line = item->data(0, Line).toInt();
		
		emit openFile(file, Common::All);
		
		emit showEditor(file, line);
	}
}

}


