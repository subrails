/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef VIEWDEBUGGER_H
#define VIEWDEBUGGER_H

#include <common/viewbase.h>

class QTabWidget;
class QTextBrowser;

namespace View {

class SyntaxPage;

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class SR_EXPORT Debugger : public Common::ViewBase
{
	Q_OBJECT;
	
	public:
		Debugger(QWidget *parent = 0);
		~Debugger();
		
		void clearSyntax();
		
		void appendErrorToSyntax(const QString &text);
		void appendOutputToSyntax(const QString &text);
		
		void moveToLine(const QString &file, int line);
		
	private:
		void analizeSyntaxText(const QString &text);
		
	private:
		QTabWidget *m_tabs;
		SyntaxPage *m_syntaxPage;
};

}

#endif
