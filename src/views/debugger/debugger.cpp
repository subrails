/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "debugger.h"
#include <QTabWidget>
#include <QVBoxLayout>
#include <QTextBrowser>
#include <QMouseEvent>
#include <QTextBlock>

#include <dcore/debug.h>
#include <dgui/iconloader.h>

namespace View {

class SyntaxPage : public QTextBrowser
{
	public:
		SyntaxPage(Debugger *debugger);
		~SyntaxPage();
		
	protected:
		void mousePressEvent(QMouseEvent *e);
		
	private:
		Debugger *m_debugger;
};

SyntaxPage::SyntaxPage(Debugger *debugger) : m_debugger(debugger)
{
}

SyntaxPage::~SyntaxPage()
{
}


void SyntaxPage::mousePressEvent(QMouseEvent *e)
{
	QString text = cursorForPosition(e->pos()).block().text();
	
	QRegExp erx("(.*):(\\d+):"); // 1: file 2: line
	if( erx.indexIn(text) > -1 )
	{
		QString file = erx.cap(1);
		int line = erx.cap(2).toInt();
		
		m_debugger->moveToLine(file, line);
	}
	
	QTextBrowser::mousePressEvent(e);
}

Debugger::Debugger(QWidget *parent)
 : Common::ViewBase(parent)
{
	setWindowTitle(tr("Debug"));
	setWindowIcon(DGui::IconLoader::self()->load("dialog-warning.svg"));
	
	QVBoxLayout *mainLayout = new QVBoxLayout(this);
	
	m_tabs = new QTabWidget;
	mainLayout->addWidget(m_tabs);
	
	m_syntaxPage = new SyntaxPage(this);
	m_syntaxPage->setUndoRedoEnabled(false);
	
	m_tabs->addTab(m_syntaxPage, tr("Syntax"));
}


Debugger::~Debugger()
{
}

void Debugger::clearSyntax()
{
	m_syntaxPage->clear();
}

void Debugger::appendOutputToSyntax(const QString &text)
{
	m_syntaxPage->append(text);
}


void Debugger::appendErrorToSyntax(const QString &text)
{
	foreach(QString line, text.split(QRegExp("$")) )
	{
		if( ! line.simplified().isEmpty() )
		{
			m_syntaxPage->append(line);
		}
	}
}

void Debugger::moveToLine(const QString &file, int line)
{
	emit showEditor(file, line);
}


}

