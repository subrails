/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "generatemodeldialog.h"
#include "ui_generate_model.h"

#include <QHeaderView>

#include <dgui/iconloader.h>

GenerateModelDialog::GenerateModelDialog(QWidget *parent)
 : QDialog(parent)
{
	ui = new Ui::GenerateModelDialog;
	ui->setupUi(this);
	
	ui->fieldTable->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
	ui->fieldTable->setHorizontalHeaderLabels(QStringList() << tr("Name") << tr("Type"));
	
	connect(ui->addField, SIGNAL(clicked()), this, SLOT(addField()));
	connect(ui->removeField, SIGNAL(clicked()), this, SLOT(removeField()));
	
	ui->addField->setIcon(DGui::IconLoader::self()->load("list-add.svg"));
	ui->removeField->setIcon(DGui::IconLoader::self()->load("list-remove.svg"));
	
	
}


GenerateModelDialog::~GenerateModelDialog()
{
	delete ui;
}

QString GenerateModelDialog::model() const
{
	return ui->modelName->text();
}

QStringList GenerateModelDialog::fields() const
{
	QStringList fields;
	for(int row = 0; row < ui->fieldTable->rowCount(); row++)
	{
		fields <<  ui->fieldTable->item(row, 0)->text() + ":" + static_cast<QComboBox *>(ui->fieldTable->cellWidget(row, 1))->currentText();
	}
	return fields;
}

void GenerateModelDialog::addField()
{
	QString field = ui->fieldName->text();
	QString type = ui->fieldType->currentText();
	
	int row = ui->fieldTable->rowCount();
	ui->fieldTable->insertRow(row);
	
	QTableWidgetItem * newField = new QTableWidgetItem();
	newField->setText(field);
	ui->fieldTable->setItem(row, 0, newField);
	
	QComboBox *newType = new QComboBox();
	
	for(int index = 0; index < ui->fieldType->count(); index++)
	{
		newType->addItem(ui->fieldType->itemText(index));
	}
	ui->fieldTable->setCellWidget(row, 1, newType);
	
	newType->setCurrentIndex(ui->fieldType->currentIndex());
}

void GenerateModelDialog::removeField()
{
	ui->fieldTable->removeRow(ui->fieldTable->currentRow());
}




