/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "mainwindow.h"

#include <QtGui>


#include <common/global.h>
#include <common/viewbase.h>

#include <core/projectmanager.h>
#include <core/databasemanager.h>
#include <core/utils.h>
#include <core/debugmanager.h>
#include <core/sourcenavigator.h>
#include <core/scriptrunner.h>

#include <editor/view.h>
#include <editor/profile.h>
#include <editor/manager.h>
#include <editor/tabwidget.h>

#include <views/files/files.h>
#include <views/project/settings.h>
#include <views/project/project.h>

#include <views/database/database.h>
#include <views/messages/messages.h>
#include <views/messages/server.h>
#include <views/debugger/debugger.h>
#include <views/navigation/sourcenavigation.h>
#include <views/inspector/modelinspector.h>

#include <views/project/newprojectdialog.h>

#include <dgui/osd.h>
#include <dgui/configurationdialog.h>
#include <dgui/iconloader.h>
#include <dcore/debug.h>

#include "generatecontrollerdialog.h"
#include "generatemodeldialog.h"
#include "generatescaffolddialog.h"
#include "settingsdialog.h"
#include "ui_generate_custom.h"

#include "../core/ui_wait_dialog.h"

#ifdef Q_WS_X11
#include <dgui/term.h>
#include <dgui/termtab.h>
#endif

MainWindow::MainWindow()
 : Ideality::TabbedMainWindow()
{
	setWindowTitle(tr("SubRails: The Rails IDE "));
	
	m_tabWidget = new Editor::TabWidget;
	loadSettings();
	setTabWidget(m_tabWidget);
	
	m_projectManager = new Core::ProjectManager(this);
	connect(m_projectManager, SIGNAL(projectOpened(const QString &)), this, SLOT(projectOpened(const QString &)));
	
	m_databaseManager = new Core::DatabaseManager(this);
	
	m_utils = new Core::Utils(this);
	connect(m_utils, SIGNAL(scriptFinished(bool)), this, SLOT(updateViews(bool)));
	connect(m_utils, SIGNAL(projectCreated(const QString&)), this, SLOT(openProject(const QString&)));
	
	m_sourceNavigator = new Core::SourceNavigator(this);
	
	View::SourceNavigation *navigationView = m_sourceNavigator->view();
	connectView(navigationView);
	
	m_debugManager = new Core::DebugManager(this);
	View::Debugger *debuggerView = m_debugManager->view();
	connectView(debuggerView);
	
	View::Project *projectView = m_projectManager->view();
	connect(projectView, SIGNAL(generateController()), this, SLOT(generateController()));
	connect(projectView, SIGNAL(destroyController(const QString &)), this, SLOT(destroyController(const QString &)));
	
	connect(projectView, SIGNAL(generateModel()), this, SLOT(generateModel()));
	connect(projectView, SIGNAL(destroyModel(const QString &)), this, SLOT(destroyModel(const QString &)));
	
	connect(projectView, SIGNAL(generateScaffold()), this, SLOT(generateScaffold()));
	connect(projectView, SIGNAL(generateCustom()), this, SLOT(generateCustom()));
	connect(projectView, SIGNAL(destroyCustom()), this, SLOT(destroyCustom()));
	
	connectView(projectView);
	
	
	m_filesView = new View::Files;
	connectView(m_filesView);
	
	View::Messages *messagesView = m_utils->genericView();
	connectView(messagesView);
	
	View::Server *serverView = m_utils->serverView();
	connectView(serverView);
	
	connect(this, SIGNAL(perspectiveChanged(int)), m_projectManager, SLOT(perspectiveChanged(int)));
	
	View::Database *databaseView = m_databaseManager->view();
	connectView(databaseView);
	
	
	m_scriptRunner = new Core::ScriptRunner(this);
	
	View::ModelInspector *inspector = new View::ModelInspector;
	connect(inspector, SIGNAL(toXml(const QString&)), m_scriptRunner, SLOT(toXml(const QString &)));
	connect(m_scriptRunner, SIGNAL(readed(const QString&)), inspector, SLOT(onReaded(const QString &)));
	connect(m_scriptRunner, SIGNAL(failed()), inspector, SLOT(onRequestFailed()));
	connect(projectView, SIGNAL(modelsChanged(const QStringList&)), inspector, SLOT(setModels(const QStringList &)));
	
	
	setupMenu();
	
	setCurrentPerspective(Common::All);
	
	m_waitDialog = new QDialog;
	connect(m_waitDialog, SIGNAL(rejected()), m_utils, SLOT(abortCurrentTask()));
	
	Ui::WaitDialog wdialog;
	wdialog.setupUi(m_waitDialog);
	m_waitDialog->setModal(true);
	
	tabWidget()->setIconSize(QSize(12, 12));
	
	addToolView(projectView, Qt::LeftDockWidgetArea, Common::All);
	addToolView(m_filesView, Qt::LeftDockWidgetArea, Common::All);
	
	addToolView(databaseView, Qt::RightDockWidgetArea, Common::All);
	addToolView(navigationView, Qt::RightDockWidgetArea, Common::All);
	addToolView(inspector, Qt::RightDockWidgetArea, Common::All);
	
	addToolView(messagesView, Qt::BottomDockWidgetArea, Common::All);
	addToolView(serverView, Qt::BottomDockWidgetArea, Common::All);
	addToolView(debuggerView, Qt::BottomDockWidgetArea, Common::All);
	
#ifdef Q_WS_X11
	DGui::TermTab *termTab = new DGui::TermTab;
	termTab->setWindowTitle(tr("Terminal"));
	termTab->setWindowIcon(DGui::IconLoader::self()->load("utilities-terminal.svg"));
	addToolView(termTab, Qt::BottomDockWidgetArea, Common::All);
	termTab->show();
#endif
	
	connect(tabWidget(), SIGNAL(currentChanged(int)), this, SLOT(onTabChanged(int)) );
}


MainWindow::~MainWindow()
{
}

void MainWindow::saveSettings()
{
	QSettings *settings = Common::settings();
	settings->endGroup();
	m_tabWidget->saveSettings(settings);
}


void MainWindow::loadSettings()
{
	QSettings *settings = Common::settings();
	settings->endGroup();
	m_tabWidget->loadSettings(settings);
}

void MainWindow::configureProject()
{
	View::ProjectSettings settings;
	settings.setProjectDir(m_projectManager->projectDir());
	
	if( settings.exec() != QDialog::Rejected )
	{
		
	}
}

void MainWindow::newProject()
{
	View::NewProjectDialog dialog;
	
	if (dialog.exec() == QDialog::Accepted )
	{
		if( !m_utils->createProject(dialog.projectLocation(), dialog.adapter(), dialog.freeze()) )
		{
			DGui::Osd::self()->display(tr("Cannot create project"), DGui::Osd::Fatal);
		}
	}
}

void MainWindow::openProject()
{
	QString dir = QFileDialog::getExistingDirectory(this, tr("Choose the project directory..."), Common::workingDirectory());
	
	if( dir.isEmpty() ) return;
	
	openProject(dir);
}

void MainWindow::openProject(const QString &dir)
{
	if( m_projectManager->openProjectFrom(dir) )
	{
		DGui::Osd::self()->display(tr("Opened!"));
	}
	else
	{
		DGui::Osd::self()->display(tr("Cannot open project!"), DGui::Osd::Error);
	}
}

void MainWindow::closeProject()
{
	m_projectManager->closeProject();
	m_databaseManager->setProjectDir(QString());
	m_utils->setProjectDir(QString());
	m_sourceNavigator->setProjectDir(QString());
	
	m_tabWidget->closeAll();
	
	m_actionManager["configure_project"]->setEnabled(false);
	m_actionManager["close_project"]->setEnabled(false);
	m_actionManager.setEnabled("server", false);
	
	m_actions->setEnabled(false);
}

void MainWindow::projectOpened(const QString &dir)
{
	m_filesView->setProjectDir(dir);
	m_utils->setProjectDir(dir);
	m_databaseManager->setProjectDir(dir);
	m_sourceNavigator->setProjectDir(dir);
	m_scriptRunner->setProjectDir(dir);
	
	m_actionManager["configure_project"]->setEnabled(true);
	m_actionManager["close_project"]->setEnabled(true);
	
	m_actionManager.setEnabled("server", true);
	
	m_actions->setEnabled(true);
}

void MainWindow::editFile(const QString &fileName, int workspace)
{
	if( !QFile::exists(fileName) ) return;
	
	Editor::View *view = m_tabWidget->editFile(fileName);
	
	if( view )
	{
		if( fileName.endsWith(".rb", Qt::CaseInsensitive) )
		{
			view->setCompleter(m_sourceNavigator->completer());
		}
		
		int currentIndex = tabWidget()->currentIndex();
		insertWidget(currentIndex+1, view, false, workspace);
		
		m_debugManager->checkSyntax(fileName);
	}
}

void MainWindow::showEditor(const QString &fileName, int line)
{
	Editor::View *editor = m_tabWidget->editFile(fileName);
	editor->goToLine(line);
}

void MainWindow::about()
{
	QMessageBox::about(this, tr("About SubRails"),
		tr("SubRails is an IDE for ruby on rails."));
}

void MainWindow::configure()
{
	SettingsDialog dialog(m_tabWidget->manager());
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->newFileAction());
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->openFileAction());
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->saveFileAction());
		
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->saveFileAsAction());
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->closeFileAction());
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->cutAction());
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->copyAction());
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->pasteAction());
	
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->toggleBookmarkAction());
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->nextBookmarkAction());
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->previousBookmarkAction());
	
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->undoAction());
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->redoAction());
	
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->findAction());
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->findNextAction());
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->findPreviousAction());
	
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->findInFilesAction());
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->replaceInFilesAction());
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->replaceAction());
	
	dialog.addShortcutToConfigure(tr("Editor"), m_tabWidget->reloadAction());
	
	foreach(QAction *a, m_actionManager.actions("project"))
	{
		dialog.addShortcutToConfigure(tr("Project"), a);
	}
	
	foreach(QAction *a, m_actionManager.actions("project"))
	{
		dialog.addShortcutToConfigure(tr("Project"), a);
	}
	
	if( dialog.exec() != QDialog::Rejected )
	{
		m_tabWidget->setProfile(dialog.currentProfile());
	}
}

void MainWindow::closeEvent(QCloseEvent *e)
{
	if( m_tabWidget->close() )
	{
		saveSettings();
		Ideality::TabbedMainWindow::closeEvent(e);
	}
	else
	{
		e->ignore();
	}
}

void MainWindow::onTabChanged(int index)
{
	if( Editor::View *editor = qobject_cast<Editor::View *>(tabWidget()->widget(index)) )
	{
		QString fileName = m_tabWidget->fileName(editor);
		
		if( !fileName.isEmpty() )
		{
			m_debugManager->checkSyntax(fileName);
		}
	}
}

void MainWindow::setupMenu()
{
	QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
	
	QAction *newFile = m_tabWidget->newFileAction();
	fileMenu->addAction(newFile);
	
	QAction *openFile = m_tabWidget->openFileAction();
	fileMenu->addAction(openFile);
	
	fileMenu->addSeparator();
	
	QAction *saveFile = m_tabWidget->saveFileAction();
	fileMenu->addAction(saveFile);
	
	QAction *saveFileAs = m_tabWidget->saveFileAsAction();
	fileMenu->addAction(saveFileAs);
	
	fileMenu->addSeparator();
	
	QAction *closeFile = m_tabWidget->closeFileAction();
	fileMenu->addAction(closeFile);
	
	fileMenu->addSeparator();
	m_actionManager.add(fileMenu->addAction(tr("&Quit"), qApp, SLOT(closeAllWindows())), "quit", "application");
	
	////////////
	
	QMenu *editMenu = menuBar()->addMenu(tr("&Edit"));
	
	QAction *undo = m_tabWidget->undoAction();
	editMenu->addAction(undo);
	
	QAction *redo = m_tabWidget->redoAction();
	editMenu->addAction(redo);
	
	editMenu->addSeparator();
	
	QAction *cut = m_tabWidget->cutAction();
	editMenu->addAction(cut);
	
	QAction *copy = m_tabWidget->copyAction();
	editMenu->addAction(copy);
	
	QAction *paste = m_tabWidget->pasteAction();
	editMenu->addAction(paste);
	
	editMenu->addSeparator();
	
	QAction *find = m_tabWidget->findAction();
	editMenu->addAction(find);
	
	QAction *replace = m_tabWidget->replaceAction();
	editMenu->addAction(replace);
	
	
	editMenu->addAction(m_tabWidget->findInFilesAction());
	
	editMenu->addSeparator();
	/*QAction *settings = */editMenu->addAction(tr("Settings..."), this, SLOT(configure()));
	
	
	////////////
	
	QMenu *projectMenu = menuBar()->addMenu(tr("&Project"));
	
	QAction *newProject = projectMenu->addAction(tr("New project..."), this, SLOT(newProject()));
	
	QAction *openProject = projectMenu->addAction(tr("Open project..."), this, SLOT(openProject()));
	
	projectMenu->addSeparator();
	
	m_actions = m_projectManager->view()->actionsMenu();
	projectMenu->addMenu(m_actions);
	m_actions->setEnabled(false);
	
	projectMenu->addSeparator();
	
	QAction *configureProject = projectMenu->addAction(tr("Configure project..."), this, SLOT(configureProject()));
	
	projectMenu->addSeparator();
	
	QAction *closeProject = projectMenu->addAction(tr("Close project"), this, SLOT(closeProject()));
	
	m_actionManager.add(newProject, "new_project", "project");
	m_actionManager.add(openProject, "open_project", "project");
	
	m_actionManager.add(configureProject, "configure_project", "project");
	m_actionManager.add(closeProject, "close_project", "project");
	
	configureProject->setEnabled(false);
	closeProject->setEnabled(false);
	
	////////////////
	
	QMenu *serverMenu = menuBar()->addMenu(tr("&Server"));
	
	QAction *startServer = serverMenu->addAction(tr("Start"));
	connect(startServer, SIGNAL(triggered()), m_utils, SLOT(runApplication()));
	startServer->setEnabled(false);
	
	QAction *stopServer = serverMenu->addAction(tr("Stop"));
	connect(stopServer, SIGNAL(triggered()), m_utils, SLOT(stopApplication()));
	
	stopServer->setEnabled(false);
	
	m_actionManager.add(startServer, "start_server", "server");
	m_actionManager.add(stopServer, "stop_server", "server");
	
	////////////
	QMenu *bookmarksMenu = menuBar()->addMenu(tr("&Bookmarks"));
	
	bookmarksMenu->addAction( m_tabWidget->toggleBookmarkAction() );
	
	bookmarksMenu->addAction( m_tabWidget->nextBookmarkAction() );
	
	bookmarksMenu->addAction( m_tabWidget->previousBookmarkAction() );
	
	////////////
	
// 	QMenu *settingsMenu = menuBar()->addMenu(tr("&Settings"));
	
	////////////
	
	perspectiveAction(Common::All)->setText(tr("All"));
	perspectiveAction(Common::View)->setText(tr("View"));
	perspectiveAction(Common::Model)->setText(tr("Model"));
	perspectiveAction(Common::Controller)->setText(tr("Controller"));
	
	menuBar()->addMenu(createPerspectiveMenu());
	
	////////////
	
	
	QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));
	helpMenu->addAction(tr("About..."), this, SLOT(about()));
	
	///////////
	
	QToolBar *mainToolBar = new QToolBar(tr("Main"), this);
	m_tabWidget->setupToolBar(mainToolBar);
	
// 	mainToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	
	QToolBar *sessions = new QToolBar(tr("Sessions"), this);
	
	sessions->addWidget(m_tabWidget->sessionSwitcher())->setVisible(true);
	
	addToolBar(Qt::TopToolBarArea, mainToolBar);
	addToolBar(Qt::TopToolBarArea, sessions);
}

void MainWindow::connectView(Common::ViewBase *view)
{
	connect(view, SIGNAL(openFile(const QString &, int)), this, SLOT(editFile(const QString &, int)));
	connect(view, SIGNAL(showEditor(const QString&, int)), this, SLOT(showEditor(const QString&, int)));
	connect(view, SIGNAL(runApplication(const QString &)), m_utils, SLOT(runApplication(const QString &)));
}

void MainWindow::generateController()
{
	GenerateControllerDialog dialog;
	if ( dialog.exec() == QDialog::Accepted )
	{
		if( m_utils->generateController(dialog.controller(), dialog.actions()) )
		{
			m_waitDialog->show();
		}
	}
}

void MainWindow::destroyController(const QString &controller)
{
	if( m_utils->destroyController(controller) )
		m_waitDialog->show();
}

void MainWindow::generateModel()
{
	GenerateModelDialog dialog;
	if ( dialog.exec() == QDialog::Accepted )
	{
		if( m_utils->generateModel(dialog.model(), dialog.fields()) )
			m_waitDialog->show();
	}
}

void MainWindow::destroyModel(const QString &model)
{
	if( m_utils->destroyModel(model) )
		m_waitDialog->show();
}


void MainWindow::generateScaffold()
{
	GenerateScaffoldDialog dialog;
	dialog.setModels(m_projectManager->models());
	if ( dialog.exec() == QDialog::Accepted )
	{
		if( m_utils->generateScaffold(dialog.model(), dialog.controller(), dialog.actions()) )
			m_waitDialog->show();
	}
}

void MainWindow::generateCustom()
{
	QDialog dialog;
	Ui::GenerateCustomDialog ui;
	ui.setupUi(&dialog);
	
	dialog.setWindowTitle(tr("Generate custom..."));
	
	if( dialog.exec() == QDialog::Accepted )
	{
		m_utils->generateCustom(ui.generator->text(), ui.args->text().split(" "));
	}
}

void MainWindow::destroyCustom()
{
	QDialog dialog;
	Ui::GenerateCustomDialog ui;
	ui.setupUi(&dialog);
	
	dialog.setWindowTitle(tr("Destroy custom..."));
	
	if( dialog.exec() == QDialog::Accepted )
	{
		m_utils->destroyCustom(ui.generator->text(), ui.args->text().split(" "));
	}
}

void MainWindow::updateViews(bool ok)
{
	if( ok )
	{
// 		m_projectManager->update();
		m_filesView->refresh();
	}
	
	m_waitDialog->close();
}

void MainWindow::closeCurrentTab()
{
	m_tabWidget->closeFile();
// 	Ideality::TabbedMainWindow::closeCurrentTab();
}



