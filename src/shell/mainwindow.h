/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QHash>
#include <QFileSystemWatcher>

#include <ideality/tabbedmainwindow.h>
#include <ideality/toolview.h>

#include <dgui/actionmanager.h>

class QDialog;

namespace Editor {
	class View;
	class Profile;
	class Manager;
	class TabWidget;
}

namespace Core {
	class ProjectManager;
	class DatabaseManager;
	class Utils;
	class DebugManager;
	class SourceNavigator;
	class ScriptRunner;
}

namespace Common {
	class ViewBase;
}

namespace View {
	class Files;
	class Messages;
}

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class MainWindow : public Ideality::TabbedMainWindow
{
	Q_OBJECT;
	public:
		MainWindow();
		~MainWindow();
		
		void saveSettings();
		void loadSettings();
		
	private slots: // Project
		void configureProject();
		void newProject();
		void openProject();
		void openProject(const QString &dir);
		void closeProject();
		
		void projectOpened(const QString &dir);
		
	private slots: // Actions
		void generateController();
		void destroyController(const QString &controller);
		void generateModel();
		void destroyModel(const QString &model);
		void generateScaffold();
		
		void generateCustom();
		void destroyCustom();
		
		void updateViews(bool ok);
		
	private slots: // Editor
		void closeCurrentTab();
		
	private slots: // Document
		void editFile(const QString &fileName, int workspace);
		void showEditor(const QString &fileName, int line);
		
	private slots:
		void about();
		void configure();
		void onTabChanged(int index);
		
	protected:
		void closeEvent(QCloseEvent *e);
		
	private:
		void setupMenu();
		void connectView(Common::ViewBase *view);
		
	private:
		Editor::TabWidget *m_tabWidget;
		
		DGui::ActionManager m_actionManager;
		Core::ProjectManager *m_projectManager;
		Core::DatabaseManager *m_databaseManager;
		Core::Utils *m_utils;
		Core::SourceNavigator *m_sourceNavigator;
		Core::DebugManager *m_debugManager;
		Core::ScriptRunner *m_scriptRunner;
		
		View::Files *m_filesView;
		QDialog *m_waitDialog;
		
		QMenu *m_actions;
};

#endif


