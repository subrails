SOURCES += main.cpp \
           mainwindow.cpp \
           generatecontrollerdialog.cpp \
           generatemodeldialog.cpp \
           generatescaffolddialog.cpp \
           settingsdialog.cpp
TEMPLATE = app
CONFIG += warn_on \
	  thread \
          qt
RESOURCES = application.qrc
HEADERS += mainwindow.h \
generatecontrollerdialog.h \
generatemodeldialog.h \
generatescaffolddialog.h \
settingsdialog.h

FORMS += ui/generate_controller.ui ui/generate_model.ui ui/generate_scaffold.ui ui/settings_general.ui ui/generate_custom.ui
QT += xml gui core svg

LIBS += -ldgui -ldcore


IDEALITY_DIR = ../../3rdparty/ideality
include($$IDEALITY_DIR/ideality.pri)

TARGET = subrails
DESTDIR = ../../bin

SRC_DIR = ..
INCLUDEPATH += $$SRC_DIR

include($$SRC_DIR/core/core.pri)
include($$SRC_DIR/views/views.pri)

include($$SRC_DIR/common/common.pri)


include(../../config.pri)

INCLUDEPATH += $$BUILD_DIR/src/core $$SRC_DIR/core

INSTALLS += target
target.path = /bin

