/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "generatecontrollerdialog.h"
#include "ui_generate_controller.h"

#include <dgui/iconloader.h>

GenerateControllerDialog::GenerateControllerDialog(QWidget *parent)
 : QDialog(parent)
{
	ui = new Ui::GenerateControllerDialog;
	ui->setupUi(this);
	
	ui->buttonBox->button(QDialogButtonBox::Ok)->setAutoDefault(false);
	ui->buttonBox->button(QDialogButtonBox::Ok)->setDefault(false);
	ui->buttonBox->button(QDialogButtonBox::Cancel)->setAutoDefault(false);
	ui->buttonBox->button(QDialogButtonBox::Cancel)->setDefault(false);
	
	connect(ui->addAction, SIGNAL(clicked()), this, SLOT(addAction()));
	connect(ui->removeAction, SIGNAL(clicked()), this, SLOT(removeAction()));
	connect(ui->actionName, SIGNAL(returnPressed()), this, SLOT(addAction()));
	
	ui->addAction->setIcon(DGui::IconLoader::self()->load("list-add.svg"));
	ui->removeAction->setIcon(DGui::IconLoader::self()->load("list-remove.svg"));
}


GenerateControllerDialog::~GenerateControllerDialog()
{
	delete ui;
}


void GenerateControllerDialog::addAction()
{
	QString action = ui->actionName->text();
	
	if( !action.isEmpty() )
	{
		ui->actions->addItem(action);
	}
}

void GenerateControllerDialog::removeAction()
{
	if( QListWidgetItem *current = ui->actions->currentItem() )
	{
		delete current;
	}
}

QStringList GenerateControllerDialog::actions() const
{
	QStringList actions;
	for(int i = 0; i < ui->actions->count(); i++)
	{
		actions << ui->actions->item(i)->text();
	}
	
	return actions;
}

QString GenerateControllerDialog::controller() const
{
	return ui->controllerName->text();
}

