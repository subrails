/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "settingsdialog.h"

#include <QFileDialog>
#include <QSettings>
#include <QDir>

#include "common/global.h"

#include <dcore/applicationproperties.h>
#include <dgui/iconloader.h>
#include <dgui/keysequenceconfigurer.h>

#include <editor/setupprofile.h>

class Page : public QWidget
{
	public:
		Page(QWidget *parent = 0) : QWidget(parent)
		{
		}
		~Page() {}
};

SettingsDialog::SettingsDialog(Editor::Manager *const manager, QWidget *parent)
 : DGui::ConfigurationDialog(parent)
{
	QSettings *settings = Common::settings();
	
	Page *general = new Page;
	
	m_generalPage.setupUi(general );
	m_generalPage.chooseRubyButton->setIcon(DGui::IconLoader::self()->load("folder.svg"));
	connect(m_generalPage.chooseRubyButton, SIGNAL(clicked()), this, SLOT(onChooseRubyClicked()));
	
	m_generalPage.chooseRailsButton->setIcon(DGui::IconLoader::self()->load("folder.svg"));
	connect(m_generalPage.chooseRailsButton, SIGNAL(clicked()), this, SLOT(onChooseRailsClicked()));
	
	m_generalPage.chooseDirectoryButton->setIcon(DGui::IconLoader::self()->load("folder.svg"));
	connect(m_generalPage.chooseDirectoryButton, SIGNAL(clicked()), this, SLOT(onChooseDirectoryClicked()));
	
	m_generalPage.rubyPath->setText(settings->value("ruby-path", "ruby").toString());
	m_generalPage.railsPath->setText(settings->value("rails-path", "rails").toString());
	m_generalPage.working->setText(settings->value("working-directory", QDir::homePath()).toString());
	
	addPage(general, tr("General"), DGui::IconLoader::self()->load("preferences-system.svg"));
	
// 	general->setMinimumSize( 320, general->height() );
	
	m_keySequenceConfigurer = new DGui::KeySequenceConfigurer;
	
	addPage(m_keySequenceConfigurer, tr("Shortcuts"), DGui::IconLoader::self()->load("preferences-desktop-keyboard-shortcuts.svg"));
	
	m_profiles = new Editor::SetupProfile(manager);
	addPage(m_profiles, tr("Profiles"), DGui::IconLoader::self()->load("system-users.svg"));
	
	settings->endGroup();
}

SettingsDialog::~SettingsDialog()
{
}

Editor::Profile *SettingsDialog::currentProfile() const
{
	return m_profiles->currentProfile();
}


void SettingsDialog::addShortcutToConfigure(const QString &section, QAction *a)
{
	m_keySequenceConfigurer->addAction(section, a);
}


void SettingsDialog::apply()
{
	// Current page
	if( this->currentPageTitle() == tr("Profiles"))
	{
		m_profiles->saveProfiles();
	}
	else if( this->currentPageTitle() == tr("General"))
	{
		saveGeneral();
	}
	
	DGui::ConfigurationDialog::apply();
}

void SettingsDialog::ok()
{
	// All pages
	m_profiles->saveProfiles();
	saveGeneral();
	
	DGui::ConfigurationDialog::ok();
}

void SettingsDialog::saveGeneral()
{
	QSettings *settings = Common::settings();
	settings->setValue("ruby-path", m_generalPage.rubyPath->text() );
	settings->setValue("rails-path", m_generalPage.railsPath->text() );
	settings->setValue("working-directory", m_generalPage.working->text());
	
	settings->endGroup();
}

void SettingsDialog::onChooseRubyClicked()
{
	QString file = QFileDialog::getOpenFileName(this, tr("Choose the ruby binary path..."), m_generalPage.rubyPath->text() );
	
	if( !file.isNull() )
	{
		m_generalPage.rubyPath->setText(file);
	}
}

void SettingsDialog::onChooseRailsClicked()
{
	QString file = QFileDialog::getOpenFileName(this, tr("Choose the rails binary path..."), m_generalPage.railsPath->text() );
	
	if( !file.isNull() )
	{
		m_generalPage.railsPath->setText(file);
	}
}

void SettingsDialog::onChooseDirectoryClicked()
{
	QString file = QFileDialog::getExistingDirectory(this, tr("Choose your working directory..."), m_generalPage.working->text());
	
	if( !file.isNull() )
	{
		m_generalPage.working->setText(file);
	}
}
