/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "rake.h"

namespace Core {

struct Rake::Private
{
	QProcess *proc;
	
	QString command;
	
	QString error;
	QString output;
	
	bool failed;
};

Rake::Rake(QObject *parent)
	: Common::CoreBase(parent), d(new Private)
{
	d->command = "rake";
	
	d->proc = new QProcess(this);
	connect(d->proc, SIGNAL(readyReadStandardError()), this, SLOT(readError()));
	connect(d->proc, SIGNAL(readyReadStandardOutput()), this, SLOT(readOutput()));
	
	connect(d->proc, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(emitFinished(int, QProcess::ExitStatus)));
}


Rake::~Rake()
{
	d->proc->kill();
	d->proc->waitForFinished();
	
	delete d->proc;
	delete d;
}

void Rake::setProjectDir(const QString &dir)
{
	d->proc->setWorkingDirectory(dir);
}

void Rake::run(const QString &task, const QStringList &options)
{
	d->error = QString();
	d->output = QString();
	
	d->proc->start(d->command, QStringList() << task << options);
	d->proc->waitForStarted();
}

void Rake::run(const QString &ns, const QString &task, const QStringList &options)
{
	QString t = ns+":"+task;
	
	run(t, options);
}

void Rake::abortCurrentTask()
{
	if( d->proc->state() != QProcess::NotRunning )
	{
		d->proc->kill();
	}
}

void Rake::emitFinished(int exitCode, QProcess::ExitStatus /*state*/)
{
	d->failed = (exitCode == 0);
	emit finished();
}

void Rake::readError()
{
	d->error += d->proc->readAllStandardError();
}

void Rake::readOutput()
{
	d->output += d->proc->readAllStandardOutput();
}

QString Rake::error() const
{
	return d->error;
}

QString Rake::output() const
{
	return d->output;
}

bool Rake::failed() const
{
	return d->failed;
}

}


