/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef CORESOURCENAVIGATOR_H
#define CORESOURCENAVIGATOR_H

#include <QObject>
#include <QProcess>
#include <common/sr_export.h>
#include <common/corebase.h>

namespace View {
	class SourceNavigation;
}

namespace Editor {
	class CompletionEngine;
}

namespace Core {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class SR_EXPORT SourceNavigator : public Common::CoreBase
{
	Q_OBJECT;
	
	public:
		SourceNavigator(QObject *parent = 0);
		~SourceNavigator();
		
		void setProjectDir(const QString &dir);
		
		Editor::CompletionEngine *completer() const;
		
		void generateRubyTags(const QString &file);
		void generateProjectTags();
		
		View::SourceNavigation *view() const;
		
		QString rubyTagsFile() const;
		QStringList projectTagFiles() const;
		
	private:
		void generateTags(const QString &dir);
		void generateNext();
		
	private slots:
		void readError();
		
	protected slots:
		void onFinished(int code, QProcess::ExitStatus status);
		void generateTags();
		void updateTags(const QString &fileName);
		
	private:
		struct Private;
		Private *const d;
};

#endif

}
