/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "ui/indicatorviewer.h"

#include <dgui/indicatordrawer.h>
#include <QPainter>

IndicatorViewer::IndicatorViewer(QWidget *parent) : QWidget(parent)
{
	m_indicator = new DGui::IndicatorDrawer;
	startTimer(130);
	
}

IndicatorViewer::~IndicatorViewer()
{
	delete m_indicator;
}

void IndicatorViewer::timerEvent( QTimerEvent * event )
{
	m_indicator->advance();
	repaint();
}

void IndicatorViewer::paintEvent(QPaintEvent *event)
{
	QPainter p(this);
	p.setRenderHint(QPainter::Antialiasing);
	
	QPalette pal = palette();
	
	m_indicator->paint(&p, rect(), pal.highlight().color(), pal.background().color());
}

