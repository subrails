
#ifndef INDICATORVIEWER_H
#define INDICATORVIEWER_H

#include <QWidget>

namespace DGui {
class IndicatorDrawer;
}

class IndicatorViewer : public QWidget
{
	public:
		IndicatorViewer(QWidget *parent = 0);
		~IndicatorViewer();
		void paintEvent(QPaintEvent *event);
		void timerEvent(QTimerEvent *event);

	private:
		DGui::IndicatorDrawer *m_indicator;
};


#endif


