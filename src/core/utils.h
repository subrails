/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef COREUTILS_H
#define COREUTILS_H

#include <QObject>
#include <QStringList>
#include <common/sr_export.h>
#include <common/corebase.h>

namespace View {
	class Messages;
	class Server;
}

namespace Core {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class SR_EXPORT Utils : public Common::CoreBase
{
	Q_OBJECT
	public:
		Utils(QObject *parent = 0);
		~Utils();
		
		void setProjectDir(const QString &dir);
		View::Messages *genericView() const;
		View::Server *serverView() const;
		
		bool createProject(const QString &project, const QString &adapter, bool freeze = false);
		
		bool generateController(const QString &name, const QStringList &actions = QStringList());
		bool destroyController(const QString &name);
		
		bool generateModel(const QString &name, const QStringList &fields = QStringList());
		bool destroyModel(const QString &name);
		
		bool generateScaffold(const QString &model, const QString &controller = QString(), const QStringList &actions = QStringList());
		
		bool generateCustom(const QString &name, const QStringList &options);
		bool destroyCustom(const QString &name, const QStringList &options);
		
	public slots:
		void runApplication(const QString &controller = QString());
		void stopApplication();
		void launchBrowser();
		void abortCurrentTask();
		
	private slots:
		void readServerStandardOutput();
		void readServerStandardError();
		
		void readScriptStandardOutput();
		void readScriptStandardError();
		
		
		void emitScriptFinished(int code);
		
	signals:
		void projectCreated(const QString &location);
		void scriptFinished(bool ok);
		
	private:
		struct Private;
		Private *const d;
};

}

#endif
