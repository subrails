/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "scriptrunner.h"
#include <QProcess>

#include <common/global.h>

#include <dgui/osd.h>
#include <dcore/debug.h>

namespace Core {

struct ScriptRunner::Private
{
	QProcess *process;
	QString readed;
	QString projectDir;
	
	void execute(const QString &script);
};

void ScriptRunner::Private::execute(const QString &script)
{
	process->start(Common::rubyPath(),  QStringList() << projectDir+"/script/runner" << script);
	
	bool started = process->waitForStarted();
	
	if( !started )
	{
		DGui::Osd::self()->display(tr("Error while try to execute script/runner"), DGui::Osd::Error);
	}
}

ScriptRunner::ScriptRunner(QObject *parent)
 : Common::CoreBase(parent), d(new Private)
{
	d->process = new QProcess(this);
	
	d->process->setEnvironment(Common::rubyEnvironment());
	
	connect(d->process, SIGNAL(started()), this, SLOT(onProcessStarted()));
	connect(d->process, SIGNAL(finished(int)), this, SLOT(onProcessFinished(int)));
	connect(d->process, SIGNAL(readyReadStandardOutput()), this, SLOT(readStandardOutput()));
	connect(d->process, SIGNAL(readyReadStandardError()), this, SLOT(readStandardError()));
}


ScriptRunner::~ScriptRunner()
{
	delete d;
}

void ScriptRunner::setProjectDir(const QString &dir)
{
	d->projectDir = dir;
	d->process->setWorkingDirectory(dir);
}

void ScriptRunner::toXml(const QString &model)
{
	QString script = QString("puts %1.find(:all).to_xml").arg(model);
	d->execute(script);
}

void ScriptRunner::toXml(const QString &model, int id)
{
	QString script = QString("puts %1.find(%2).to_xml").arg(model).arg(id);
	d->execute(script);
}

QString ScriptRunner::lastOutput() const
{
	return d->readed;
}

void ScriptRunner::readStandardOutput()
{
	d->readed += d->process->readAllStandardOutput();
}

void ScriptRunner::readStandardError()
{
	D_SHOW_VAR(QString(d->process->readAllStandardError()));
}

void ScriptRunner::onProcessStarted()
{
	d->readed = "";
}

void ScriptRunner::onProcessFinished(int endcode)
{
	if( endcode == 0 )
		emit readed(d->readed);
	else
		emit failed();
}


}

