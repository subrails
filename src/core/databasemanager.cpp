/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "databasemanager.h"
#include "views/database/database.h"

#include "rake.h"
#include "ui_wait_dialog.h"

#include <dcore/debug.h>
#include <dgui/osd.h>

namespace Core {

struct DatabaseManager::Private
{
	View::Database *databaseView;
	QString projectDir;
	Rake *rake;
	bool restoringMigrate_hack;
};

DatabaseManager::DatabaseManager(QObject *parent)
 : Common::CoreBase(parent), d(new Private)
{
	d->databaseView = new View::Database;
	d->rake = new Rake(this);
	
	d->restoringMigrate_hack = false;
	
	connect(d->rake, SIGNAL(finished()), this, SLOT(rakeFinished()));
	connect(d->databaseView, SIGNAL(migrate()), this ,SLOT(rakeMigrate()));
}


DatabaseManager::~DatabaseManager()
{
	delete d;
}

void DatabaseManager::setProjectDir(const QString &dir)
{
	d->projectDir = dir;
	d->databaseView->setProjectDir(dir);
	
	d->rake->setProjectDir(dir);
}

View::Database *DatabaseManager::view() const
{
	return d->databaseView;
}

void DatabaseManager::rakeMigrate()
{
	if( !d->restoringMigrate_hack )
	{
		QDialog *dialog = new QDialog(d->databaseView);
		dialog->setAttribute(Qt::WA_DeleteOnClose, true);
		dialog->setModal(true);
		
		Ui::WaitDialog builder;
		builder.setupUi(dialog);
		
		
		connect(d->rake, SIGNAL(finished()), dialog, SLOT(accept()));
		connect(dialog, SIGNAL(rejected()), d->rake, SLOT(abortCurrentTask()));
		
		d->restoringMigrate_hack = true;
		
		dialog->show();
		
		d->rake->run("db:migrate", QStringList() << "VERSION=0");
	}
}

void DatabaseManager::rakeFinished()
{
	bool ok = d->rake->failed();
	
	if( d->restoringMigrate_hack )
	{
		d->rake->run("db:migrate");
		d->restoringMigrate_hack = false;
		return;
	}
	
	if( ! ok )
	{
		QStringList lines = d->rake->output().split("\n");
		
		QStringList::iterator it = lines.begin();
		
		while(it != lines.end())
		{
			if( *it == "rake aborted!")
			{
				QString error = *(it+1);
				
				DGui::Osd::self()->display(tr("Rake failed!\nError was:\n")+error, DGui::Osd::Error);
				break;
			}
			
			++it;
		}
	}
	else
	{
		DGui::Osd::self()->display(tr("Rake has finished successfully!"));
	}
}

}


