TEMPLATE = lib

CONFIG += staticlib \
warn_on
HEADERS += projectmanager.h \
utils.h \
databasemanager.h \
rake.h \
debugmanager.h \
sourcenavigator.h \
scriptrunner.h
SOURCES += projectmanager.cpp \
utils.cpp \
databasemanager.cpp \
rake.cpp \
ui/indicatorviewer.cpp \
debugmanager.cpp \
sourcenavigator.cpp \
scriptrunner.cpp
FORMS += ui/wait_dialog.ui

SRC_DIR = ..

DESTDIR = ../../lib

INCLUDEPATH += $$SRC_DIR
include($$SRC_DIR/common/common.pri)
include($$SRC_DIR/views.pri)

include(../../config.pri)
