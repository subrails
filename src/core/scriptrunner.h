/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef CORESCRIPTRUNNER_H
#define CORESCRIPTRUNNER_H

#include <QObject>
#include <common/corebase.h>

namespace Core {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class ScriptRunner : public Common::CoreBase
{
	Q_OBJECT
	public:
		ScriptRunner(QObject *parent = 0);
		~ScriptRunner();
		
		void setProjectDir(const QString &dir);
		QString lastOutput() const;
		
	public slots:
		void toXml(const QString &model);
		void toXml(const QString &model, int id);
		
	private slots:
		void readStandardOutput();
		void readStandardError();
		
		void onProcessStarted();
		void onProcessFinished(int endcode);
		
	signals:
		void readed(const QString &output);
		void failed();
		
	private:
		struct Private;
		Private *const d;
};

}

#endif
