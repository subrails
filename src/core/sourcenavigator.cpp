/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "sourcenavigator.h"

#include <QProcess>
#include <QFileInfo>
#include <QDateTime>
#include <QTimer>
#include <QDir>
#include <QQueue>
#include <QHash>
#include <QFileSystemWatcher>
#include <QTextBlock>
#include <QTextDocument>
#include <QThread>
#include <QSet>
#include <QTemporaryFile>

#include <common/global.h>
#include <views/navigation/sourcenavigation.h>

#include <dcore/debug.h>

#include <editor/completionengine.h>
#include <editor/languages/rubylanguagehandler.h>

namespace Core {


class DeepSearch : public QThread
{
	public:
		DeepSearch();
		~DeepSearch();
		
		void search(const QString &klass, const QStringList &projectTagFiles, const QString &rubyTagFile );
		
		QSet<QString> methods(const QString &klass, const QString &startsWith = QString()) const;
		
		void stop();
		
	protected:
		void run();
		
	private:
		struct Klass
		{
			QString name;
			QString scope;
			QSet<QString> base;
			QSet<QString> methods;
			QSet<QString> varmethods;
		};
		
		QHash<QString, Klass> m_classes;
		
		struct ParseInfo
		{
			QString uklass;
			QStringList tagFiles;
			QString rubyTagFile;
		};
		
		QQueue<ParseInfo> m_awaiting;
		
	private:
		inline Klass parseClass(const QString &klass);
		inline bool findInFile(const QString &filePath, const QString &scope, bool variable);
		
		void findInFiles(const ParseInfo &parseInfo);
		
		bool m_stop;
};

DeepSearch::DeepSearch() : m_stop(false)
{
}

DeepSearch::~DeepSearch()
{
	stop();
}

void DeepSearch::search(const QString &uklass, const QStringList &projectTagFiles, const QString &rubyTagFile )
{
	stop();
	
	ParseInfo info;
	info.uklass = uklass;
	info.tagFiles = projectTagFiles;
	info.rubyTagFile = rubyTagFile;
	
	m_awaiting.enqueue(info);
	
	if( !isRunning() )
	{
		start();
	}
}

QSet<QString> DeepSearch::methods(const QString &klass, const QString &startsWith) const
{
	dDebug() << "Methods for class: " << klass;
	Klass kinfo = m_classes.value(klass);
	
	bool filter = !startsWith.isEmpty();
	
	QSet<QString> methods;
	foreach( QString method, kinfo.methods + kinfo.varmethods)
	{
		if( filter )
		{
			if( method.startsWith(startsWith) )
			{
				methods << method;
			}
		}
		else
		{
			methods << method;
		}
	}
	
	foreach( QString base, kinfo.base)
	{
		foreach(QString method, this->methods(base) )
		{
			if( method.startsWith(startsWith) )
			{
				methods << method;
			}
		}
	}
	
	return methods;
}

void DeepSearch::stop()
{
	m_stop = true;
}

void DeepSearch::run()
{
	m_stop = false;
	while( !m_awaiting.isEmpty() )
	{
		if( m_stop ) break;
		
		ParseInfo info = m_awaiting.dequeue();
		
		findInFiles(info);
	}
}

void DeepSearch::findInFiles(const ParseInfo &parseInfo)
{
	if( m_stop ) return;
	Klass klassInfo = parseClass(parseInfo.uklass);
	
	bool parsed = m_classes.contains(klassInfo.scope);
	if( parsed )
	{
		m_classes[klassInfo.scope].varmethods.clear();
	}
	else
	{
		m_classes[klassInfo.scope] = klassInfo;
	}
	
	QStringList tagFilesC = parseInfo.tagFiles;
	
	bool ok = false;
	while( ! tagFilesC.isEmpty() )
	{
		if( m_stop ) break;
		
		QString filePath = tagFilesC.takeLast();
		ok = ok || findInFile(filePath, klassInfo.scope, true);
	}
	
	if( !ok && !parsed )
	{
		ok = findInFile(parseInfo.rubyTagFile, klassInfo.scope, false);
	}
	
	foreach(QString base, m_classes[klassInfo.scope].base)
	{
		if( m_stop ) break;
		ParseInfo info;
		info.uklass = base;
		info.tagFiles = parseInfo.tagFiles;
		info.rubyTagFile = parseInfo.rubyTagFile;
		
		findInFiles(info);
	}
	
}

bool DeepSearch::findInFile(const QString &filePath, const QString &scope, bool variable)
{
	QFile file(filePath);
	
	bool ok = file.open(QIODevice::ReadOnly | QIODevice::Text );
	if( ok )
	{
		QString nativeScope = "::"+scope;
		
		QTextStream ts(&file);
		
		QRegExp entity("^(module|class)\\s(\\w+)\\s([\\w:]+)(#\\w+){0,1}\\s(\\d+),(\\d+)");
		QRegExp method("^def\\s(\\w+)");
		QRegExp filerx("^(.+),\\d+$");
		
		QRegExp inherits("<\\s*([\\w:]+)");
		
		bool readingMethods = false;
		QString currentFile;
		
		QSet<QString> methods;
		
		while( !ts.atEnd() )
		{
			if ( m_stop ) return false;
			
			QString line = ts.readLine();
			
			if( entity.indexIn(line) > -1 )
			{
				if( readingMethods )
				{
					readingMethods = false;
					continue;
				}
				
				QString container = entity.cap(1);
				
				if( container == "class" )
				{
					if( entity.cap(2) == m_classes[scope].name )
					{
						if( entity.cap(3) != nativeScope) continue;
						
						QFile classFile(currentFile);
						if( classFile.open(QIODevice::ReadOnly) )
						{
							int pos = entity.cap(6).toInt();
							classFile.seek(pos);
							QString classLine = classFile.readLine();
							
							if( inherits.indexIn(classLine) > -1 )
							{
								QString baseClass = inherits.cap(1);
								
								if( baseClass.startsWith("::") )
								{
									baseClass.remove(0, 2);
								}
								
								if( !m_classes[scope].base.contains(baseClass) )
								{
									m_classes[scope].base << baseClass;
								}
							}
							
							classFile.close();
						}
						
						readingMethods = true;
					}
				}
			}
			else if( readingMethods )
			{
				if( method.indexIn(line) > -1 )
				{
					methods << method.cap(1);
				}
			}
			else if( filerx.indexIn(line) > -1 )
			{
				currentFile = filerx.cap(1);
			}
		}
		
		file.close();
		
		ok = !methods.isEmpty();
		if( ok )
		{
			foreach(QString method, methods)
			{
				if ( m_stop ) return false;
				
				if( variable )
				{
					m_classes[scope].varmethods << method;
				}
				else
				{
					m_classes[scope].methods << method;
				}
			}
		}
	}
	else
	{
		dWarning() << "Cannot open file: " << filePath;
	}
	
	return ok;
}

DeepSearch::Klass DeepSearch::parseClass(const QString &klass)
{
	Klass  k;
	QStringList splitted = klass.split("::");
	k.name = splitted.last();
	
	k.scope = splitted.join("::");
	
	return k;
}

class Completer : public Editor::CompletionEngine
{
	Q_OBJECT;
	
	public:
		Completer(SourceNavigator *navigator);
		~Completer();
		
		void setProjectDir(const QString &projectDir);
		
		void setTagFiles(const QStringList &tagFiles);
		
		void stop();
		
	protected:
		void emitCompletionList(const QString &word);
		
		void findClass(const QString &var);
		
	private slots:
		void tryNaiveWay();
		void tryDeepWay();
		
		void onProcessFinished(int code, QProcess::ExitStatus);
		void readError();
		void readOutput();
		
		void onDeepSearchFinish();
		
	private:
		QProcess m_process;
		QString m_class;
		QString m_filter;
		QString m_additionalCode;
		QString m_projectDir;
		SourceNavigator *m_navigator;
		
		bool m_basicSearch;
		DeepSearch m_deepSearcher;
		
		QStringList m_tagFiles;
};

Completer::Completer(SourceNavigator *navigator) : m_navigator(navigator), m_basicSearch(false)
{
	connect(&m_process, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onProcessFinished(int, QProcess::ExitStatus)));
	connect(&m_process, SIGNAL(readyReadStandardError()), this, SLOT(readError()));
	connect(&m_process, SIGNAL(readyReadStandardOutput()), this, SLOT(readOutput()));
	
	connect(&m_deepSearcher, SIGNAL(finished()), this, SLOT(onDeepSearchFinish()));
}

Completer::~Completer()
{
	m_process.terminate();
	if( !m_process.waitForFinished() )
		m_process.kill();
	
	m_deepSearcher.stop();
}

void Completer::setProjectDir(const QString &projectDir)
{
	m_process.setEnvironment(Common::rubyEnvironment("", projectDir+"/config"));
	m_projectDir = projectDir;
}

void Completer::setTagFiles(const QStringList &tagFiles)
{
	m_tagFiles = tagFiles;
}

void Completer::stop()
{
	m_deepSearcher.stop();
}

void Completer::emitCompletionList(const QString &word)
{
	// First try with naive way
	findClass(word);
	
	D_SHOW_VAR(m_class);
	
	tryNaiveWay();
}

/**
 * TODO:
 * - Evaluar que parametros recibe, el numero de parametros q recibe es importante
 * - Capturar los require y pasarlos como parametro al interprete por medio de -r
 * - Verificar si la clase se encuentra entre las clases de la aplicacion
 **/
void Completer::findClass(const QString &var)
{
	D_SHOW_VAR(var);
	m_class = QString();
	m_filter = QString();
	
	QTextBlock block = document()->begin();
	
	switch(operation())
	{
		case CompletionEngine::FindObjectMethods:
		{
			QRegExp require("^\\s*require\\s+\\\'\\w*\\\'\\s*");
			
			QRegExp assign(var+"\\s+=\\s+(\\w+).new");
			
			QRegExp string(var+"\\s+=\\s+(\\\"|\\\'|%)");
			
			QRegExp array(var+"\\s+=\\s+\\[");
			QRegExp hash(var+"\\s+=\\s+\\{");
			
			QRegExp include("\\s*include\\s+(.*)\\s*");
			
			int defstack = 0;
			int contstack = 0;
			
			QString module;
			
			while( block.isValid() )
			{
				QString blockText = block.text();
				
				foreach( QString line, blockText.split(QRegExp("$|;")) )
				{
					if( line.trimmed().isEmpty() ) continue;
					
					if( include.indexIn(line) > -1 && contstack > 0 )
					{
						module = include.cap(1);
					}
					else if( assign.indexIn(line) > -1 )
					{
						m_class = assign.cap(1);
					}
					else if( string.indexIn(line) > -1 )
					{
						m_class = "String";
					}
					else if( array.indexIn(line) > -1 )
					{
						m_class = "Array";
					}
					else if( hash.indexIn(line) > -1 )
					{
						m_class = "Hash";
					}
					
					if( !m_class.isEmpty() && !module.isEmpty() )
					{
						m_class = module + "::" + m_class;
						module = QString();
					}
					
					if( require.indexIn(line) > -1 )
					{
						m_additionalCode += line+"\n";
					}
					else if( Editor::RubyLanguageHandler::isDefinition(line) )
					{
						if( Editor::RubyLanguageHandler::isContainer(line) )
						{
							contstack++;
						}
						else
						{
							defstack++;
						}
						
						m_additionalCode += line+"\n";
					}
					else if ( Editor::RubyLanguageHandler::isCloseDefinition(line) )
					{
						if( defstack == 0 )
						{
							contstack--;
						}
						else
						{
							defstack--;
						}
						
						m_additionalCode += line+"\n";
					}
					else if( contstack > 0 )
					{
						m_additionalCode += line+"\n";
					}
				}
				
				block = block.next();
			}
		}
		break;
		case CompletionEngine::FindScopeMethods:
		{
			QRegExp container("^\\s*(class|module)\\s+(\\w+)");
			
			QTextBlock block = currentBlock();
			
			int defstack = 0;
			
			while( block.isValid() )
			{
				QString text = block.text();
				
				if( Editor::RubyLanguageHandler::isDefinition(text) )
				{
					if( container.indexIn(text) > -1 )
					{
// 						if( defstack <= 0 )
						{
							m_filter = var;
							m_class = container.cap(2);
							break;
						}
					}
					defstack++;
				}
				else if( Editor::RubyLanguageHandler::isCloseDefinition(text) )
				{
					defstack--;
				}
				
				block = block.previous();
			}
			
		}
		break;
	}
}

void Completer::tryNaiveWay()
{
	if( !m_class.isEmpty() )
	{
		QString dummyCode = QString("dummy = %1.new; puts dummy.public_methods").arg(m_class);
		
		m_basicSearch = true;
		m_process.start(Common::rubyPath(), QStringList() << "-e" << m_additionalCode +";"+ dummyCode);
		m_additionalCode = "";
	}
}

void Completer::tryDeepWay()
{
	if( !m_class.isEmpty() )
	{
		m_deepSearcher.search(m_class, m_navigator->projectTagFiles(), m_navigator->rubyTagsFile());
	}
}

void Completer::onProcessFinished(int code, QProcess::ExitStatus)
{
	if( code == 0 )
	{
	}
	else
	{
		if( m_basicSearch )
		{
			// Search in tags
			QTimer::singleShot(0, this, SLOT(tryDeepWay()));
		}
	}
}

void Completer::readError()
{
	// Failed
	qDebug(m_process.readAllStandardError());
}

void Completer::readOutput()
{
	// Success
	QString readed = m_process.readAllStandardOutput();
	
	QSet<QString> set;
	foreach(QString m, readed.split('\n'))
	{
		set << m;
	}
	
	emit completionList(set);
}

void Completer::onDeepSearchFinish()
{
	emit completionList(m_deepSearcher.methods(m_class, m_filter));
}

struct SourceNavigator::Private
{
	QString projectDir;
	QProcess *script;
	QString ruby;
	
	View::SourceNavigation *navigationView;
	
	QString rubyTagsFile;
	
	bool generatingRubyTags;
	QQueue<QString> waitsForTag;
	
	QString projectTagsDir;
	
	QHash<QString, QString> tags;
	QFileSystemWatcher watcher;
	QString currentTag;
	
	Completer *completer;
	
	QString lastError;
	
	void cleanFile(const QString &path)
	{
		QTemporaryFile temp(QDir::tempPath()+"/subrails_XXXXX");
		temp.setAutoRemove(false);
		
		if( temp.open() )
		{
			bool ok = false;
			{
				QFile file(path);
				ok = file.open(QIODevice::ReadOnly);
				if( ok )
				{
					QTextStream rts(&file);
					QTextStream wts(&temp);
					
					while( !rts.atEnd() )
					{
						QString dirtyLine = rts.readLine();
						QString line;
						
						for(int i = 0; i < dirtyLine.count(); i++)
						{
							QChar c = dirtyLine[i];
							
							switch(c.cell())
							{
								case 127:
								case 1:
								{
									line += " ";
								}
								break;
								case 0:
								case 10:
								case 12:
								{
								}
								break;
								default:
								{
									line += c;
								}
								break;
							}
						}
						
						line = line.simplified();
						
						if( !line.isEmpty() )
						{
							wts << line << endl;
						}
					}
					file.close();
				}
			}
			
			QString fname = temp.fileName();
			
			temp.close();
			if( ok )
			{
				QFile::remove(path);
				QFile::copy(fname, path);
			}
			temp.remove();
		}
	}
};

SourceNavigator::SourceNavigator(QObject *parent)
 : Common::CoreBase(parent), d(new Private)
{
	d->generatingRubyTags = false;
	
	d->rubyTagsFile = CONFIG_DIR+"/RUBY_TAGS";
	
	d->script = new QProcess(this);
	d->script->setEnvironment(Common::rubyEnvironment());
	
	d->navigationView = new View::SourceNavigation;
	
	connect(d->script, SIGNAL(finished( int, QProcess::ExitStatus)), this, SLOT(onFinished(int, QProcess::ExitStatus)));
	connect(d->script, SIGNAL(readyReadStandardError()), this, SLOT(readError()));
	
	connect(&d->watcher, SIGNAL(fileChanged( const QString &)), this, SLOT(updateTags(const QString &)));
	
	d->completer = new Completer(this);
	
	generateRubyTags(d->rubyTagsFile);
}


SourceNavigator::~SourceNavigator()
{
	d->completer->stop();
	d->script->terminate();
	if( ! d->script->waitForFinished() )
		d->script->kill();
	
	if (d->generatingRubyTags )
	{
		QFile::remove(d->rubyTagsFile);
	}
	
	delete d->script;
	delete d;
}

void SourceNavigator::setProjectDir(const QString &dirPath)
{
	d->projectDir = dirPath;
	
	d->completer->blockSignals(true);
	d->completer->stop();
	d->completer->blockSignals(false);
	d->navigationView->clear();
	
	if( !dirPath.isEmpty() )
	{
		d->completer->setProjectDir(dirPath);
		d->script->setWorkingDirectory(dirPath);
		
		d->projectTagsDir = dirPath+"/tags";
		
		QDir dir(d->projectTagsDir);
		if( !dir.exists(d->projectTagsDir) )
		{
			dir.mkdir(d->projectTagsDir);
		}
		
		QTimer::singleShot(100, this, SLOT(generateTags()));
	}
}

Editor::CompletionEngine *SourceNavigator::completer() const
{
	return d->completer;
}

void SourceNavigator::generateRubyTags(const QString &file)
{
	if( d->generatingRubyTags ) return;
	
	QFileInfo finfo(file);
	
	bool createRubyTags = !finfo.exists();
	
	if( !createRubyTags )
	{
		int days = finfo.lastModified().daysTo(QDateTime::currentDateTime());
		
		if( days > 15 ) // Generate it every 15 days
		{
			createRubyTags = true;
		}
	}
	
	if(createRubyTags && d->script->state() == QProcess::NotRunning)
	{
		d->generatingRubyTags = true;
		d->lastError = QString();
		d->script->start(Common::rubyPath(), QStringList() << DATA_DIR+"/tools/ruby_tags.rb" << file);
		d->script->waitForStarted();
	}
	else
	{
		generateProjectTags();
	}
}

QString SourceNavigator::rubyTagsFile() const
{
	return d->rubyTagsFile;
}

QStringList SourceNavigator::projectTagFiles() const
{
	return d->tags.values();
}

void SourceNavigator::generateProjectTags()
{
	if( d->projectDir.isEmpty() ) return;
	
	d->watcher.blockSignals(true);
	
	generateTags(d->projectDir+"/app");
	generateTags(d->projectDir+"/lib");
	
	generateNext();
}

void SourceNavigator::generateNext()
{
	QString file = d->waitsForTag.dequeue();
	
	QFileInfo finfo(file);
	QDir dir = finfo.dir();
	
	d->script->setWorkingDirectory(dir.absolutePath());
	
	QString saveIn = d->projectTagsDir+"/"+dir.dirName()+"_"+finfo.fileName();
	
	if( QFile::exists(saveIn) )
	{
		QFile::remove(saveIn);
	}
	
	d->currentTag = saveIn;
	
	d->tags.insert(file, saveIn);
	d->watcher.addPath(file);
	
	d->lastError = QString();
	d->script->start(Common::rubyPath(), QStringList() << DATA_DIR+"/tools/rtags/rtags.rb" << "-f" << saveIn << file );
	d->script->waitForStarted();
	
}

void SourceNavigator::generateTags(const QString &dirPath)
{
	QDir dir(dirPath);
	
	QList<QFileInfo> list = dir.entryInfoList();
	foreach(QFileInfo finfo, list)
	{
		if( !finfo.fileName().startsWith(".") && finfo.isDir() )
		{
			generateTags(finfo.absoluteFilePath());
		}
		else if ( finfo.fileName().endsWith(".rb", Qt::CaseInsensitive) )
		{
			d->waitsForTag.enqueue(finfo.absoluteFilePath());
		}
	}
}


View::SourceNavigation *SourceNavigator::view() const
{
	return d->navigationView;
}

void SourceNavigator::readError()
{
	d->lastError += d->script->readAllStandardError();
}

void SourceNavigator::onFinished(int code, QProcess::ExitStatus)
{
	D_FUNCINFO;
	if( code == 0 )
	{
		if( d->generatingRubyTags && !d->projectDir.isEmpty())
		{
			// Cleanup tag file
			d->cleanFile(d->rubyTagsFile);
			d->generatingRubyTags = false;
			this->generateProjectTags();
		}
		else
		{
			if( QFile::exists(d->currentTag) )
			{
				d->cleanFile(d->currentTag);
				d->navigationView->loadTags(d->currentTag);
			}
			
			if( !d->waitsForTag.isEmpty() )
			{
				generateNext();
			}
			else
			{
				d->script->setWorkingDirectory(d->projectDir);
				d->watcher.blockSignals(false);
				
				d->currentTag = QString();
			}
		}
	}
	else
	{
		qWarning("Error while try to execute tag generator, error was: %s", qPrintable(d->lastError));
		dError() << "Environment: " << d->script->environment();
	}
}

void SourceNavigator::generateTags()
{
	///////////////////
	
	
	if( !d->generatingRubyTags )
	{
		this->generateRubyTags(d->rubyTagsFile);
	}
	else
	{
		generateProjectTags();
	}
}

void SourceNavigator::updateTags(const QString &fileName)
{
	D_SHOW_VAR(fileName);
	
	if( d->tags.contains(fileName ) )
	{
		if( ! QFile::exists(fileName) )
		{
			d->navigationView->removeTags(fileName);
		}
		else
		{
			d->waitsForTag.enqueue( fileName );
			generateNext();
		}
	}
}

}


#include "sourcenavigator.moc"

