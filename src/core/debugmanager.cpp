/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "debugmanager.h"

#include <QFileSystemWatcher>
#include <QProcess>

#include <views/debugger/debugger.h>

#include <common/global.h>

#include <dcore/debug.h>

namespace Core {

struct DebugManager::Private
{
	Private() : script(0), dirtyContents(false) {}
	~Private()
	{
		delete script;
	}
	
	QString ruby;
	
	QProcess *script;
	View::Debugger *debuggerView;
	
	QFileSystemWatcher watcher;
	QString currentFile;
	
	bool dirtyContents;
};

DebugManager::DebugManager(QObject *parent) : QObject(parent), d(new Private)
{
	d->script = new QProcess(this);
	d->script->setEnvironment(Common::rubyEnvironment());
	
	connect(d->script, SIGNAL(readyReadStandardOutput()), this, SLOT(readOutput()));
	connect(d->script, SIGNAL(readyReadStandardError()), this, SLOT(readError()));
	
	d->debuggerView = new View::Debugger;
	
	connect(&d->watcher, SIGNAL(fileChanged(const QString &)), this, SLOT(checkSyntax(const QString &)));
}


DebugManager::~DebugManager()
{
	d->script->terminate();
	if( ! d->script->waitForFinished() )
		d->script->kill();
	
	delete d;
}

void DebugManager::checkSyntax(const QString &file)
{
	if( ! file.endsWith(".rb", Qt::CaseInsensitive )) return;
	
	d->watcher.addPath(file);
	d->currentFile = file;
	
	if( d->script->state() != QProcess::NotRunning )
	{
		d->script->kill();
		d->script->waitForFinished();
	}
	
	d->dirtyContents = true;
	d->script->start(Common::rubyPath(), QStringList() << "-c" << file);
	if( ! d->script->waitForStarted() )
	{
		d->debuggerView->appendErrorToSyntax(tr("Invalid ruby binary path!"));
	}
}

View::Debugger *DebugManager::view() const
{
	return d->debuggerView;
}

void DebugManager::readOutput()
{
	if( d->dirtyContents )
	{
		d->debuggerView->clearSyntax();
		d->dirtyContents = false;
	}
	
	QString readed = d->script->readAllStandardOutput();
	d->debuggerView->appendOutputToSyntax( readed);
}

void DebugManager::readError()
{
	if( d->dirtyContents )
	{
		d->debuggerView->clearSyntax();
		d->dirtyContents = false;
	}
	
	QString readed = d->script->readAllStandardError();
	d->debuggerView->appendErrorToSyntax( readed);
}


}


