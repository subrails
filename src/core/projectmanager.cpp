/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "projectmanager.h"
#include <common/viewbase.h>

#include <views/project/project.h>

#include <dcore/debug.h>

#include <QDir>

#include <common/global.h>

namespace Core {

struct ProjectManager::Private
{
	View::Project *projectView;
	
	QString projectName;
	QString projectDir;
};

ProjectManager::ProjectManager(QObject *parent)
	: Common::CoreBase(parent), d(new Private)
{
	d->projectView = new View::Project;
}


ProjectManager::~ProjectManager()
{
	delete d;
}

void ProjectManager::setProjectDir(const QString &dir)
{
	openProjectFrom(dir);
}

bool ProjectManager::openProjectFrom(const QString &dir)
{
	dDebug() << "Opening project from: " << dir;
	QDir projectDir(dir);
	if( isValidProjectDir(projectDir) )
	{
		d->projectName = projectDir.dirName();
		
		d->projectView->setProjectDir(dir);
		d->projectDir = dir;
		
		emit projectOpened(dir);
		
		return true;
	}
	
	return false;
}

void ProjectManager::closeProject()
{
	d->projectName = QString();
	d->projectView->setProjectDir(QString());
	d->projectDir = QString();
}

View::Project *ProjectManager::view() const
{
	return d->projectView;
}

QStringList ProjectManager::models() const
{
	return d->projectView->models();
}

bool ProjectManager::isValidProjectDir(const QDir &dir)
{
	if( dir.exists() )
	{
		QStringList files = QStringList() << "app" <<  "config" <<  "db" <<  "lib" <<  "public" <<  "script" <<  "vendor" << "app/controllers" << "app/helpers" << "script/generate" << "script/destroy" << "script/plugin" << "script/server";
		
		foreach(QString file, files)
		{
			if( ! dir.exists(file) )
			{
				dFatal() << "Missing file or directory: " << file;
				return false;
			}
		}
		
		return true;
	}
	
	return false;
}

QString ProjectManager::projectDir() const
{
	return d->projectDir;
}

void ProjectManager::update()
{
	d->projectView->refresh();
}

void ProjectManager::perspectiveChanged(int wps)
{
	d->projectView->perspectiveChanged(wps);
}

}



