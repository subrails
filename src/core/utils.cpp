/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "utils.h"

#include <QDesktopServices>
#include <QProcess>
#include <QUrl>
#include <QMessageBox>
#include <QTimer>
#include <QtDebug>

#include <views/messages/messages.h>
#include <views/messages/server.h>

#include <dcore/debug.h>
#include <dgui/osd.h>

#include <common/global.h>

namespace Core {

struct Utils::Private
{
	View::Messages *messagesView;
	View::Server *serverView;
	
	QString ruby;
	QString rails;
	int port;
	
	QString projectDir;
	
	QProcess *server;
	QString lastController;
	
	QProcess *script;
	
	bool creatingProject;
	
	bool runScript(const QString &program, const QStringList &arguments);
};

bool Utils::Private::runScript(const QString &program, const QStringList &arguments)
{
	script->start(program, arguments);
	
	bool started = script->waitForStarted();
	
	if( !started )
	{
		DGui::Osd::self()->display(tr("Error running command: ")+"<b>"+program+"</b>", DGui::Osd::Error);
	}
	
	return started;
}

Utils::Utils(QObject *parent)
 : Common::CoreBase(parent), d(new Private)
{
	d->creatingProject = false;
	
	d->port = 3000;
	
	d->messagesView = new View::Messages;
	d->serverView = new View::Server;
	
	d->server = new QProcess(this);
// 	d->server->setProcessChannelMode(QProcess::ForwardedChannels);
	connect(d->server, SIGNAL(readyReadStandardOutput()), this, SLOT(readServerStandardOutput()));
	connect(d->server, SIGNAL(readyReadStandardError()), this, SLOT(readServerStandardError()));
	
	
	d->script = new QProcess(this);
	connect(d->script, SIGNAL(finished(int)), this, SLOT(emitScriptFinished(int)));
	connect(d->script, SIGNAL(readyReadStandardOutput()), this, SLOT(readScriptStandardOutput()));
	connect(d->script, SIGNAL(readyReadStandardError()), this, SLOT(readScriptStandardError()));
	
	// Configure environment
	
	QStringList env = Common::rubyEnvironment();
	
	
	d->script->setEnvironment(env);
	d->server->setEnvironment(env);
}


Utils::~Utils()
{
	d->server->kill();
	d->server->waitForFinished();
	
	delete d->server;
	
	d->script->kill();
	d->script->waitForFinished();
	delete d->script;
	
	delete d;
}

View::Messages *Utils::genericView() const
{
	return d->messagesView;
}

View::Server *Utils::serverView() const
{
	return d->serverView;
}

void Utils::setProjectDir(const QString &dir)
{
	d->projectDir = dir;
	d->server->setWorkingDirectory(dir);
	d->script->setWorkingDirectory(dir);
}

void Utils::runApplication(const QString &controller)
{
	d->lastController = controller;
	
	if( d->server->state() == QProcess::Running )
	{
		launchBrowser();
		return;
	}
	
	QString rubyPath = Common::rubyPath();
	
	d->server->start(rubyPath, QStringList() << d->projectDir+"/script/server" << "-p" << QString::number(d->port));
	
	if( ! d->server->waitForStarted() )
	{
		DGui::Osd::self()->display(tr("Error running: ")+"<b>"+rubyPath+"</b>", DGui::Osd::Error);
	}
}

void Utils::stopApplication()
{
	if( d->server->state() == QProcess::Running )
	{
		d->server->terminate();
		if( !d->server->waitForFinished(500) )
		{
			d->server->kill();
		}
	}
}

void Utils::launchBrowser()
{
	QDesktopServices::openUrl(QString("http://localhost:%1/").arg(d->port)+d->lastController);
}

void Utils::abortCurrentTask()
{
	if( d->script->state() != QProcess::NotRunning )
	{
		d->script->kill();
	}
}

void Utils::readServerStandardOutput()
{
	QString readed = d->server->readAllStandardOutput();
	
	QRegExp started("Rails application started");
	foreach(QString line, readed.split('\n'))
	{
		if( !line.trimmed().isEmpty() )
		{
			d->serverView->appendOutput( line );
			
			if( started.indexIn(line) > -1 )
			{
				launchBrowser();
			}
		}
	}
}

void Utils::readServerStandardError()
{
	QString readed = d->server->readAllStandardError();
	foreach(QString line, readed.split('\n'))
	{
		if( !line.trimmed().isEmpty() )
		{
			d->serverView->appendError( line );
		}
	}
}


void Utils::readScriptStandardOutput()
{
	QString readed = d->script->readAllStandardOutput();
	
	QRegExp question("^overwrite ([\\w\\W]+)\\?");
	
	foreach(QString line, readed.split("\n"))
	{
		if( question.indexIn(line) > -1 )
		{
			int answer = QMessageBox::question(0, tr(""), tr("Overwrite \"")+question.cap(1)+ "\"?", QMessageBox::Yes | QMessageBox::No | QMessageBox::YesToAll | QMessageBox::Cancel);
			
			switch( answer )
			{
				case QMessageBox::Yes:
				{
					d->script->write(QByteArray("y\n"));
				}
				break;
				case QMessageBox::YesToAll:
				{
					d->script->write(QByteArray("a\n"));
				}
				break;
				case QMessageBox::No:
				{
					d->script->write(QByteArray("n\n"));
				}
				break;
				case QMessageBox::Cancel:
				{
					d->script->write(QByteArray("q\n"));
				}
				break;
			}
		}
		else
			d->messagesView->appendOutput( line );
	}
}

void Utils::readScriptStandardError()
{
	d->messagesView->appendError(d->script->readAllStandardError());
}

void Utils::emitScriptFinished(int code)
{
	bool ok = (code == 0);
	
	if( d->creatingProject && !d->projectDir.isEmpty() )
	{
		setProjectDir(d->projectDir);
		
		emit projectCreated(d->projectDir);
		d->creatingProject = false;
	}
	
	emit scriptFinished(ok);
}

bool Utils::createProject(const QString &project, const QString &adapter, bool freeze)
{
	QStringList options;
	
	options << project;
	
	if( !adapter.isEmpty() )
	{
		options << "--database="+adapter;
	}
	if( freeze )
	{
		options << "-f";
	}
	
	
	d->creatingProject = d->runScript(Common::railsPath(), options);
	
	if( d->creatingProject )
	{
		d->projectDir = project;
	}
	
	return d->creatingProject;
}

bool Utils::generateController(const QString &name, const QStringList &actions)
{
	return d->runScript(Common::rubyPath(), QStringList() << d->projectDir+"/script/generate" << "controller" << name << actions);
}

bool Utils::destroyController(const QString &name)
{
	return d->runScript(Common::rubyPath(), QStringList() << d->projectDir+"/script/destroy" << "controller" << name);
}

bool Utils::generateModel(const QString &name, const QStringList &fields )
{
	return d->runScript(Common::rubyPath(), QStringList() << d->projectDir+"/script/generate" << "model" << name << fields);
}

bool Utils::destroyModel(const QString &name)
{
	return d->runScript(Common::rubyPath(), QStringList() << d->projectDir+"/script/destroy" << "model" << name);
}

bool Utils::generateScaffold(const QString &model, const QString &controller, const QStringList &actions)
{
	QStringList options = QStringList() << d->projectDir+"/script/generate" << "scaffold" << model;
	
	if ( !controller.isEmpty() )
	{
		options << controller << actions;
	}
	
	return d->runScript(Common::rubyPath(), options );
}

bool Utils::generateCustom(const QString &name, const QStringList &options)
{
	return d->runScript(Common::rubyPath(), QStringList() << d->projectDir+"/script/generate" << name << options);
}

bool Utils::destroyCustom(const QString &name, const QStringList &options)
{
	return d->runScript(Common::rubyPath(), QStringList() << d->projectDir+"/script/destroy" << name << options);
}

}


