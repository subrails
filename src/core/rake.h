/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef CORERAKE_H
#define CORERAKE_H

#include <QObject>
#include <QStringList>
#include <common/sr_export.h>
#include <common/corebase.h>

#include <QProcess>

namespace Core {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class SR_EXPORT Rake : public Common::CoreBase
{
	Q_OBJECT
	public:
		Rake(QObject *parent = 0);
		~Rake();
		
		void setProjectDir(const QString &dir);
		
		void run(const QString &task, const QStringList &options = QStringList());
		void run(const QString &ns, const QString &task, const QStringList &options = QStringList());
		
		QString error() const;
		QString output() const;
		
		bool failed() const;
		
	public slots:
		void abortCurrentTask();
		
	private slots:
		void emitFinished(int exitCode, QProcess::ExitStatus state);
		void readError();
		void readOutput();
		
	signals:
		void finished();
		
	private:
		struct Private;
		Private *const d;
};

}

#endif
