/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "global.h"

#include <QProcess>
#include <QDir>
#include <QApplication>

#include <dcore/globaldeleter.h>
#include <dcore/debug.h>

static DCore::GlobalDeleter<QSettings> deleter;

namespace Common {

static QSettings *s_settings = 0;

QStringList rubyEnvironment(const QString &appendOpt, const QString &appendLib)
{
	QProcess dummy;
	
	QStringList env = dummy.environment();
	QString currentOpts;
	QString currentLib;
	
	bool envHasHOME = false;
	
	QRegExp rxopts("RUBYOPT=(.*)");
	QRegExp rxlib("RUBYLIB=(.*)");
	
	QStringList::iterator  it = env.begin();
	while(it != env.end() )
	{
		if(rxopts.indexIn(*it) > -1 )
		{
			currentOpts = rxopts.cap(1);
			it = env.erase(it);
		}
		else if( rxlib.indexIn(*it) > -1 )
		{
			currentLib = rxlib.cap(1);
			D_SHOW_VAR(currentLib);
			it = env.erase(it);
		}
		else if ( (*it).startsWith("HOME=" ) )
		{
			envHasHOME = true;
		}
		
		if( !currentLib.isEmpty() && !currentOpts.isEmpty() )
		{
			break;
		}
		
		++it;
	}
	
#ifdef Q_OS_WIN32
	env << QDir::toNativeSeparators("RUBYLIB="+currentLib+";"+DATA_DIR+"/tools:"+appendLib);
#else
	env << "RUBYLIB="+currentLib+":"+DATA_DIR+"/tools:"+appendLib;
#endif
	env << "RUBYOPT="+currentOpts+" -rhackio.rb "+appendOpt;
	
	if( !envHasHOME)
	{
		env << "HOME="+QDir::homePath();
	}
	
	return env;
}

QString rubyPath()
{
	QString path = "ruby";
	
	QSettings *sets = settings();
	path = sets->value("ruby-path", path).toString();
	
	return path;
}

QString workingDirectory()
{
	QString path = QDir::homePath();
	
	QSettings *sets = settings();
	path = sets->value("working-directory", path).toString();
	
	return path;
}

QString railsPath()
{
	QString path = "rails";
	
	QSettings *sets = settings();
	path = sets->value("rails-path", path).toString();
	
	return path;
}

QSettings *settings(const QString &group)
{
	if( s_settings )
	{
		delete s_settings; // Make sure close all groups
	}
	
	s_settings = new QSettings(QApplication::applicationName(), "subrails");
	deleter.setObject(s_settings);
	
	s_settings->beginGroup(group);
	
	return s_settings;
}

}

