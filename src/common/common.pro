TEMPLATE = lib

CONFIG += staticlib \
warn_on
HEADERS += viewbase.h \
global.h \
sr_export.h \
corebase.h \
modelxmlparser.h
SOURCES += viewbase.cpp \
global.cpp \
corebase.cpp \
modelxmlparser.cpp
TARGET = common
DESTDIR = ../../lib

include(../../config.pri)

INCLUDEPATH += ..
