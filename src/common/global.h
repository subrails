/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef COMMONGLOBAL_H
#define COMMONGLOBAL_H

#include <dcore/applicationproperties.h>

#include <QStringList>
#include <QString>
#include <QSettings>

#include "sr_export.h"

namespace Common
{
	enum Workspace
	{
		Model = 0x01,
		View = 0x02,
		Controller = 0x04,
		All = Model | View | Controller
	};
	
	enum Environment
	{
		Development = 0x01,
		Production,
		Tests
	};
	
	SR_EXPORT QStringList rubyEnvironment(const QString &appendOpt = QString(), const QString &appendLib = QString());
	
	SR_EXPORT QString rubyPath();
	SR_EXPORT QString workingDirectory();
	SR_EXPORT QString railsPath();
	
	SR_EXPORT QSettings *settings(const QString &group = "General" );
};


#endif
