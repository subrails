/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "modelxmlparser.h"
#include <QHash>

#include <dcore/debug.h>

namespace Common {

struct ModelXmlParser::Private
{
	Private() : level(0) {}
	int level;
	QList< Fields > fields;
};

ModelXmlParser::ModelXmlParser()
 : DCore::XmlParserBase(), d(new Private)
{
}


ModelXmlParser::~ModelXmlParser()
{
	delete d;
}

bool ModelXmlParser::startTag(const QString &tag, const QXmlAttributes &atts)
{
	d->level++;
	
	if( d->level == 2 )
	{
		d->fields << Fields();
	}
	else if( d->level > 2 )
	{
		QString type = atts.value("type");
		if( type.isEmpty() )
		{
			type = "string";
		}
		
		d->fields.last()[tag].type = type;
		setReadText(true);
	}
	
	return true;
}

bool ModelXmlParser::endTag(const QString &)
{
	d->level--;
	
	return true;
}

void ModelXmlParser::text(const QString &txt)
{
	d->fields.last()[ currentTag() ].value = txt;
}

QList<ModelXmlParser::Fields> ModelXmlParser::objectInfo() const
{
	return d->fields;
}

}
